var lapiBGFlag = context.getVariable("lapi-bg-flag");
var targetURL = context.getVariable("lapi-blue-target");
var lapiGreenTarget = context.getVariable("lapi-green-target");
print('lapiBGFlag value: ' + lapiBGFlag);
print('targetURL value: ' + targetURL);
print('lapiGreenTarget value: ' + lapiGreenTarget);

var targetHost;

if (lapiBGFlag === false) {
    targetURL = lapiGreenTarget;
}

// If the incoming request has any query params we need to add them to the targetURL
var queryParams = context.getVariable('message.querystring');
if (queryParams) {
    targetURL = targetURL + '?' + queryParams;
}

var url =  "https://" + targetURL;
context.setVariable("target.url", url);
print('targetURL value: ', context.getVariable("target.url"));
context.setVariable("lapiBGFlag", lapiBGFlag);
