# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-s3/browse
module "new-loans-s3" {
  source                             = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-s3.git?ref=v2"
  namespace                          = module.namespace.lower_resource_name
  name                               = "new-loans-s3"
  acl                                = "private"
  versioning_enabled                 = true
  sse_algorithm                      = "aws:kms"
  kms_master_key_arn                 = module.encryption.key_arn
  noncurrent_version_transition_days = 30
  noncurrent_version_expiration_days = 90
  noncurrent_storage_class           = "INTELLIGENT_TIERING"
  current_transitions = {
    short_transition = {
      days          = "30"
      storage_class = "STANDARD_IA"
    },
    long_transition = {
      days          = "90"
      storage_class = "GLACIER"
    }
  }
  current_expiration = {
    days = 365
  }
  force_destroy          = true
  lifecycle_rule_enabled = true
  tags                   = module.namespace.tags
}

# Lambda Function to generate new Loans S3 Bucket Upload URL.
module "new-loans" {
  source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
  namespace   = module.namespace.lower_resource_name
  name        = "new-loans"
  environment = var.environment
  code        = "./../../src/apis/utility/.webpack/new-loans-url/new-loans-url"
  # code_requires_zipping = false
  handler = "main.handler"
  runtime = "${local.nodejs_compatible_runtimes}"
  size    = "s"
  #   warmer_enabled = true
  #   warmer_rate = 1
  timeout          = 600
  vpc_cidr_code    = lookup(var.awsacct_cidr_code, var.environment)
  tags             = module.namespace.tags
  short_tags       = module.namespace.short_tags
  execution_policy = data.aws_iam_policy_document.new-loans-url-lambda.json
  env_vars = {
    # s3_bucket_url_name = aws_s3_access_point.new-loans.arn
    new_loans_bucket = module.new-loans-s3.id
    parameter_store  = module.namespace.upper_resource_name
    url_expires      = "604800"
  }
  layer_arns = [
    # "${local.appconfigLambdaExtension}",
    "${aws_lambda_layer_version.czl74b-libs-utility.arn}",
  ]
}



# resource "aws_iam_role" "new-loans-url-lambda-role" {
#   name = "new-loans-url-lambda-role"
#   permissions_boundary = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy/CloudCoreL3Permissions"
#   assume_role_policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Action": "sts:AssumeRole",
#       "Principal": {
#         "Service": "lambda.amazonaws.com"
#       },
#       "Effect": "Allow"
#     }
#   ]
# }
# EOF
# }

# # The role that AWS Lambda Function will use during execution
# resource "aws_iam_role" "new-loans-url-lambda-role" {
#   name                 = "new-loans-url-lambda-role"
#   permissions_boundary = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy/CloudCoreL3Permissions"
#   assume_role_policy   = data.aws_iam_policy_document.lambda_assume_role_policy.json
#   tags                 = module.namespace.tags
# }


# # A policy that allows AWS Lambda to assume a role
# data "aws_iam_policy_document" "lambda_assume_role_policy" {
#   version = "2012-10-17"
#   statement {
#     actions = ["sts:AssumeRole"]
#     effect  = "Allow"
#     principals {
#       type        = "Service"
#       identifiers = ["lambda.amazonaws.com"]
#     }
#   }
# }

# # Attaches an AWS managed policy to the role above
# resource "aws_iam_role_policy_attachment" "AWSLambdaBasicExecutionRole-new-loans-url-lambda" {
#   role       = aws_iam_role.new-loans-url-lambda-role.id
#   policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
# }

# # Attaches an AWS managed policy to the role above
# resource "aws_iam_role_policy_attachment" "AWSLambdaVPCAccessExecutionRole-new-loans-url-lambda" {
#   role       = aws_iam_role.new-loans-url-lambda-role.id
#   policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
# }

# # The policy for the role above
# resource "aws_iam_role_policy" "new-loans-url-lambda-policy" {
#   name   = "new-loans-url-lambda-policy"
#   role   = aws_iam_role.new-loans-url-lambda-role.id
#   policy = data.aws_iam_policy_document.new-loans-url-lambda.json
# }

data "aws_iam_policy_document" "new-loans-url-lambda" {
  version = "2012-10-17"
  statement {
    sid = "AllowLambdaFunctionToPutParameterToSsm"
    actions = [
      "ssm:PutParameter",
      "ssm:AddTagsToResource"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.account_id}:parameter/${module.namespace.upper_resource_name}/*"
    ]
  }
  statement {
    sid     = "AllowLambdaFunctionToPutObjectToS3"
    actions = ["s3:PutObject", "s3:PutObjectAcl"]
    effect  = "Allow"
    resources = [
      "${module.new-loans-s3.arn}/*"
    ]
  }
  statement {
    sid     = "AllowLambdaFunctionToGetObjectFromS3"
    actions = ["s3:GetObject"]
    effect  = "Allow"
    resources = [
      "${module.new-loans-s3.arn}/*/public.key"
    ]
  }
}


# # Lambda permission fr new-loans-url lambda function to generate a viable PutObject Signed URL for New Loans S3
# resource "aws_iam_role_policy" "new-loans-url-lambda" {
#   name = "new-loans-url-lambda"
#   role = module.new-loans-url-lambda.role.id

#   policy = <<EOF
# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Sid": "Blanket",
#             "Effect": "Allow",
#             "Action": [
#                 "s3:PutObject",
#                 "s3:PutObjectAcl",
#                 "s3:GetObject",
#                 "s3:GetObjectAcl"
#             ],
#             "Resource": [
#               "${module.new-loans-s3.arn}",
#               "${module.new-loans-s3.arn}/*",
#               "${module.new-loans-s3.arn}/*/public.key"
#               ]
#         }
#     ]
# }
# EOF
# }