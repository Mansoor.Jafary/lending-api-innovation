## Party Lambda Aliases for each environment
resource "aws_lambda_alias" "czl74b-party_dev" {
  name             = "czl74b-party_dev"
  description      = "Alias to manage dev traffic"
  function_name    = module.czl74b-party.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}

resource "aws_lambda_alias" "czl74b-party_qa" {
  name             = "czl74b-party_qa"
  description      = "Alias to manage prod traffic"
  function_name    = module.czl74b-party.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}

resource "aws_lambda_alias" "czl74b-party_prod" {
  name             = "czl74b-party_prod"
  description      = "Alias to manage prod traffic"
  function_name    = module.czl74b-party.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}