# resource "aws_wafregional_ipset" "apigee_ips" {
#   name = "apigee_ips"

#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "54.156.250.213/32"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "3.217.69.172/32"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "34.204.20.17/32"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "3.82.177.193/32"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "35.166.41.234/32"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "35.165.223.249/32"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "34.211.132.98/32"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "35.167.139.191/32"
#   }
# }

# resource "aws_wafregional_ipset" "akamai_ips" {
#   name = "akamai_ips"

#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "23.62.238.0/24"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "72.246.52.0/24"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "23.213.54.0/24"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "23.57.69.0/24"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "72.246.150.0/24"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "77.67.85.0/24"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "184.51.101.0/24"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "184.51.199.0/24"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "165.254.137.64/26"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "69.31.33.0/24"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "104.96.220.0/24"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "23.198.10.0/24"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "23.48.94.0/24"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "184.28.127.0/24"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "23.42.158.0/24"
#   }  
# }


# resource "aws_wafregional_ipset" "developer_ips" {
#   name = "developer_ips"

#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "198.74.88.196/30"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "198.74.88.194/31"
#   }  
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "198.74.80.193/32"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "12.129.87.3/32"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "198.74.88.193/32"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "12.129.87.4/32"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "198.74.80.194/31"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "198.74.88.200/32"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "198.74.80.200/32"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "12.129.87.0/27"
#   }
#   ip_set_descriptor {
#     type  = "IPV4"
#     value = "198.74.80.196/30"
#   }
# }

# resource "aws_wafregional_rule" "apigeeiprule" {
#   depends_on  = [aws_wafregional_ipset.apigee_ips]
#   name        = "apigeeiprule"
#   metric_name = "apigeeiprule"

#   predicate {
#     data_id = aws_wafregional_ipset.apigee_ips.id
#     negated = false
#     type    = "IPMatch"
#   }
# }

# resource "aws_wafregional_rule" "developeriprule" {
#   depends_on  = [aws_wafregional_ipset.developer_ips]
#   name        = "developeriprule"
#   metric_name = "developeriprule"

#   predicate {
#     data_id = aws_wafregional_ipset.developer_ips.id
#     negated = false
#     type    = "IPMatch"
#   }
# }

# resource "aws_wafregional_web_acl" "czl74bwaf" {
#     depends_on  = [
#         aws_wafregional_ipset.apigee_ips,
#         aws_wafregional_rule.apigeeiprule,
#         aws_wafregional_ipset.developer_ips,
#         aws_wafregional_rule.developeriprule
#         ]
#     name        = "czl74bwaf"
#     metric_name = "czl74bwaf"

#     default_action {
#        type = "ALLOW"
#     }

#     rule {
#         action {
#             type = "ALLOW"
#         }

#         priority = 1
#         rule_id  = aws_wafregional_rule.apigeeiprule.id
#         type     = "REGULAR"
#     }

#     rule {
#         action {
#             type = "ALLOW"
#         }

#         priority = 2
#         rule_id  = aws_wafregional_rule.developeriprule.id
#         type     = "REGULAR"
#     }
# }

# resource "aws_wafregional_web_acl_association" "association" {
#   resource_arn = aws_appsync_graphql_api.CZL74B-ally-lending.arn
# #   resource_arn = module.api-gateway.rest_api_stage.arn
#   web_acl_id   = aws_wafregional_web_acl.czl74bwaf.id
# }