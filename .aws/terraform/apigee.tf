# module "api_proxy_hello" {
#   source              = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-apigee.git//modules/api?ref=v1"
#   display_name        = "cloud-lab-${module.namespace.lower_resource_name}"
#   description         = "Apigee to Lambda Demo"
#   token               = var.apigee_access_token

#   basepath            = "cloud-lab"
#   developer_email     = "mansoor.jafary@ally.com"
#   application_id      = var.ally_application_id
#   namespace           = module.namespace.lower_resource_name
#   environment         = var.environment
#   bundle_src          = "../../apigee/"
#   apiproxy_variables  = {
#     target_server_host      = "${module.api-gateway.rest_api.id}.execute-api.${var.region}.amazonaws.com"
#     ssl_client_auth_enabled = false
#     ssl_key_store           = "Ally"
#     ssl_key_alias           = "star.api.ally.com"
#     apigw_path              = "${element(split(".com/", module.api-gateway.rest_api_stage.invoke_url), 1)}/hello"
#   }
# }


#########################################################################

locals {
  lending_api_uris = {
        for k, api in aws_appsync_graphql_api.CZL74B-ally-lending : k => api.uris["GRAPHQL"]
    }
}

# provider "apigee" {
#   base_uri = module.common.base_uri
#   org      = module.common.organization
# }

locals {
  basepath_lending  = "cloud-lab"
  basepath_customer = "czl74b-party"
  # apigee-organization = var.apigee_org
  apiproxy_variables = {
    application_id      = var.ally_application_id,
    application_name    = var.application_name,
    application_version = "1.0",
    application_channel = "Web",
    # stage_name          = var.apigw_prod_stage_name,
    api_description         = var.api_description
    ssl_client_auth_enabled = false
    ssl_key_store           = "Ally"
    ssl_key_alias           = "star.api.ally.com"
    apigw_path              = "/graphql"
  }
}

variable "publish_green_lending_appsync" {
  default = false
  type    = bool
}

module "api_proxy_graphql" {
  source       = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-apigee.git//modules/api?ref=v1"
  display_name = module.namespace.lower_resource_name
  description  = "Apigee to AppSync Demo"
  token        = var.apigee_access_token

  developer_email = "mansoor.jafary@ally.com"
  application_id  = var.ally_application_id
  basepath        = local.basepath_lending
  namespace       = module.namespace.lower_resource_name
  environment     = var.environment
  bundle_src      = "../../apigee/"
  apiproxy_variables = "${merge(
    local.apiproxy_variables,
    map(
      "target_server_host", "${element(split("https://", element(split("/graphql", lookup(local.lending_api_uris, local.green_schema)), 0)), 1)}",
      "aws_api_key", "${random_password.header_token.result}",
  ))}"
}

resource "apigee_environment_kvm" "lapi-bg-vars" {
  provider = publish.apigee
  environment_name = "dev"
  name = "lapi-bg-vars"
  entry = {
    lapi-blue-target = element(split("https://", lookup(local.lending_api_uris, local.blue_schema)), 1)
    lapi-green-target = element(split("https://", lookup(local.lending_api_uris, local.green_schema)), 1)
    lapi-bg-flag = var.publish_green_lending_appsync
  }
}

module "api_proxy_graphql-party" {
  source       = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-apigee.git//modules/api?ref=v1"
  display_name = "party-${module.namespace.lower_resource_name}"
  description  = "Apigee to Party AppSync Demo"
  token        = var.apigee_access_token

  developer_email = "mansoor.jafary@ally.com"
  application_id  = var.ally_application_id
  basepath        = local.basepath_customer
  namespace       = "czl74b-party"
  environment     = var.environment
  bundle_src      = "../../apigee/"
  apiproxy_variables = {
    target_server_host      = "${element(split("https://", element(split("/graphql", aws_appsync_graphql_api.CZL74B-ally-party.uris["GRAPHQL"]), 0)), 1)}"
    ssl_client_auth_enabled = false
    ssl_key_store           = "Ally"
    ssl_key_alias           = "star.api.ally.com"
    apigw_path              = "/graphql"
    aws_api_key             = "${aws_appsync_api_key.CZL74B-apigee-party.key}"
  }
}


# data "template_file" "swagger_file" {
#   template = file(var.swagger_file)
#   vars = {
#     api_title   = "${module.namespace.upper_resource_name}-Lending-API"
#     api_version = var.api_version
#     env         = var.environment
#     region      = var.region
#     credentials = module.lapi_api_role.arn
#   }
# }

# module "lapi_api_role" {
#   source       = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-iam.git?ref=v2"
#   name         = "lapi_api_role"
#   namespace    = module.resource_info.lower_resource_name
#   tags         = module.resource_info.tags
#   trust_policy = data.aws_iam_policy_document.trust.json
# }

# data "aws_iam_policy_document" "trust" {
#   statement {
#     effect = "Allow"
#     actions = [
#       "sts:AssumeRole"
#     ]
#     principals {
#       type = "Service"
#       identifiers = [
#         "apigateway.amazonaws.com"
#       ]
#     }
#   }
# }
