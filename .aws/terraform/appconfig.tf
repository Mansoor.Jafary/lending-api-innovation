

resource "aws_appconfig_application" "czl74b-lambda_config" {
  name        = "czl74b-lambda_config"
  description = "AppConfig for Lambda Configurations"

  #   tags = {
  #     Type = "AppConfig Application"
  #   }
}

resource "aws_appconfig_environment" "czl74b-lambda_config" {
  name           = "czl74b-lambda_config"
  description    = "Example AppConfig Environment"
  application_id = aws_appconfig_application.czl74b-lambda_config.id

  #   monitor {
  #     alarm_arn      = aws_cloudwatch_metric_alarm.czl74b-lambda_config.arn
  #     alarm_role_arn = aws_iam_role.czl74b-lambda_config.arn
  #   }

  #   tags = {
  #     Type = "AppConfig Environment"
  #   }
}

resource "aws_appconfig_configuration_profile" "czl74b-lambda_config" {
  application_id = aws_appconfig_application.czl74b-lambda_config.id
  description    = "Lambda Functions Configuration Profile"
  name           = "czl74b-lambda-config"
  location_uri   = "hosted"

  #   validator {
  #     content = aws_lambda_function.example.arn
  #     type    = "LAMBDA"
  #   }

  #   tags = {
  #     Type = "AppConfig Configuration Profile"
  #   }
}

resource "aws_appconfig_hosted_configuration_version" "czl74b-lambda_config" {
  application_id           = aws_appconfig_application.czl74b-lambda_config.id
  configuration_profile_id = aws_appconfig_configuration_profile.czl74b-lambda_config.configuration_profile_id
  description              = "Hosted Configuration"
  content_type             = "application/json"

  content = jsonencode(local.appConfigs)
}

resource "aws_appconfig_deployment_strategy" "czl74b-lambda_config" {
  name                           = "czl74b-lambda_config"
  description                    = "Deployment Strategy"
  deployment_duration_in_minutes = 0
  final_bake_time_in_minutes     = 4
  growth_factor                  = 100
  replicate_to                   = "NONE"
  growth_type                    = "LINEAR"
  #   tags = {
  #     Type = "AppConfig Deployment Strategy"
  #   }
}

resource "aws_appconfig_deployment" "czl74b-lambda_config" {
  application_id           = aws_appconfig_application.czl74b-lambda_config.id
  configuration_profile_id = aws_appconfig_configuration_profile.czl74b-lambda_config.configuration_profile_id
  configuration_version    = aws_appconfig_hosted_configuration_version.czl74b-lambda_config.version_number
  deployment_strategy_id   = aws_appconfig_deployment_strategy.czl74b-lambda_config.id
  description              = "Deploy Lambda Configurations"
  environment_id           = aws_appconfig_environment.czl74b-lambda_config.environment_id

  #   tags = {
  #     Type = "AppConfig Deployment"
  #   }
}