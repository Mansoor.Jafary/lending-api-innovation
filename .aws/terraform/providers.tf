provider "publish" {
  alias = "apigee"
  organization = module.common.organization
}

provider "aws" {
  region = var.region
}