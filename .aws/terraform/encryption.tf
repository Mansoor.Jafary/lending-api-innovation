# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-encryption/browse
module "encryption" {
  source    = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-encryption.git?ref=v1"
  namespace = module.namespace.lower_resource_name
  name      = "czl74b_key"
  tags      = module.namespace.tags
}