output "aws_api_key" {
  value = "${random_password.header_token.result}"
}

output "secret_name" {
  value = module.secretsmanager.secret.name
}

output "appconfig_app_id" {
  value = "${aws_appconfig_application.czl74b-lambda_config.id}"
}

output "appconfig_profile_id" {
  value = "${aws_appconfig_configuration_profile.czl74b-lambda_config.configuration_profile_id}"
}

output "appconfig_environment_id" {
  value = "${aws_appconfig_environment.czl74b-lambda_config.id}"
}

output "czl74b-hello_lambda_version" {
  value = "${module.czl74b-hello_lambda.version}"
}


// not needed
output "appconfig_app_arn" {
  value = "${aws_appconfig_application.czl74b-lambda_config.arn}"
}

//not needed
output "appconfig_config_arn" {
  value = "${aws_appconfig_configuration_profile.czl74b-lambda_config.arn}"
}

//not needed
output "appconfig_hostedconfig_arn" {
  value = "${aws_appconfig_hosted_configuration_version.czl74b-lambda_config.arn}"
}

# Environment Variables for SAM-CLI Setup Help
output "environment" {
  value = "${var.environment}"
}

output "uris" {
  value = "${local.balanceInquiryAdvanceI2CURL[var.environment]}"
}

output "uappconfig_application_name" {
  value = "${aws_appconfig_application.czl74b-lambda_config.name}"
}

output "uappconfig_environment_name" {
  value = "${aws_appconfig_environment.czl74b-lambda_config.name}"
}

output "uappconfig_configuration_name" {
  value = "${aws_appconfig_configuration_profile.czl74b-lambda_config.name}"
}

# output "secret_id" {
#     value = "${data.aws_secretsmanager_secret.secret.id}"
# }

output "namespace1" {
  value = module.namespace
}

output "namespace2" {
  value = module.namespace.lower_resource_name
}

# output "new-loans-s3-access-point" {
#     value = aws_s3_access_point.new-loans.endpoints
# }

# # output "new-loans-s3-access-domain-name" {
# #     value = aws_s3_access_point.new-loans.domain_name
# # }

# # output "new-loans-s3-alias" {
# #     value = aws_s3_access_point.new-loans.alias
# # }

output "encryption_key" {
  value = module.encryption.key
}


output "blue_url_nogql" {
  value = element(split("https://", lookup(local.lending_api_uris, local.blue_schema)), 1)
}
output "green_url_nogql" {
  value = element(split("https://", lookup(local.lending_api_uris, local.green_schema)), 1)
}