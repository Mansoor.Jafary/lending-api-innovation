variable "region" {
  type        = string
  description = "The target AWS region to deploy to."
  default     = "us-east-1"
}

variable "environment" {
  type        = string
  description = "Env to use for resolving variables"
  default     = "default"
}

variable "ally_application_id" {
  type        = string
  description = "The Ally Application ID of the project."
}

variable "application_name" {
  type        = string
  description = "The Ally Application Name of the project."
}

variable "service_name" {
  type        = string
  description = "The Ally Application service name."
}

variable "data_classification" {
  type        = string
  description = "The Ally data classification."
}

variable "issrcl_level" {
  type        = string
  description = "This tag is used to identify the Information System Security Risk Classification Level for the resource with one of the following: low, medium, high. See the readme for the document to determine this."
}

variable "owner" {
  type        = string
  description = "Team owner of the application"
}

variable "scm_project" {
  type        = string
  description = "The Bitbucket project used to deploy the resource."
}

variable "scm_repo" {
  type        = string
  description = "The Bitbucket repository used to deploy the release."
}

variable "scm_branch" {
  type        = string
  description = "The Bitbucket and tag used to deploy the resource."
  default     = "local-deploy"
}

variable "scm_commit_id" {
  type        = string
  description = "The specific commit that was used to deploy the resource."
  default     = "000000"
}

variable "creation_date" {
  type        = string
  description = "The date and time this project was generated."
  default     = "2021-10-05T19:56:46.099365"
}

variable "awsacct_cidr_code" {
  type        = map
  description = "The CIDR block assigned to the account VPC"
}

# Apigee
variable "apigee_access_token" {
  type        = string
  description = "The apigee access token our service accounts (one for each ORG) generate for CICD with Apigee Edge SaaS"
}

variable "api_basepath" {
  type        = string
  description = "The base path identified for API Products in the API Product Domain directly inform the base path for your Apigee proxy"
}

variable "api_name" {
  type        = string
  description = "An API Product is expressed as a business capability. Named in 'noun' + 'passive verb' manner. Directly associated wtih RESTful path."
}

variable "api_description" {
  type        = string
  description = "A short description of the API. CommonMark syntax MAY be used for rich text representation."
}

# Secrets Manager
variable "secrets_names" {
  type        = map
  description = "The names of the secret per environment"
}

variable "czl74b_developer_email" {
  type        = map
  description = "ccn developer email for each apigee environment"
}

# variable "dd_api_key" {
#   description = "Datadog api key"
# }