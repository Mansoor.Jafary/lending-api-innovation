# ################
# IAM            #
# ################

# Policies for AppSync to invoke Lambda Aliases \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
# AppSync Data Source permission to Balances lambda function alias - allows for managing traffic
resource "aws_iam_role_policy" "CZL74B_policy_party" {
  name = "CZL74B_policy_party"
  role = "${aws_iam_role.CZL74B_role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "lambda:InvokeFunction"
      ],
      "Resource": [
         "${local.czl74b-party_ds_function_arn[var.environment]}"
      ]
    }
  ]
}
EOF
}