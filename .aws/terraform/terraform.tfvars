# Name & Tagging
ally_application_id = "105082"
application_name    = "czl74b"
service_name        = "Lending-API"
owner               = "czl74b"
data_classification = "Proprietary"
issrcl_level        = "Low"
scm_project         = ""
scm_repo            = ""
awsacct_cidr_code = {
  default = "010-073-112-000"
  dev     = "010-073-112-000"
  qa      = "010-073-128-000"
  cap     = "010-073-128-000"
  psp     = "010-073-160-000"
  prod    = "010-073-160-000"
}

# Apigee
api_basepath               = "/v1/graphql"
api_description            = "This API supports data operations on Lending Product Accounts"
api_name                   = "Lending API"
apigee_oauth2_product_name = "czl74b-105082-api"
czl74b_developer_email = {
  default = "mansoor.jafary@ally.com"
  dev     = "mansoor.jafary@ally.com"
  qa      = "mansoor.jafary@ally.com"
  cap     = "mansoor.jafary@ally.com"
  psp     = "mansoor.jafary@ally.com"
  prod    = "mansoor.jafary@ally.com"
}
api_version  = "v1.0.0"
swagger_file = "../../resources/api/swagger.yaml"



# Secrets Manager
secrets_names = {
  default = "czl74b-lending-api-105082-default-czl74b_v24"
  dev     = "czl74b-lending-api-105082-default-czl74b_v24"
  qa      = "czl74b-lending-api-105082-qa-czl74b_v24"
  cap     = "czl74b-lending-api-105082-qa-czl74b_v24"
  psp     = "czl74b-lending-api-105082-prod-czl74b_v24"
  prod    = "czl74b-lending-api-105082-prod-czl74b_v24"
}


