data "aws_caller_identity" "current" {}

module "namespace" {
  source              = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-name-and-tags.git?ref=v1"
  application_name    = var.application_name
  service_name        = var.service_name
  workspace           = terraform.workspace
  environment         = var.environment
  application_id      = var.ally_application_id
  data_classification = var.data_classification
  issrcl_level        = var.issrcl_level
  owner               = var.owner
  scm_project         = var.scm_project
  scm_repo            = var.scm_repo
  scm_branch          = var.scm_branch
  scm_commit_id       = var.scm_commit_id
  additional_tags = {
    "tf_starter" = var.creation_date
  }
}

# locals {
#   is_feature_branch       = length(regexall(".*dev-f.*", terraform.workspace)) > 0
#   is_feature_or_hotfix    = length(regexall(".*dev-f.*|.*-h-*.", terraform.workspace)) > 0
#   czl74b_common_workspace    = local.is_feature_branch ? "dev" : terraform.workspace
#   czl74b_common_tf_bucket_id = "ally-${var.region}-${data.aws_caller_identity.current.account_id}-tf-states"
#   czl74b_common_tf_state_key = "env:/${local.czl74b_common_workspace}/105082/terraform.tfstate"
# }

# data "terraform_remote_state" "czl74b_common_resource" {
#   backend = "s3"
#   config = {
#     region = var.region
#     bucket = local.czl74b_common_tf_bucket_id
#     key    = local.czl74b_common_tf_state_key
#   }
# }