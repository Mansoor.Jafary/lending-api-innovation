# ################
# # Data Sources #
# ################

resource "aws_appsync_datasource" "CZL74B_hello_datasource" {
  for_each    = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id      = each.value.id
  name             = "CZL74B_hello_datasource${each.value.tags_all.deployment_color=="blue" ? "blue" : "green"}"
  service_role_arn = "${aws_iam_role.CZL74B_role.arn}"
  type             = "AWS_LAMBDA"
  lambda_config {
    function_arn = "${each.value.tags_all.deployment_color=="blue" ? "${aws_lambda_alias.czl74b-hello_lambda_prod_blue.arn}":  "${aws_lambda_alias.czl74b-hello_lambda_prod_green.arn}"}"
  }
}

resource "aws_appsync_datasource" "CZL74B_fineGrainAuthFail_datasource" {
  for_each         = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id           = each.value.id
  name             = "CZL74B_fineGrainAuthFail_datasource${each.value.tags_all.deployment_color=="blue" ? "blue" : "green"}"
  service_role_arn = "${aws_iam_role.CZL74B_role.arn}"
  type             = "NONE"
}

resource "aws_appsync_datasource" "CZL74B_metrics_datasource" {
  for_each         = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id           = each.value.id
  name             = "CZL74B_metrics_datasource${each.value.tags_all.deployment_color=="blue" ? "blue" : "green"}"
  service_role_arn = "${aws_iam_role.CZL74B_role.arn}"
  type             = "NONE"
}

# demo artifacts

# This single data source replaces all other data sources created for resolver lambdas. Others will be decommissioned
resource "aws_appsync_datasource" "czl74b_lending" {
  for_each         = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id           = each.value.id
  name             = "czl74b_lending${each.value.tags_all.deployment_color=="blue" ? "blue" : "green"}"
  service_role_arn = "${aws_iam_role.CZL74B_role.arn}"
  type             = "AWS_LAMBDA"
  lambda_config {
    # The arn of the lambda function
    function_arn = "${each.value.tags_all.deployment_color=="blue" ? "${local.czl74b-lending_ds_function_arn_blue[var.environment]}":  "${local.czl74b-lending_ds_function_arn_green[var.environment]}"}"
  }
}

resource "aws_appsync_datasource" "czl74b_payments" {
  for_each         = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id           = each.value.id
  name             = "CZL74B_payments${each.value.tags_all.deployment_color=="blue" ? "blue" : "green"}"
  service_role_arn = "${aws_iam_role.CZL74B_role.arn}"
  type             = "AWS_LAMBDA"
  lambda_config {
    # The arn of the lambda function
    function_arn = "${local.czl74b-pay_lambda_alias[var.environment]}"
  }
}