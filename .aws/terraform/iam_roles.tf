# ################
# IAM            #
# ################

# Create an IAM Role

# Role for AppSync
## Used for AppSync Data Sources to Lambda call
resource "aws_iam_role" "CZL74B_role" {
  name                 = "CZL74B_role"
  permissions_boundary = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy/CloudCoreL3Permissions"
  assume_role_policy   = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "appsync.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

# Policy for AppSync to write logs to Cloud Watch
resource "aws_iam_role_policy" "AWSAppSyncPushToCloudWatchLogsPolicy" {
  name = "AWSAppSyncPushToCloudWatchLogsPolicy"
  role = "${aws_iam_role.CZL74B_role.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

# Policies for AppSync to invoke Lambda Aliases \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
# AppSync Data Source permission to Hello lambda function alias - allows for managing prod traffic
resource "aws_iam_role_policy" "CZL74B_policy_hello_prod" {
  name = "CZL74B_policy_hello_prod"
  role = "${aws_iam_role.CZL74B_role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "lambda:InvokeFunction"
      ],
      "Resource": [
        "${aws_lambda_alias.czl74b-hello_lambda_prod_blue.arn}",
        "${aws_lambda_alias.czl74b-hello_lambda_prod_green.arn}"
      ]
    }
  ]
}
EOF
}

# Policy for Lending Data Source to call Resolver Lambda Function \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
# AppSync Data Source permission to lambda function alias allows for managing live versions
resource "aws_iam_role_policy" "CZL74B_policy_lending" {
  name = "CZL74B_policy_lending"
  role = "${aws_iam_role.CZL74B_role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "lambda:InvokeFunction"
      ],
      "Resource": [
         "${local.czl74b-lending_ds_function_arn_blue[var.environment]}",
         "${local.czl74b-lending_ds_function_arn_green[var.environment]}"

      ]
    }
  ]
}
EOF
}

# once I  move balances field decomm below in favor of single resolver lambda data source?
# resource "aws_iam_role_policy" "CZL74B_policy_bal" {
#   name = "CZL74B_policy_bal"
#   role = "${aws_iam_role.CZL74B_role.id}"

#   policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Effect": "Allow",
#       "Action": [
#         "lambda:InvokeFunction"
#       ],
#       "Resource": [
#          "${local.czl74b_balances_ds_function_arn[var.environment]}"
#       ]
#     }
#   ]
# }
# EOF
# }

# once I  move balances field decomm below in favor of single resolver lambda data source?
# AppSync Data Source permission to Payment Info lambda function alias - allows for managing prod traffic
resource "aws_iam_role_policy" "CZL74B_policy_pay" {
  name = "CZL74B_policy_pay"
  role = "${aws_iam_role.CZL74B_role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "lambda:InvokeFunction"
      ],
      "Resource": [
        "${local.czl74b-pay_lambda_alias[var.environment]}"
      ]
    }
  ]
}
EOF
}

# Policies for AppSync to Directly invoke Lambda \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
# An example of attach a policy for datasource direct access to invoke lambda; no alias. This should only be used in test
resource "aws_iam_role_policy" "CZL74B_policy_hello" {
  name = "CZL74B_policy_hello"
  role = "${aws_iam_role.CZL74B_role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "lambda:InvokeFunction"
      ],
      "Resource": [
        "${module.czl74b-hello_lambda.arn}"
      ]
    }
  ]
}
EOF
}

# Policies for Exisitng Lambda Role to allow call to another Lambda function alias - dev, qa and prod \/\/\/\/\/\/\/
# Lambda permission to call another lambda function

resource "aws_iam_role_policy" "czl74b-bal_lambda" {
  name = "czl74b-bal_lambda"
  role = module.czl74b-bal_lambda.role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "lambda:InvokeFunction"
      ],
      "Resource": [
        "${local.czl74b-bal-inquiry_alias[var.environment]}"
      ]
    }
  ]
}
EOF
}

# Lambda permission to call another lambda function
resource "aws_iam_role_policy" "czl74b-pay_lambda" {
  name = "czl74b-pay_lambda"
  role = module.czl74b-pay_lambda.role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "lambda:InvokeFunction"
      ],
      "Resource": [
        "${local.czl74b-creditPayInfo_alias[var.environment]}"
      ]
    }
  ]
}
EOF
}

# Lambda permission to call another lambda function
resource "aws_iam_role_policy" "czl74b-lending" {
  name = "czl74b-lending"
  role = module.czl74b-lending.role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "lambda:InvokeFunction"
      ],
      "Resource": [
        "${local.czl74b-bal_lambda_alias[var.environment]}",
        "${module.new-loans.arn}"
      ]
    }
  ]
}
EOF
}

# This is not needed as Client will not upload to S3 via direct IAM Role Access
# # Lambda permission to upload data from new-loans-s3-client lambda new-loans-s3
# resource "aws_iam_role_policy" "czl74b-new-loans-s3-client" {
#   name = "czl74b-new-loans-s3-client"
#   role = module.new-loans-s3-client.role.id

#   policy = <<EOF
# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Sid": "ListObjectsInBucket",
#             "Effect": "Allow",
#             "Action": ["s3:PutObject"],
#             "Resource": ["${module.new-loans-s3.arn}"]
#         }
#     ]
# }
# EOF
# }

# disabling temporarily.. to take things step by step
# # Lambda permission fr new-loans lambda function to access objects in New Loans S3
# resource "aws_iam_role_policy" "czl74b-new-loans-lambda" {
#   name = "czl74b-new-loans-lambda"
#   role = module.new-loans-lambda.role.id

#   policy = <<EOF
# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Sid": "ListObjectsInBucket",
#             "Effect": "Allow",
#             "Action": ["s3:ListBucket"],
#             "Resource": ["${module.new-loans-s3.arn}"]
#         },
#         {
#             "Sid": "AllObjectActions",
#             "Effect": "Allow",
#             "Action": "s3:*Object",
#             "Resource": ["${module.new-loans-s3.arn}/*"]
#         }
#     ]
# }
# EOF
# }

# # Giving role policies to S3 bucket for allowing upload - 403 access denied when trying to do this
# resource "aws_s3_bucket_policy" "allow_access_test" {
#   bucket = "${module.new-loans-s3.id}"
#   policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Effect": "Allow",
#       "Principal": {
#         "AWS": "*"
#       },
#       "Action": [ "s3:*" ],
#       "Resource": [
#         "${module.new-loans-s3.arn}",
#       "${module.new-loans-s3.arn}/*"
#       ]
#     }
#   ]
# }
# EOF
# }



#Create a Lambda Policy document to access AppConfig Extensions API

data "aws_iam_policy_document" "appconfig_policy" {
  statement {
    effect = "Allow"
    actions = [
      "ssm:GetDocument",
      "ssm:ListDocuments",
      "appconfig:ListApplications",
      "appconfig:GetApplication",
      "appconfig:ListEnvironments",
      "appconfig:GetEnvironment",
      "appconfig:ListConfigurationProfiles",
      "appconfig:GetConfigurationProfile",
      "appconfig:ListDeploymentStrategies",
      "appconfig:GetDeploymentStrategy",
      "appconfig:GetConfiguration",
      "appconfig:ListDeployments",
      "appconfig:GetDeployment"
    ]
    resources = [
      "*"
    ]
  }
}

# Create and Attach Lambda permission to access appconfig extensions
resource "aws_iam_policy" "appconfig_policy" {
  name   = "appconfig_policy"
  policy = data.aws_iam_policy_document.appconfig_policy.json
}

resource "aws_iam_role_policy_attachment" "czl74b-hello_lambda_appconfig" {
  role       = module.czl74b-hello_lambda.role.name
  policy_arn = aws_iam_policy.appconfig_policy.arn
}

resource "aws_iam_role_policy_attachment" "czl74b-bal_lambda_appconfig" {
  role       = module.czl74b-bal_lambda.role.name
  policy_arn = aws_iam_policy.appconfig_policy.arn
}

resource "aws_iam_role_policy_attachment" "czl74b-pay_lambda_appconfig" {
  role       = module.czl74b-pay_lambda.role.name
  policy_arn = aws_iam_policy.appconfig_policy.arn
}

resource "aws_iam_role_policy_attachment" "czl74b-bal-inquiry_appconfig" {
  role       = module.czl74b-bal-inquiry.role.name
  policy_arn = aws_iam_policy.appconfig_policy.arn
}

resource "aws_iam_role_policy_attachment" "czl74b-creditPayInfo_appcongig" {
  role       = module.czl74b-creditPayInfo.role.name
  policy_arn = aws_iam_policy.appconfig_policy.arn
}
resource "aws_iam_role_policy_attachment" "czl74b-lending_appconfig" {
  role       = module.czl74b-lending.role.name
  policy_arn = aws_iam_policy.appconfig_policy.arn
}

# Create a Lambda Policy document to access Param Store

data "aws_iam_policy_document" "param-store_policy" {
  statement {
    effect = "Allow"
    actions = [
      "ssm:DescribeParameters",
      "ssm:GetParameter",
      "ssm:GetParametersByPath",
      "kms:Decrypt"
    ]
    resources = [
      "*"
    ]
  }
}

# Create and Attach Lambda permission to access param-store
resource "aws_iam_policy" "param_policy" {
  name   = "param_policy"
  policy = data.aws_iam_policy_document.param-store_policy.json
}

resource "aws_iam_role_policy_attachment" "param_policy" {
  role       = module.czl74b-hello_lambda.role.name
  policy_arn = aws_iam_policy.param_policy.arn
}

# Policies for Lambda to Lambda access for Demo - End

# Lambda Permissions \/\/\/\/\/\/\/\/\/
## used for API Gateway Rest API to Lambda Calls

# Permission for API Gateway to access Lambda Function directly
resource "aws_lambda_permission" "apigw_hello_lambda" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = module.czl74b-hello_lambda.name
  principal     = "apigateway.amazonaws.com"

  # The "/*/*" portion grants access from any method on any resource
  # within the API Gateway REST API.
  source_arn = "${module.api-gateway.rest_api.execution_arn}/*/*"
}

# Permission for API Gateway to access Lambda Function directly
resource "aws_lambda_permission" "apigw-test_lambda" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = module.czl74b-test_lambda.name
  principal     = "apigateway.amazonaws.com"

  # The "/*/*" portion grants access from any method on any resource
  # within the API Gateway REST API.
  source_arn = "${module.api-gateway.rest_api.execution_arn}/*/*"
}

# I had to remove this due to temp issues with API Gateway deployment.. this will be enabled again
# # Permission for API Gateway to access Lambda Function directly
# resource "aws_lambda_permission" "apigw-gateway" {
#    statement_id  = "AllowAPIGatewayInvoke"
#    action        = "lambda:InvokeFunction"
#    function_name = module.gateway-server.name
#    principal     = "apigateway.amazonaws.com"

#    # The "/*/*" portion grants access from any method on any resource
#    # within the API Gateway REST API.
#    source_arn = "${module.api-gateway.rest_api.execution_arn}/*/*"
# }

# IAM Policie and Attachment to allow Lambda function access to secrets \/\/\/\/\/\/\/

# # Creating a lambda policy document
# data "aws_iam_policy_document" "lambda_policy" {
#     statement {
#         effect = "Allow"
#         actions = [
#             "kms:decrypt"
#         ]
#         resources = [
#             data.aws_secretsmanager_secret.secret.kms_key_id
#         ]
#     }
#     statement {
#         effect = "Allow"
#         actions = [
#             "secretsmanager:GetSecretValue"
#         ]
#         resources = [data.aws_secretsmanager_secret.secret.arn]
#     }
# }

# # Lambda permission to access secret
# resource "aws_iam_policy" "secrets-mgr-policy" {
#     name = "${module.namespace.lower_resource_name}_secrets-mgr-policy"
#     policy = data.aws_iam_policy_document.lambda_policy.json
# }

# resource "aws_iam_role_policy_attachment" "czl74b-hello_lambda-secrets" {
#     role = module.czl74b-hello_lambda.role.name
#     policy_arn = aws_iam_policy.secrets-mgr-policy.arn
# }

# resource "aws_iam_role_policy_attachment" "czl74b-bal-inquiry-secrets" {
#     role = module.czl74b-bal-inquiry.role.name
#     policy_arn = aws_iam_policy.secrets-mgr-policy.arn
# }

# resource "aws_iam_role_policy_attachment" "czl74b-creditPayInfo-secrets" {
#     role = module.czl74b-creditPayInfo.role.name
#     policy_arn = aws_iam_policy.secrets-mgr-policy.arn
# }