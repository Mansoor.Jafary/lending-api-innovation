# #############
# API Gateway #
# #############

# # Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-api-gateway/browse
module "api-gateway" {
  source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-api-gateway.git?ref=v1"
  namespace   = module.namespace.lower_resource_name
  name        = "czl74b-api-gateway"
  environment = var.environment

  api_body = jsonencode({
    openapi = "3.0.1"
    info = {
      title   = "example"
      version = "1.0"
    }
    paths = {
      # "/graphql" = {
      #   post = {
      #     x-amazon-apigateway-integration = {
      #       httpMethod           = "POST"
      #       payloadFormatVersion = "1.0"
      #       type                 = "HTTP_PROXY"
      #       uri                  = var.impl_blue_lending_appsync? "${aws_appsync_graphql_api.CZL74B-ally-lending.uris["GRAPHQL"]}" : ""
      #     }
      #   }
      # },
      # "/lending" = {
      #   post = {
      #     x-amazon-apigateway-integration = {
      #       httpMethod           = "POST"
      #       payloadFormatVersion = "1.0"
      #       type                 = "HTTP_PROXY"
      #       uri                  = module.gateway-server.invoke_arn
      #     }
      #   }
      # },
      "/test" : {
        "get" : {
          "responses" : {
            "200" : {
              "description" : "200 response"
            }
          },
          "x-amazon-apigateway-integration" : {
            "type" : "AWS",
            "uri" = module.czl74b-test_lambda.invoke_arn,
            "httpMethod" : "POST",
            "responses" : {
              "default" : {
                "statusCode" : 200
              }
            }
          }
        }
      },
      "/hello" : {
        "get" : {
          "responses" : {
            "200" : {
              "description" : "200 response"
            }
          },
          "x-amazon-apigateway-integration" : {
            "type" : "AWS",
            "uri" = module.czl74b-hello_lambda.invoke_arn,
            "httpMethod" : "POST",
            "responses" : {
              "default" : {
                "statusCode" : 200
              }
            }
          }
        }
      }
    }
  })
  tags = module.namespace.tags
}

