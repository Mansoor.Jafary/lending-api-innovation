# this implementation is for retrieving existing secrets from secrets manager
# # For accessing secret name by other terraform resources and modules
# data "aws_secretsmanager_secret" "secret" {
#     name = var.secrets_names[var.environment]
# }

# data "aws_secretsmanager_secret_version" "userId" {
#   secret_id = data.aws_secretsmanager_secret.userId.id
# }

# data "aws_secretsmanager_secret_version" "password" {
#   secret_id = data.aws_secretsmanager_secret.password.id
# }