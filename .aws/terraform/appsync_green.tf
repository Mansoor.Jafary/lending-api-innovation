# #######################
# # AppSync GraphQL API #
# #######################

# # variable "impl_green_lending_appsync" {
# #   default = false
# #   type    = bool
# # }

# resource "aws_appsync_graphql_api" "CZL74B-lending-green" {
# #   count               = var.impl_green_lending_appsync ? 1 : 0
#   name                = "CZL74B-ally-lending"
#   tags                = module.namespace.tags
#   schema              = "${file("./../../src/graphql/Lending/schema/schema.graphql")}"
#   authentication_type = "AWS_LAMBDA"
#   # additional_authentication_provider {
#   #   authentication_type = "API_KEY"
#   # }
#   lambda_authorizer_config {
#     # authorizer_uri                 = local.czl74b-authForAS_function_arn[var.environment] //Lambda Alias for AppSync Lambda Authorizer is not supported
#     authorizer_uri                   = module.czl74b_authforas.arn
#     authorizer_result_ttl_in_seconds = 0
#   }
#   log_config {
#     cloudwatch_logs_role_arn = aws_iam_role.CZL74B_role.arn
#     field_log_level          = "ERROR"
#   }
#   # additional_tags = {
#   #   Has_Toggle = var.enable_new_ami
#   # }
# }

# # resource "aws_appsync_api_key" "CZL74B-apigee-green" {
# #   count   = var.impl_green_lending_appsync ? 1 : 0
# #   api_id  = aws_appsync_graphql_api.CZL74B-lending-green[count.index].id
# #   expires = "2022-05-03T04:00:00Z"
# # }

# resource "aws_wafv2_web_acl_association" "AppSync-green" {
# #   count        = var.impl_green_lending_appsync ? 1 : 0
#   resource_arn = aws_appsync_graphql_api.CZL74B-lending-green.arn
#   web_acl_arn  = aws_wafv2_web_acl.czl74b-waf.arn
# }