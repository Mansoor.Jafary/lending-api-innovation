# Lambda Layer archives
locals {
  home-dir                            = "./../../"
  libs-business-input-path            = "./../../src/apis/business/.webpack/libs-business"
  libs-business-zip-location          = format("%s/%s", local.home-dir, "czl74b-libs-business.zip")
  libs-sor-input-path                 = "./../../src/apis/sor/.webpack/libs-sor"
  libs-sor-zip-location               = format("%s/%s", local.home-dir, "czl74b-libs-sor.zip")
  libs-utility-input-path             = "./../../src/apis/utility/.webpack/libs-utility"
  libs-utility-zip-location           = format("%s/%s", local.home-dir, "czl74b-libs-utility.zip")
  libs-resolver-input-path            = "./../../src/resolver/.webpack/libs-resolver"
  libs-resolver-zip-location          = format("%s/%s", local.home-dir, "czl74b-libs-resolver.zip")
  libs-appsync-auth-input-path        = "./../../src/appsync-authorizer/.webpack/libs-auth"
  libs-appsync-auth-zip-location      = format("%s/%s", local.home-dir, "czl74b-libs-appsync-auth.zip")
  czl74b-npm-apollo-libs_zip_location = "./../../czl74b-npm-apollo-libs.zip"
  # libs-apollo-libs-input-path = "./../.."
  # npm-apollo-libs-zip-location = format("%s/%s", local.libs-utility-input-path,"czl74b-npm-apollo-libs.zip")
}

data "archive_file" "czl74b-libs-business" {
  type        = "zip"
  source_dir  = local.libs-business-input-path
  output_path = local.libs-business-zip-location
}
# data "external" "czl74b-libs-business" {
#   program = ["sh", "${path.module}/archive.sh"]
#   query = {
#     input_path = local.libs-business-input-path
#     output_path = local.libs-business-zip-location
#   }
# }
resource "aws_lambda_layer_version" "czl74b-libs-business" {
  filename            = local.libs-business-zip-location
  layer_name          = "czl74b-libs-business"
  compatible_runtimes = ["${local.nodejs_compatible_runtimes}"]
  source_code_hash    = data.archive_file.czl74b-libs-business.output_base64sha256
}

data "archive_file" "czl74b-libs-sor" {
  type        = "zip"
  source_dir  = local.libs-sor-input-path
  output_path = local.libs-sor-zip-location
}

# data "external" "czl74b-libs-sor" {
#   program = ["sh", "${path.module}/archive.sh"]
#   query = {
#     input_path = local.libs-sor-input-path
#     output_path = local.libs-sor-zip-location
#   }
# }
resource "aws_lambda_layer_version" "czl74b-libs-sor" {
  filename            = local.libs-sor-zip-location
  layer_name          = "czl74b-libs-sor"
  compatible_runtimes = ["${local.nodejs_compatible_runtimes}"]
  source_code_hash    = data.archive_file.czl74b-libs-sor.output_base64sha256
}

data "archive_file" "czl74b-libs-utility" {
  type        = "zip"
  source_dir  = local.libs-utility-input-path
  output_path = local.libs-utility-zip-location
}
# data "external" "czl74b-libs-utility" {
#   program = ["sh", "${path.module}/archive.sh"]
#   query = {
#     input_path = local.libs-utility-input-path
#     output_path = local.libs-utility-zip-location
#   }
# }
resource "aws_lambda_layer_version" "czl74b-libs-utility" {
  filename            = local.libs-utility-zip-location
  layer_name          = "czl74b-libs-utility"
  compatible_runtimes = ["${local.nodejs_compatible_runtimes}"]
  source_code_hash    = data.archive_file.czl74b-libs-utility.output_base64sha256
}

data "archive_file" "czl74b-libs-resolver" {
  type        = "zip"
  source_dir  = local.libs-resolver-input-path
  output_path = local.libs-resolver-zip-location
}
# data "external" "czl74b-libs-resolver" {
#   program = ["sh", "${path.module}/archive.sh"]
#   query = {
#     input_path = local.libs-resolver-input-path
#     output_path = local.libs-resolver-zip-location
#   }
# }
resource "aws_lambda_layer_version" "czl74b-libs-resolver" {
  filename            = local.libs-resolver-zip-location
  layer_name          = "czl74b-libs-resolver"
  compatible_runtimes = ["${local.nodejs_compatible_runtimes}"]
  source_code_hash    = data.archive_file.czl74b-libs-resolver.output_base64sha256
}

data "archive_file" "czl74b-libs-appsync-auth" {
  type        = "zip"
  source_dir  = local.libs-appsync-auth-input-path
  output_path = local.libs-appsync-auth-zip-location
}
# data "external" "czl74b-libs-appsync-auth" {
#   program = ["sh", "${path.module}/archive.sh"]
#   query = {
#     input_path = local.libs-appsync-auth-input-path
#     output_path = local.libs-appsync-auth-zip-location
#   }
# }
resource "aws_lambda_layer_version" "czl74b-libs-appsync-auth" {
  filename            = local.libs-appsync-auth-zip-location
  layer_name          = "czl74b-libs-appsync-auth"
  compatible_runtimes = ["${local.nodejs_compatible_runtimes}"]
  source_code_hash    = data.archive_file.czl74b-libs-appsync-auth.output_base64sha256
}

# data "archive_file" "czl74b-common-utils" {
#   type        = "zip"
#   source_dir = "./../../src/common-utils"
#   output_path = "${ local.czl74b-common-utils_zip_location }"
# }

# data "archive_file" "czl74b-npm-libs" {
#   type        = "zip"
#   source_dir = "./../../src/npm-libs"
#   output_path = "${ local.czl74b-npm-libs_zip_location }"
# }

# resource "aws_lambda_layer_version" "czl74b-common-utils" {
#   filename   = "${ local.czl74b-common-utils_zip_location }"
#   layer_name = "czl74b-common-utils"
#   compatible_runtimes = ["${local.nodejs_compatible_runtimes}"]
#   source_code_hash = data.archive_file.czl74b-common-utils.output_base64sha256
# }

# resource "aws_lambda_layer_version" "czl74b-npm-libs" {
#   filename   = "${ local.czl74b-npm-libs_zip_location }"
#   layer_name = "czl74b-npm-libs"
#   compatible_runtimes = ["${local.nodejs_compatible_runtimes}"]
#   source_code_hash = data.archive_file.czl74b-npm-libs.output_base64sha256
# }

data "archive_file" "czl74b-npm-apollo-libs" {
  type        = "zip"
  source_dir  = "./../../src/npm-apollo-libs"
  output_path = "${local.czl74b-npm-apollo-libs_zip_location}"
}

resource "aws_lambda_layer_version" "czl74b-npm-apollo-libs" {
  filename            = "${local.czl74b-npm-apollo-libs_zip_location}"
  layer_name          = "czl74b-npm-apollo-libs"
  compatible_runtimes = ["${local.nodejs_compatible_runtimes}"]
  source_code_hash    = data.archive_file.czl74b-npm-apollo-libs.output_base64sha256
}