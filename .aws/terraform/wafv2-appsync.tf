resource "aws_wafv2_ip_set" "apigee_ips" {
  name               = "apigee_ips"
  description        = "Apigee IPs"
  scope              = "REGIONAL"
  ip_address_version = "IPV4"
  addresses = [
    "54.156.250.213/32",
    "3.217.69.172/32",
    "34.204.20.17/32",
    "3.82.177.193/32",
    "35.166.41.234/32",
    "35.165.223.249/32",
    "34.211.132.98/32",
    "35.167.139.191/32"
  ]
}
resource "aws_wafv2_ip_set" "akamai_ips" {
  name               = "akamai_ips"
  description        = "Akamai IPs"
  scope              = "REGIONAL"
  ip_address_version = "IPV4"
  addresses = [
    "23.62.238.0/24",
    "72.246.52.0/24",
    "23.213.54.0/24",
    "23.57.69.0/24",
    "72.246.150.0/24",
    "77.67.85.0/24",
    "184.51.101.0/24",
    "184.51.199.0/24",
    "165.254.137.64/26",
    "69.31.33.0/24",
    "104.96.220.0/24",
    "23.198.10.0/24",
    "23.48.94.0/24",
    "184.28.127.0/24",
    "23.42.158.0/24"
  ]
}

resource "aws_wafv2_ip_set" "developer_ips" {
  name               = "developer_ips"
  description        = "Developer IPs"
  scope              = "REGIONAL"
  ip_address_version = "IPV4"
  addresses = [
    "198.74.88.196/30",
    "198.74.88.194/31",
    "198.74.80.193/32",
    "12.129.87.3/32",
    "198.74.88.193/32",
    "12.129.87.4/32",
    "198.74.80.194/31",
    "198.74.88.200/32",
    "198.74.80.200/32",
    "12.129.87.0/27",
    "198.74.80.196/30"
  ]
}

resource "aws_wafv2_web_acl" "czl74b-waf" {
  name = "czl74b-waf"
  # metric_name = "czl74b-waf"
  scope = "REGIONAL"

  default_action {
    block {}
  }


  rule {
    name = "allowed_apigee_ips"
    action {
      allow {}
    }

    priority = 1
    statement {
      ip_set_reference_statement {
        arn = aws_wafv2_ip_set.apigee_ips.arn
      }
    }

    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "allowed_apigee_ips"
      sampled_requests_enabled   = true
    }
  }

  rule {
    name = "allowed_developer_ips"
    action {
      allow {}
    }

    priority = 2
    statement {
      ip_set_reference_statement {
        arn = aws_wafv2_ip_set.developer_ips.arn
      }
    }
    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "allowed_developer_ips"
      sampled_requests_enabled   = true
    }
  }

  # rule {
  #   name = "allowed_akamai_ips"
  #   action {
  #     allow {}
  #   }

  #   priority = 3
  #   statement {
  #     ip_set_reference_statement {
  #       arn = aws_wafv2_ip_set.akamai_ips.arn
  #     }
  #   }

  #   visibility_config {
  #     cloudwatch_metrics_enabled = true
  #     metric_name                = "allowed_akamai_ips"
  #     sampled_requests_enabled   = true
  #   }
  # }

  visibility_config {
    cloudwatch_metrics_enabled = true
    metric_name                = "allowed_ips"
    sampled_requests_enabled   = true
  }
}

# locals {
#   blue_api_arn = "${aws_appsync_graphql_api.CZL74B-ally-lending[each.key].arn}"
#   green_api_arn = "${file("./../../src/graphql/Lending/schema/schema_green.graphql")}"
# }

# locals {
#   graph_apis_arn_list = var.impl_blue_lending_appsync ? var.impl_green_lending_appsync ? concat([local.blue_api_arn], [local.green_api_arn]) : [local.blue_api_arn] : var.impl_green_lending_appsync ? [local.green_api_arn] : []
# }

resource "aws_wafv2_web_acl_association" "AppSync" {
  for_each = aws_appsync_graphql_api.CZL74B-ally-lending
  # count        = var.impl_blue_lending_appsync? 1 : 0
  # resource_arn = aws_appsync_graphql_api.CZL74B-ally-lending[count.index].arn
  resource_arn = each.value.arn
  web_acl_arn  = aws_wafv2_web_acl.czl74b-waf.arn
}