# #############
# # Functions #
# #############

locals {
  helloPipeFunc_ids = {
          for k, func in aws_appsync_function.czl74b_hello_function : k => func.function_id
      }
}

resource "aws_appsync_function" "czl74b_hello_function" {
  depends_on = [
    aws_appsync_datasource.CZL74B_hello_datasource
  ]
  for_each    = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id                    = each.value.id
  data_source = "${each.value.tags_all.deployment_color=="blue" ? "CZL74B_hello_datasourceblue" : "CZL74B_hello_datasourcegreen"}"
  name                      = "hello"
  # name             = "helloPipeFunc${each.value.tags_all.deployment_color=="blue" ? "blue" : "green"}"
  request_mapping_template  = "${file("./../../src/graphql/Lending/function-templates/Query.hello.hello.req.vtl")}"
  response_mapping_template = "${file("./../../src/graphql/Lending/function-templates/Query.hello.hello.res.vtl")}"
}