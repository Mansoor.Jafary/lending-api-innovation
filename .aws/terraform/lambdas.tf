# #############
# Lambdas     #
# #############

# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-lambda/browse
module "czl74b-hello_lambda" {
  source          = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
  namespace       = module.namespace.lower_resource_name
  name            = "czl74b-hello_lambda"
  environment     = var.environment
  code            = "./../../src/apis/hello"
  handler         = "main.handler"
  runtime         = "${local.nodejs_compatible_runtimes}"
  vpc_cidr_code   = lookup(var.awsacct_cidr_code, var.environment)
  size            = "s"
  timeout         = 600
  lambda_insights = true
  tags            = module.namespace.tags
  short_tags      = module.namespace.short_tags
  publish         = true
  env_vars = {
    ENVIRONMENT = var.environment
    APPCONFIG_PARAMS = jsonencode({
      "appConfig" : {
        "Application" : "${aws_appconfig_application.czl74b-lambda_config.name}",
        "ClientId" : "lapi",
        "Environment" : "${aws_appconfig_environment.czl74b-lambda_config.name}",
        "Configuration" : "${aws_appconfig_configuration_profile.czl74b-lambda_config.configuration_profile_id}",
        "ClientConfigurationVersion" : "${aws_appconfig_hosted_configuration_version.czl74b-lambda_config.id}"
      }
    })
    # AppConfig Configurations - Used by multiple lamba Layers
    APPCONFIG_LOCALHOST                           = "http://localhost:${local.appConfigExtensionPort}/applications/${local.applicationName}/environments/${local.environmentName}/configurations/${local.configurationName}"
    AWS_APPCONFIG_EXTENSION_POLL_INTERVAL_SECONDS = local.appConfigExtensionPollIntervalSeconds
    AWS_APPCONFIG_EXTENSION_POLL_TIMEOUT_MILLIS   = local.appConfigExtensionPollTimeoutMilliSeconds
    # Secrets Manager
    # SECRET_NAME = data.aws_secretsmanager_secret.secret.id
    # Datadog
    # DD_LAMBDA_HANDLER = "./../../src/apis/hello/main.handler"
    # DD_API_KEY = var.dd_api_key
    # DD_TRACE_ENABLED = true
    # DD_MERGE_XRAY_TRACES = true
    # DD_FLUSH_TO_LOG      = true
    # DD_ENHANCED_METRICS  = true
  }
  layer_arns = [
    "${local.appconfigLambdaExtension}",
    "${aws_lambda_layer_version.czl74b-libs-sor.arn}",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Extension:9",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Node14-x:62"
  ]
}

module "czl74b-test_lambda" {
  source          = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
  namespace       = module.namespace.lower_resource_name
  name            = "czl74b-test_lambda"
  environment     = var.environment
  code            = "./../../src/apis/test"
  handler         = "main.handler"
  runtime         = "${local.nodejs_compatible_runtimes}"
  size            = "s"
  timeout         = 600
  lambda_insights = true
  vpc_cidr_code   = lookup(var.awsacct_cidr_code, var.environment)
  tags            = module.namespace.tags
  short_tags      = module.namespace.short_tags
  publish         = true
  env_vars = {
    URIS = jsonencode({
      i2C_TEST_API : "https://ws2.mycardplace.com:6443/MCPWebServicesRestful/services/MCPWSHandler/balanceInquiryAdvance"
    })
    # Datadog
    # DD_LAMBDA_HANDLER = "./../../src/apis/test/main.handler"
    # DD_API_KEY = var.dd_api_key
    # DD_TRACE_ENABLED = true
    # DD_MERGE_XRAY_TRACES = true
    # DD_FLUSH_TO_LOG      = true
    # DD_ENHANCED_METRICS  = true
  }
  layer_arns = [
    "${aws_lambda_layer_version.czl74b-libs-sor.arn}",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Extension:9",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Node14-x:62"
  ]
}

module "czl74b-testpokeapi" {
  source          = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
  namespace       = module.namespace.lower_resource_name
  name            = "czl74b-testpokeapi"
  environment     = var.environment
  code            = "./../../src/apis/testpokeapi"
  handler         = "main.handler"
  runtime         = "${local.nodejs_compatible_runtimes}"
  size            = "s"
  timeout         = 600
  lambda_insights = true
  vpc_cidr_code   = lookup(var.awsacct_cidr_code, var.environment)
  tags            = module.namespace.tags
  short_tags      = module.namespace.short_tags
  layer_arns = [
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Extension:9",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Node14-x:62"
  ]
}

module "czl74b-metrics" {
  source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
  namespace   = module.namespace.lower_resource_name
  name        = "czl74b-metrics"
  environment = var.environment
  code        = "./../../src/apis/metrics"
  handler     = "main.handler"
  runtime     = "${local.nodejs_compatible_runtimes}"
  size        = "s"
  timeout     = 600
  # lambda_insights = true
  vpc_cidr_code = lookup(var.awsacct_cidr_code, var.environment)
  tags          = module.namespace.tags
  short_tags    = module.namespace.short_tags
  layer_arns = [
    "${local.appconfigLambdaExtension}",
    "${aws_lambda_layer_version.czl74b-libs-sor.arn}"
  ]
}

# Artifacts for Demo
# Lambda Function to test uploading data to S3 Bucket. This is for testing only
module "new-loans-s3-client" {
  source          = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
  namespace       = module.namespace.lower_resource_name
  name            = "new-loans-s3-client"
  environment     = var.environment
  code            = "./../../src/new-loans-s3-client"
  handler         = "main.handler"
  runtime         = "${local.nodejs_compatible_runtimes}"
  size            = "s"
  timeout         = 600
  lambda_insights = true
  vpc_cidr_code   = lookup(var.awsacct_cidr_code, var.environment)
  tags            = module.namespace.tags
  short_tags      = module.namespace.short_tags
  env_vars = {
    s3_bucket_name = module.new-loans-s3.arn,
    # Datadog
    # DD_LAMBDA_HANDLER = "./../../src/new-loans-s3-client/main.handler"
    # DD_API_KEY = var.dd_api_key
    # DD_TRACE_ENABLED = true
    # DD_MERGE_XRAY_TRACES = true
    # DD_FLUSH_TO_LOG      = true
    # DD_ENHANCED_METRICS  = true
  }
  # layer_arns = [
  #   # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Extension:9",
  #   # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Node14-x:62"
  # ]
}

# Lambda Function Triggered by New Loans S3 Bucket. This will integrate with SQS for processing new loan records
module "new-loans-lambda" {
  source          = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
  namespace       = module.namespace.lower_resource_name
  name            = "new-loans-lambda"
  environment     = var.environment
  code            = "./../../src/apis/utility/new-loans-s3-processor"
  handler         = "main.handler"
  runtime         = "${local.nodejs_compatible_runtimes}"
  size            = "s"
  timeout         = 600
  lambda_insights = true
  vpc_cidr_code   = lookup(var.awsacct_cidr_code, var.environment)
  tags            = module.namespace.tags
  short_tags      = module.namespace.short_tags
  # env_vars = {
  #   # Datadog
  #   # DD_LAMBDA_HANDLER = "./../../src/apis/utility/new-loans-s3-processor/main.handler"
  #   # DD_API_KEY = var.dd_api_key
  #   # DD_TRACE_ENABLED = true
  #   # DD_MERGE_XRAY_TRACES = true
  #   # DD_FLUSH_TO_LOG      = true
  #   # DD_ENHANCED_METRICS  = true
  # }
  layer_arns = [
    "${local.appconfigLambdaExtension}",
    # "${aws_lambda_layer_version.czl74b-npm-libs.arn}",
    # "${aws_lambda_layer_version.czl74b-common-utils.arn}",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Extension:9",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Node14-x:62"
  ]
}

# Resolver Lambdas \/\/\/\/\/\/\/
# Single Resolver Lambda for Lending Service GraphQL Schema.
module "czl74b-lending" {
  source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
  namespace   = module.namespace.lower_resource_name
  name        = "czl74b-lending"
  environment = var.environment
  code        = "./../../src/resolver/.webpack/lending/lending"
  # code_requires_zipping = false
  handler         = "main.handler"
  runtime         = "${local.nodejs_compatible_runtimes}"
  size            = "s"
  timeout         = 600
  publish         = true
  lambda_insights = true
  vpc_cidr_code   = lookup(var.awsacct_cidr_code, var.environment)
  tags            = module.namespace.tags
  short_tags      = module.namespace.short_tags
  env_vars = {
    ENVIRONMENT = var.environment
    SCHEMA_PATH = "/lending.graphql"
    URIS = jsonencode({
      readBalancesSnapshot : "${local.czl74b-bal_lambda_alias[var.environment]}",
      createNewLoansUploadURL : "${module.new-loans.arn}"
    })
    # AppConfig Configurations - Used by multiple lamba Layers
    APPCONFIG_LOCALHOST                           = "http://localhost:${local.appConfigExtensionPort}/applications/${local.applicationName}/environments/${local.environmentName}/configurations/${local.configurationName}"
    AWS_APPCONFIG_EXTENSION_POLL_INTERVAL_SECONDS = local.appConfigExtensionPollIntervalSeconds
    AWS_APPCONFIG_EXTENSION_POLL_TIMEOUT_MILLIS   = local.appConfigExtensionPollTimeoutMilliSeconds

    # Datadog
    # DD_LAMBDA_HANDLER = "./../../src/graphql/Lending/resolver-lambdas/main.handler"
    # DD_API_KEY = var.dd_api_key
    # DD_TRACE_ENABLED = true
    # DD_MERGE_XRAY_TRACES = true
    # DD_FLUSH_TO_LOG      = true
    # DD_ENHANCED_METRICS  = true  
  }
  layer_arns = [
    "${local.appconfigLambdaExtension}",
    "${aws_lambda_layer_version.czl74b-libs-resolver.arn}",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Extension:9",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Node14-x:62"
  ]
}

# Business Function Lambdas \/\/\/\/\/\/\/

module "czl74b-bal_lambda" {
  source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
  namespace   = module.namespace.lower_resource_name
  name        = "czl74b-bal_lambda"
  environment = var.environment
  code        = "./../../src/apis/business/.webpack/read-balances-snapshot/read-balances-snapshot"
  # code_requires_zipping = false
  handler         = "main.handler"
  runtime         = "${local.nodejs_compatible_runtimes}"
  size            = "s"
  timeout         = 600
  lambda_insights = true
  vpc_cidr_code   = lookup(var.awsacct_cidr_code, var.environment)
  tags            = module.namespace.tags
  short_tags      = module.namespace.short_tags
  env_vars = {
    ENVIRONMENT = var.environment
    URIS = jsonencode({
      balanceInquiry : "${local.czl74b-bal-inquiry_alias[var.environment]}"
    })
    # AppConfig Configurations - Used by multiple lamba Layers
    APPCONFIG_LOCALHOST                           = "http://localhost:${local.appConfigExtensionPort}/applications/${local.applicationName}/environments/${local.environmentName}/configurations/${local.configurationName}"
    AWS_APPCONFIG_EXTENSION_POLL_INTERVAL_SECONDS = local.appConfigExtensionPollIntervalSeconds
    AWS_APPCONFIG_EXTENSION_POLL_TIMEOUT_MILLIS   = local.appConfigExtensionPollTimeoutMilliSeconds
    # Datadog
    # DD_LAMBDA_HANDLER = "./../../src/apis/business/read-balances-snapshot/resolver-lambdas/main.handler"
    # DD_API_KEY = var.dd_api_key
    # DD_TRACE_ENABLED = true
    # DD_MERGE_XRAY_TRACES = true
    # DD_FLUSH_TO_LOG      = true
    # DD_ENHANCED_METRICS  = true
  }
  layer_arns = [
    "${local.appconfigLambdaExtension}",
    "${aws_lambda_layer_version.czl74b-libs-business.arn}",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Extension:9",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Node14-x:62"
  ]
}

module "czl74b-pay_lambda" {
  source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
  namespace   = module.namespace.lower_resource_name
  name        = "czl74b-pay_lambda"
  environment = var.environment
  code        = "./../../src/apis/business/.webpack/read-payments-snapshot/read-payments-snapshot"
  # code_requires_zipping = false
  handler         = "main.handler"
  runtime         = "${local.nodejs_compatible_runtimes}"
  size            = "s"
  timeout         = 600
  lambda_insights = true
  vpc_cidr_code   = lookup(var.awsacct_cidr_code, var.environment)
  tags            = module.namespace.tags
  short_tags      = module.namespace.short_tags
  env_vars = {
    ENVIRONMENT = var.environment
    URIS = jsonencode({
      getCreditPaymentInfo : "${local.czl74b-creditPayInfo_alias[var.environment]}"
    })
    # AppConfig Configurations - Used by multiple lamba Layers
    APPCONFIG_LOCALHOST                           = "http://localhost:${local.appConfigExtensionPort}/applications/${local.applicationName}/environments/${local.environmentName}/configurations/${local.configurationName}"
    AWS_APPCONFIG_EXTENSION_POLL_INTERVAL_SECONDS = local.appConfigExtensionPollIntervalSeconds
    AWS_APPCONFIG_EXTENSION_POLL_TIMEOUT_MILLIS   = local.appConfigExtensionPollTimeoutMilliSeconds
    # Datadog
    # DD_LAMBDA_HANDLER = "./../../src/apis/business/read-payments-snapshot/main.handler"
    # DD_API_KEY = var.dd_api_key
    # DD_TRACE_ENABLED = true
    # DD_MERGE_XRAY_TRACES = true
    # DD_FLUSH_TO_LOG      = true
    # DD_ENHANCED_METRICS  = true
  }
  layer_arns = [
    "${local.appconfigLambdaExtension}",
    "${aws_lambda_layer_version.czl74b-libs-business.arn}",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Extension:9",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Node14-x:62"
  ]
}

# SOR Function Lambdas \/\/\/\/\/\/\/

module "czl74b-bal-inquiry" {
  source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
  namespace   = module.namespace.lower_resource_name
  name        = "czl74b-bal-inquiry"
  environment = var.environment
  code        = "./../../src/apis/sor/.webpack/balance-inquiry/balance-inquiry"
  # code_requires_zipping = false
  handler         = "main.handler"
  runtime         = "${local.nodejs_compatible_runtimes}"
  size            = "s"
  timeout         = 600
  lambda_insights = true
  vpc_cidr_code   = lookup(var.awsacct_cidr_code, var.environment)
  tags            = module.namespace.tags
  short_tags      = module.namespace.short_tags
  env_vars = {
    ENVIRONMENT = var.environment
    URIS = jsonencode({
      balanceInquiryAdvanceI2CURL : "${local.balanceInquiryAdvanceI2CURL[var.environment]}"
    })
    # AppConfig Configurations - Used by multiple lamba Layers
    APPCONFIG_LOCALHOST                           = "http://localhost:${local.appConfigExtensionPort}/applications/${local.applicationName}/environments/${local.environmentName}/configurations/${local.configurationName}"
    AWS_APPCONFIG_EXTENSION_POLL_INTERVAL_SECONDS = local.appConfigExtensionPollIntervalSeconds
    AWS_APPCONFIG_EXTENSION_POLL_TIMEOUT_MILLIS   = local.appConfigExtensionPollTimeoutMilliSeconds
    # Secrets Manager
    # SECRET_NAME = data.aws_secretsmanager_secret.secret.id
    # Datadog
    # DD_LAMBDA_HANDLER = "./../../src/apis/sor/balance-inquiry/main.handler"
    # DD_API_KEY = var.dd_api_key
    # DD_TRACE_ENABLED = true
    # DD_MERGE_XRAY_TRACES = true
    # DD_FLUSH_TO_LOG      = true
    # DD_ENHANCED_METRICS  = true
  }
  layer_arns = [
    "${local.appconfigLambdaExtension}",
    "${aws_lambda_layer_version.czl74b-libs-sor.arn}",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Extension:9",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Node14-x:62"
  ]
}

module "czl74b-creditPayInfo" {
  source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
  namespace   = module.namespace.lower_resource_name
  name        = "czl74b-creditPayInfo"
  environment = var.environment
  code        = "./../../src/apis/sor/.webpack/get-credit-payment-info/get-credit-payment-info"
  # code_requires_zipping = false
  handler         = "main.handler"
  runtime         = "${local.nodejs_compatible_runtimes}"
  size            = "s"
  timeout         = 600
  lambda_insights = true
  vpc_cidr_code   = lookup(var.awsacct_cidr_code, var.environment)
  tags            = module.namespace.tags
  short_tags      = module.namespace.short_tags
  env_vars = {
    ENVIRONMENT = var.environment
    URIS = jsonencode({
      getCreditPaymentInfoI2CURL : "${local.getCreditPaymentInfoI2CURL[var.environment]}"
    })
    # AppConfig Configurations - Used by multiple lamba Layers
    APPCONFIG_LOCALHOST                           = "http://localhost:${local.appConfigExtensionPort}/applications/${local.applicationName}/environments/${local.environmentName}/configurations/${local.configurationName}"
    AWS_APPCONFIG_EXTENSION_POLL_INTERVAL_SECONDS = local.appConfigExtensionPollIntervalSeconds
    AWS_APPCONFIG_EXTENSION_POLL_TIMEOUT_MILLIS   = local.appConfigExtensionPollTimeoutMilliSeconds
    # Secrets Manager
    # SECRET_NAME = data.aws_secretsmanager_secret.secret.id
    # Datadog
    # DD_LAMBDA_HANDLER = "./../../src/apis/sor/get-credit-payment-info/main.handler"
    # DD_API_KEY = var.dd_api_key
    # DD_TRACE_ENABLED = true
    # DD_MERGE_XRAY_TRACES = true
    # DD_FLUSH_TO_LOG      = true
    # DD_ENHANCED_METRICS  = true
  }
  layer_arns = [
    "${local.appconfigLambdaExtension}",
    "${aws_lambda_layer_version.czl74b-libs-sor.arn}",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Extension:9",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Node14-x:62"
  ]
}



