# #############
# # Resolvers #
# #############

resource "aws_appsync_resolver" "czl74b_Query_partyByAccountNumber" {
  api_id      = aws_appsync_graphql_api.CZL74B-ally-party.id
  type        = "Query"
  field       = "customerByAllyCustomerID"
  data_source = aws_appsync_datasource.czl74b_Party.name
  # request_template  = "${file("./../../src/graphql/Party/resolver-templates/Query.partyByAccountNumber.req.vtl")}"
  response_template = "${file("./../../src/graphql/Party/resolver-templates/Query.partyByAccountNumber.res.vtl")}"
}

resource "aws_appsync_resolver" "czl74b_Query__service" {
  api_id      = aws_appsync_graphql_api.CZL74B-ally-party.id
  type        = "Query"
  field       = "_service"
  data_source = aws_appsync_datasource.czl74b_Party.name
}

# resource "aws_appsync_resolver" "czl74b_Query___typename" {
#   api_id            = aws_appsync_graphql_api.CZL74B-ally-party.id
#   type              = "Query"
#   field             = "__typename"
#   data_source       = aws_appsync_datasource.czl74b_Party.name
# }

resource "aws_appsync_resolver" "czl74b_Query__entities" {
  api_id      = aws_appsync_graphql_api.CZL74B-ally-party.id
  type        = "Query"
  field       = "_entities"
  data_source = aws_appsync_datasource.czl74b_Party.name
}

resource "aws_appsync_resolver" "czl74b_Party_accounts" {
  api_id      = aws_appsync_graphql_api.CZL74B-ally-party.id
  type        = "Customer"
  field       = "accounts"
  data_source = aws_appsync_datasource.czl74b_Party.name
}