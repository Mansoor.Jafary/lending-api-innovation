# #############
# Cloud Watch #
# #############

### Setup Cloud Watch for monitoring ###
resource "aws_cloudwatch_dashboard" "main" {
  dashboard_name = "${module.czl74b-hello_lambda.name}-Alert-Trigger"
  dashboard_body = <<EOF
  {
    "start": "-PT24H",
    "periodOverride": "inherit",
    "widgets": [
        {
            "type": "metric",
            "x": 0,
            "y": 24,
            "width": 24,
            "height": 3,
            "properties": {
                "metrics": [
                    [ "AWS/Lambda", "Duration", "FunctionName", "${module.czl74b-hello_lambda.name}", { "stat": "p95", "period": 300, "yAxis": "left" } ]
                ],
                "view": "timeSeries",
                "stacked": true,
                "region": "us-east-1",
                "period": 300,
                "yAxis": {
                    "left": {
                        "min": 50,
                        "max": 20000
                    }
                },
                "setPeriodToTimeRange": true,
                "title": "Alert-Trigger Batch Process Duration",
                "legend": {
                    "position": "right"
                }
            }
        },
        {
            "type": "metric",
            "x": 0,
            "y": 0,
            "width": 24,
            "height": 3,
            "properties": {
                "metrics": [
                    [ "AWS/ApiGateway", "4XXError", "ApiName", "${module.api-gateway.rest_api.name}", { "stat": "Sum" } ],
                    [ ".", "5XXError", ".", ".", { "stat": "Sum" } ],
                    [ ".", "Count", ".", ".", { "stat": "Sum" } ],
                    [ ".", "IntegrationLatency", ".", ".", { "stat": "p99" } ],
                    [ ".", "Latency", ".", ".", { "stat": "p99" } ]
                ],
                "view": "singleValue",
                "region": "us-east-1",
                "period": 300,
                "title": "API GW - 4XXError, 5XXError, Count, IntegrationLatency, Latency"
            }
        },
        {
            "type": "metric",
            "x": 0,
            "y": 9,
            "width": 24,
            "height": 3,
            "properties": {
                "view": "singleValue",
                "metrics": [
                    [ "AWS/Lambda", "Errors", "FunctionName", "${module.czl74b-hello_lambda.name}", { "stat": "Sum" } ],
                    [ ".", "Throttles", ".", ".", { "stat": "Sum" } ],
                    [ ".", "Invocations", ".", ".", { "stat": "Sum" } ],
                    [ ".", "Duration", ".", ".", { "stat": "SampleCount", "label": "Duration-Avg" } ],
                    [ ".", "ConcurrentExecutions", ".", ".", { "stat": "Sum", "label": "ConcurrentExecutions-Avg" } ],
                    [ "...", { "stat": "Maximum", "label": "ConcurrentExecutions-Max" } ]
                ],
                "view": "singleValue",
                "region": "us-east-1",
                "title": "Trigger Alert Lambda - Errors, Throttles"
            }
        }
     ]
  }
  EOF
}