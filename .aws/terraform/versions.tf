terraform {
  required_version = ">= 0.13"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.70"
    }
    apigee = {
      # This URL does not resolve but it tells Terraform where to find the Apigee provider on the filesystem
      source  = "terraform-registry.services.ally.com/ally/apigee"
      version = "~> 0.0.22"
    }
    publish = {
      source  = "terraform-registry.services.ally.com/scastria/apigee"
      version = "0.1.41"
    }
    # local = {
    #   source  = "hashicorp/local"
    #   version = "~> 1.4"
    # }
    # random = {
    #   source  = "hashicorp/random"
    #   version = "~> 2.2"
    # }
    # template = {
    #   source  = "hashicorp/template"
    #   version = "~> 2.2.0"
    # }
    # archive = {
    #   source = "hashicorp/archive"
    #   version = "~> 2.1.0"
    # }
  }
}
