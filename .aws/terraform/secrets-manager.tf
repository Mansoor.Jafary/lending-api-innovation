# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-secretsmanager/browse
module "secretsmanager" {
  source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-secretsmanager.git?ref=v3"
  namespace   = module.namespace.lower_resource_name
  name        = local.secretsmanager_name
  environment = var.environment
  kms_key_arn = module.encryption.key.arn
  tags        = module.namespace.tags
}