# Environment based pointers to Lambda Function Aliases
locals {
  czl74b-party_ds_function_arn = {
    default = "${aws_lambda_alias.czl74b-party_dev.arn}"
    dev     = "${aws_lambda_alias.czl74b-party_dev.arn}"
    qa      = "${aws_lambda_alias.czl74b-party_qa.arn}"
    cap     = "${aws_lambda_alias.czl74b-party_qa.arn}"
    psp     = "${aws_lambda_alias.czl74b-party_qa.arn}"
    prod    = "${aws_lambda_alias.czl74b-party_prod.arn}"
  }
}