
# #############
# # Resolvers #
# #############

resource "aws_appsync_resolver" "hello" {
  depends_on = [
    aws_appsync_datasource.CZL74B_hello_datasource
  ]
  for_each    = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id      = each.value.id
  type        = "Query"
  field       = "hello"
  data_source = "${each.value.tags_all.deployment_color=="blue" ? "CZL74B_hello_datasourceblue" : "CZL74B_hello_datasourcegreen"}"
  # request_template  = "${file("./../../src/graphql/Lending/resolver-templates/Query.hello.req.vtl")}"
  # response_template = "${file("./../../src/graphql/Lending/resolver-templates/Query.hello.res.vtl")}"
}

# resource "aws_appsync_resolver" "bgTest" {
#   depends_on = [
#     aws_appsync_datasource.czl74b_lending
#   ]
#   for_each    = aws_appsync_graphql_api.CZL74B-ally-lending
#   api_id      = each.value.id
#   type        = "Query"
#   field       = "bgTest"
#   data_source = "${each.value.tags_all.deployment_color=="blue" ? "czl74b_lendingblue" : "czl74b_lendinggreen"}"
# }

resource "aws_appsync_resolver" "helloPipeline" {
  kind              = "PIPELINE"
  for_each    = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id      = each.value.id
  type              = "Query"
  field             = "helloPipeline"
  request_template  = "${file("./../../src/graphql/Lending/resolver-templates/Query.hello.req.vtl")}"
  response_template = "${file("./../../src/graphql/Lending/resolver-templates/Query.hello.res.vtl")}"
  pipeline_config {
    functions = [
      lookup(local.helloPipeFunc_ids, each.value.tags_all.deployment_color=="green" ? local.green_schema : local.blue_schema)      
    ]
  }
}

resource "aws_appsync_resolver" "fineGrainAuthFail" {
  depends_on = [
    aws_appsync_datasource.CZL74B_fineGrainAuthFail_datasource
  ]
  kind              = "UNIT"
  for_each    = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id      = each.value.id
  type              = "Query"
  field             = "fineGrainAuthFail"
  data_source = "${each.value.tags_all.deployment_color=="blue" ? "CZL74B_fineGrainAuthFail_datasourceblue" : "CZL74B_fineGrainAuthFail_datasourcegreen"}"
  request_template  = "${file("./../../src/graphql/Lending/resolver-templates/Query.fineGrainAuthFail.req.vtl")}"
  response_template = "${file("./../../src/graphql/Lending/resolver-templates/Query.fineGrainAuthFail.res.vtl")}"
}

# demo artifacts below this

# this one will use the single data source and lambda for resolver functions
resource "aws_appsync_resolver" "czl74b_Lending_Query__Lservice" {
  depends_on = [
    aws_appsync_datasource.czl74b_lending
  ]
  for_each    = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id      = each.value.id
  type        = "Query"
  field       = "_service"
  data_source = "${each.value.tags_all.deployment_color=="blue" ? "czl74b_lendingblue" : "czl74b_lendinggreen"}"
}

resource "aws_appsync_resolver" "czl74b_Lending_Query__entities" {
  depends_on = [
    aws_appsync_datasource.czl74b_lending
  ]  
  for_each    = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id      = each.value.id
  type        = "Query"
  field       = "_entities"
  data_source = "${each.value.tags_all.deployment_color=="blue" ? "czl74b_lendingblue" : "czl74b_lendinggreen"}"
}

resource "aws_appsync_resolver" "czl74b_prodAcctByAcctNum" {
  depends_on = [
    aws_appsync_datasource.czl74b_lending
  ]
  for_each    = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id      = each.value.id
  type        = "Query"
  field       = "accountByAccountID"
  data_source = "${each.value.tags_all.deployment_color=="blue" ? "czl74b_lendingblue" : "czl74b_lendinggreen"}"
  # request_template  = "${file("./../../src/graphql/Lending/resolver-templates/Query.productAccountByAccountNumber.req.vtl")}"
  response_template = "${file("./../../src/graphql/Lending/resolver-templates/Query.productAccountByAccountNumber.res.vtl")}"
}

resource "aws_appsync_resolver" "czl74b_createNewLoansUploadURL" {
  depends_on = [
    aws_appsync_datasource.czl74b_lending
  ]
  for_each    = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id      = each.value.id
  type              = "Mutation"
  field             = "createNewInstallmentLoansUploadURL"
  data_source = "${each.value.tags_all.deployment_color=="blue" ? "czl74b_lendingblue" : "czl74b_lendinggreen"}"
  response_template = "${file("./../../src/graphql/Lending/resolver-templates/Mutation.createNewInstallmentLoansUploadURL.res.vtl")}"
}

resource "aws_appsync_resolver" "czl74b_financialPosition" {
  depends_on = [
    aws_appsync_datasource.czl74b_lending
  ]
  for_each    = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id      = each.value.id
  type        = "LineOfCreditAccount"
  field       = "financialPosition"
  data_source = "${each.value.tags_all.deployment_color=="blue" ? "czl74b_lendingblue" : "czl74b_lendinggreen"}"
  # request_template  = "${file("./../../src/graphql/Lending/resolver-templates/LineOfCreditAccount.financialPosition.req.vtl")}"
  # response_template = "${file("./../../src/graphql/Lending/resolver-templates/LineOfCreditAccount.financialPosition.res.vtl")}"
}

resource "aws_appsync_resolver" "czl74b_balances" {
  depends_on = [
    aws_appsync_datasource.czl74b_lending
  ]
  for_each    = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id      = each.value.id
  type              = "LineOfCreditAccountFinancialPosition"
  field             = "balancesPosition"
  data_source = "${each.value.tags_all.deployment_color=="blue" ? "czl74b_lendingblue" : "czl74b_lendinggreen"}"
  request_template  = "${file("./../../src/graphql/Lending/resolver-templates/LineOfCreditAccountFinancialPosition.balancesPosition.req.vtl")}"
  response_template = "${file("./../../src/graphql/Lending/resolver-templates/LineOfCreditAccountFinancialPosition.balancesPosition.res.vtl")}"
}

resource "aws_appsync_resolver" "czl74b_payments" {
  depends_on = [
    aws_appsync_datasource.czl74b_payments
  ]
  for_each    = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id      = each.value.id
  type              = "LineOfCreditAccountFinancialPosition"
  field             = "paymentsPosition"
  data_source = "${each.value.tags_all.deployment_color=="blue" ? "CZL74B_paymentsblue" : "CZL74B_paymentsgreen"}"
  request_template  = "${file("./../../src/graphql/Lending/resolver-templates/LineOfCreditAccountFinancialPosition.paymentsPosition.req.vtl")}"
  response_template = "${file("./../../src/graphql/Lending/resolver-templates/LineOfCreditAccountFinancialPosition.paymentsPosition.res.vtl")}"
}

resource "aws_appsync_resolver" "czl74b_metrics" {
  depends_on = [ 
    aws_appsync_datasource.CZL74B_metrics_datasource
  ]
  kind              = "UNIT"
  for_each    = aws_appsync_graphql_api.CZL74B-ally-lending
  api_id      = each.value.id
  type              = "BalancesPosition"
  field             = "metrics"
  data_source = "${each.value.tags_all.deployment_color=="blue" ? "CZL74B_metrics_datasourceblue" : "CZL74B_metrics_datasourcegreen"}"
  request_template  = "${file("./../../src/graphql/Lending/resolver-templates/BalancesPosition.metrics.req.vtl")}"
  response_template = "${file("./../../src/graphql/Lending/resolver-templates/BalancesPosition.metrics.res.vtl")}"
}
