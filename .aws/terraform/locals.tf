# Global Runtimes
locals {
  nodejs_compatible_runtimes = "nodejs14.x"
}
# Name for secrets manager
locals {
  secretsmanager_name = "czl74b_v38"
}

# AppConfig Lambda Extension

locals {
  appconfigLambdaExtension                  = "arn:aws:lambda:us-east-1:027255383542:layer:AWS-AppConfig-Extension:55"
  applicationName                           = "${aws_appconfig_application.czl74b-lambda_config.name}"
  environmentName                           = "${aws_appconfig_environment.czl74b-lambda_config.name}"
  configurationName                         = "${aws_appconfig_configuration_profile.czl74b-lambda_config.name}"
  appConfigExtensionPort                    = 2772
  appConfigExtensionPollIntervalSeconds     = 45
  appConfigExtensionPollTimeoutMilliSeconds = 3000
}

## Resolver Function ARNS
locals {
  # Lending Resolver Function Blue
  czl74b-lending_ds_function_arn_blue = {
    default = "${aws_lambda_alias.czl74b-lending_dev_blue.arn}"
    dev     = "${aws_lambda_alias.czl74b-lending_dev_blue.arn}"
    qa      = "${aws_lambda_alias.czl74b-lending_qa_blue.arn}"
    cap     = "${aws_lambda_alias.czl74b-lending_qa_blue.arn}"
    psp     = "${aws_lambda_alias.czl74b-lending_qa_blue.arn}"
    prod    = "${aws_lambda_alias.czl74b-lending_prod_blue.arn}"
  }
}

locals {
  # Lending Resolver Function Green
  czl74b-lending_ds_function_arn_green = {
    default = "${aws_lambda_alias.czl74b-lending_dev_green.arn}"
    dev     = "${aws_lambda_alias.czl74b-lending_dev_green.arn}"
    qa      = "${aws_lambda_alias.czl74b-lending_qa_green.arn}"
    cap     = "${aws_lambda_alias.czl74b-lending_qa_green.arn}"
    psp     = "${aws_lambda_alias.czl74b-lending_qa_green.arn}"
    prod    = "${aws_lambda_alias.czl74b-lending_prod_green.arn}"
  }
}

## Business FUNCTION ARNS
locals {
  # balances business function
  czl74b-bal_lambda_alias = {
    default = "${aws_lambda_alias.czl74b-bal_lambda_dev.arn}"
    dev     = "${aws_lambda_alias.czl74b-bal_lambda_dev.arn}"
    qa      = "${aws_lambda_alias.czl74b-bal_lambda_qa.arn}"
    cap     = "${aws_lambda_alias.czl74b-bal_lambda_qa.arn}"
    psp     = "${aws_lambda_alias.czl74b-bal_lambda_qa.arn}"
    prod    = "${aws_lambda_alias.czl74b-bal_lambda_prod.arn}"
  }
  # payment business function
  czl74b-pay_lambda_alias = {
    default = "${aws_lambda_alias.czl74b-pay_lambda_dev_blue.arn}"
    dev     = "${aws_lambda_alias.czl74b-pay_lambda_dev_blue.arn}"
    qa      = "${aws_lambda_alias.czl74b-pay_lambda_qa_blue.arn}"
    cap     = "${aws_lambda_alias.czl74b-pay_lambda_qa_blue.arn}"
    psp     = "${aws_lambda_alias.czl74b-pay_lambda_qa_blue.arn}"
    prod    = "${aws_lambda_alias.czl74b-pay_lambda_prod_blue.arn}"
  }
}

## SOR FUNCTION ARNS
locals {
  # balances sor function
  czl74b-bal-inquiry_alias = {
    default : "${aws_lambda_alias.czl74b-bal-inquiry_dev.arn}",
    dev : "${aws_lambda_alias.czl74b-bal-inquiry_dev.arn}",
    qa : "${aws_lambda_alias.czl74b-bal-inquiry_qa.arn}",
    cap : "${aws_lambda_alias.czl74b-bal-inquiry_qa.arn}",
    psp : "${aws_lambda_alias.czl74b-bal-inquiry_qa.arn}",
    prod : "${aws_lambda_alias.czl74b-bal-inquiry_prod.arn}"
  }
  # payment sor function
  czl74b-creditPayInfo_alias = {
    default : "${aws_lambda_alias.czl74b-creditPayInfo_dev.arn}",
    dev : "${aws_lambda_alias.czl74b-creditPayInfo_dev.arn}",
    qa : "${aws_lambda_alias.czl74b-creditPayInfo_qa.arn}",
    cap : "${aws_lambda_alias.czl74b-creditPayInfo_qa.arn}",
    psp : "${aws_lambda_alias.czl74b-creditPayInfo_qa.arn}",
    prod : "${aws_lambda_alias.czl74b-creditPayInfo_prod.arn}"
  }
}

## HTTP API URLS
locals {
  balanceInquiryAdvanceI2CURL = {
    default : "https://ws2.mycardplace.com:6443/MCPWebServicesRestful/services/MCPWSHandler/balanceInquiryAdvance",
    dev : "https://ws2.mycardplace.com:6443/MCPWebServicesRestful/services/MCPWSHandler/balanceInquiryAdvance",
    qa : "https://ws2.mycardplace.com:6443/MCPWebServicesRestful/services/MCPWSHandler/balanceInquiryAdvance",
    cap : "https://ws2.mycardplace.com:6443/MCPWebServicesRestful/services/MCPWSHandler/balanceInquiryAdvance",
    psp : "https://ws2.mycardplace.com:6443/MCPWebServicesRestful/services/MCPWSHandler/balanceInquiryAdvance",
    prod : "https://ws2.mycardplace.com:6443/MCPWebServicesRestful/services/MCPWSHandler/balanceInquiryAdvance"
  }
  getCreditPaymentInfoI2CURL = {
    default : "https://ws2.mycardplace.com:6443/MCPWebServicesRestful/services/MCPWSHandler/getCreditPaymentInfo",
    dev : "https://ws2.mycardplace.com:6443/MCPWebServicesRestful/services/MCPWSHandler/getCreditPaymentInfo",
    qa : "https://ws2.mycardplace.com:6443/MCPWebServicesRestful/services/MCPWSHandler/getCreditPaymentInfo",
    cap : "https://ws2.mycardplace.com:6443/MCPWebServicesRestful/services/MCPWSHandler/getCreditPaymentInfo",
    psp : "https://ws2.mycardplace.com:6443/MCPWebServicesRestful/services/MCPWSHandler/getCreditPaymentInfo",
    prod : "https://ws2.mycardplace.com:6443/MCPWebServicesRestful/services/MCPWSHandler/getCreditPaymentInfo"
  }
}

# Runtime Configurations for all lambda functions e.g. logging level etc. These are loaded via AppConfig
locals {
  globalAppConfigs = {
    apiCallerTimeout : {
      default : 8000,
      dev : 8000,
      qa : 8000,
      cap : 8000,
      psp : 8000,
      prod : 3000
    },
    # functionCallerTimeout: {
    #   default: 100000,
    #   dev: 100000,
    #   qa: 10000,
    #   cap: 10000,
    #   psp: 10000,
    #   prod: 6000    
    # },
    logLevel : {
      default : "DEBUG",
      dev : "DEBUG",
      qa : "DEBUG",
      cap : "DEBUG",
      psp : "DEBUG",
      prod : "ERROR"
    }
  }
  czl74bBalInquiry = {
    mockResponse : {
      default : "TRUE",
      dev : "TRUE",
      qa : "TRUE",
      cap : "TRUE",
      psp : "TRUE",
      prod : "FALSE"
    }
  }
  czl74bCreditPayInfo = {
    mockResponse : {
      default : "TRUE",
      dev : "TRUE",
      qa : "TRUE",
      cap : "TRUE",
      psp : "TRUE",
      prod : "FALSE"
    }
  }
}

# AppConfig Content - Entire object is available to Lambda Function Code
locals {
  appConfigs = {
    globalAppConfigs : {
      apiCallerTimeout : "${local.globalAppConfigs.apiCallerTimeout[var.environment]}",
      # functionCallerTimeout: "${local.globalAppConfigs.functionCallerTimeout[var.environment]}",
      logLevel : "${local.globalAppConfigs.logLevel[var.environment]}"
    },
    czl74bBalInquiry : {
      mockResponse : "${local.czl74bBalInquiry.mockResponse[var.environment]}"
    },
    czl74bCreditPayInfo : {
      mockResponse : "${local.czl74bCreditPayInfo.mockResponse[var.environment]}"
    }
  }
}