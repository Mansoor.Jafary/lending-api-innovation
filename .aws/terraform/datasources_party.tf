# ################
# # Data Sources #
# ################

# Data source for apollo resolver reference
resource "aws_appsync_datasource" "czl74b_Party" {
  api_id           = aws_appsync_graphql_api.CZL74B-ally-party.id
  name             = "czl74b_Party"
  service_role_arn = "${aws_iam_role.CZL74B_role.arn}"
  type             = "AWS_LAMBDA"
  lambda_config {
    # The arn of the lambda function
    function_arn = "${local.czl74b-party_ds_function_arn[var.environment]}"
  }
}