locals {
  risk                          = lower(module.namespace.tags.issrcl_level)
  dast                          = false
  unstable_env                  = true # this flag is being used for hotfix when being pushed from branch other than develop
  allow_devportal_auto_register = true
  set_devportal_auto_register   = true
  access                        = "public" // public private or internal
  allow_apigee_auto_register    = "auto"
}

module "common" {
  source       = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-apigee.git//modules/common?ref=v1"
  token        = var.apigee_access_token
  namespace    = module.namespace.lower_resource_name
  environment  = var.environment
  display_name = module.namespace.lower_resource_name
  type         = "api"
}

resource "apigee_product" "czl74b-lending-admin" {
  depends_on    = [module.api_proxy_graphql]
  provider      = apigee
  name          = "${module.namespace.lower_resource_name}-czl74b-lending-admin"
  display_name  = "${module.namespace.lower_resource_name}-czl74b-lending-admin"
  approval_type = "auto"
  description   = var.api_description
  api_resources = ["/"] # Supports the base path and all sub-URIs
  scopes        = []
  proxies       = [module.common.name]
  attributes = {
    access             = "private" # limit the visible API products that teams can register for in developer portal. 
    api_classification = local.risk
    api_protect_scope  = local.risk
    checkmarx_verified = local.dast
  }
}

# ADMIN Testing App
resource "apigee_developer_app" "czl74b-lending-admin-app" {
  depends_on      = [apigee_product.czl74b-lending-admin]
  name            = "${module.namespace.lower_resource_name}-czl74b-lending-admin"
  developer_email = apigee_developer.developer.email
  api_products = [
    apigee_product.czl74b-lending-admin.name,
    "Customer Authentication API"
  ]
  key_expires_in = 1602860518014
  attributes = {
    clientId    = "admin"
    DisplayName = "${module.namespace.lower_resource_name}-czl74b-lending-admin"
    Notes       = "Admin testing App"
  }
}

resource "apigee_developer" "developer" {
  email      = "DigitalAppOps-${module.namespace.lower_short_name}@ally.com"
  first_name = "CZL74B"
  last_name  = "Developer"
  user_name  = "${module.namespace.lower_short_name}-app"
}