# locals {
#   czl74b-authForAS_function_arn = {
#     default = "${aws_lambda_alias.czl74b_authforas_dev.arn}"
#     dev     = "${aws_lambda_alias.czl74b_authforas_dev.arn}"
#     qa      = "${aws_lambda_alias.czl74b_authforas_qa.arn}"
#     cap     = "${aws_lambda_alias.czl74b_authforas_qa.arn}"
#     psp     = "${aws_lambda_alias.czl74b_authforas_qa.arn}"
#     prod    = "${aws_lambda_alias.czl74b_authforas_prod.arn}"
#   }
# }

# # Create the Lambda Alias for AppSync Lambda Authorizer
# # Aliases created for dev, qa and prod
# resource "aws_lambda_alias" "czl74b_authforas_dev" {
#   name             = "czl74b_authforas_dev"
#   description      = "Alias to manage dev traffic"
#   function_name    = module.czl74b_authforas.arn
#   function_version = "$LATEST"

#   # routing_config {
#   #   additional_version_weights = {
#   #     "2" = 0.5
#   #   }
#   # }
# }

# resource "aws_lambda_alias" "czl74b_authforas_qa" {
#   name             = "czl74b_authforas_qa"
#   description      = "Alias to manage qa traffic"
#   function_name    = module.czl74b_authforas.arn
#   function_version = "$LATEST"

#   # routing_config {
#   #   additional_version_weights = {
#   #     "2" = 0.5
#   #   }
#   # }
# }

# resource "aws_lambda_alias" "czl74b_authforas_prod" {
#   name             = "czl74b_authforas_prod"
#   description      = "Alias to manage prod traffic"
#   function_name    = module.czl74b_authforas.arn
#   function_version = "$LATEST"

#   # routing_config {
#   #   additional_version_weights = {
#   #     "2" = 0.5
#   #   }
#   # }
# }

# Create the Lambda Authorizer for AppSync
module "czl74b_authforas" {
  source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
  namespace   = module.namespace.lower_resource_name
  name        = "czl74b_authforas"
  environment = var.environment

  code = "./../../src/appsync-authorizer/.webpack/appsync-authorizer"
  # code_requires_zipping = false
  handler       = "main.handler"
  runtime       = "nodejs14.x"
  size          = "s"
  timeout       = 600
  vpc_cidr_code = lookup(var.awsacct_cidr_code, var.environment)
  tags          = module.namespace.tags
  short_tags    = module.namespace.short_tags
  env_vars = {
    APIGEE_KEY = "${random_password.header_token.result}"
  }
  layer_arns = [
    "${aws_lambda_layer_version.czl74b-libs-appsync-auth.arn}",
  ]
}

# Permission for AppSync to access Auth Lambda Function
resource "aws_lambda_permission" "lambdaAuthForAS" {
  action        = "lambda:InvokeFunction"
  function_name = module.czl74b_authforas.name
  principal     = "appsync.amazonaws.com"
  # source_arn    = aws_appsync_graphql_api.CZL74B-ally-lending[count.index].arn
}