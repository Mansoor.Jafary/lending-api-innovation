#######################
# AppSync GraphQL API #
#######################

variable "impl_blue_lending_appsync" {
  default = true
  type    = bool
}

variable "impl_green_lending_appsync" {
  default = true
  type    = bool
}

locals {
  schema = "./../../src/graphql/Lending/schema/schema.graphql"
  blue_schema = "Blue"
  green_schema = "Green"
}

locals {
  schemas_list = var.impl_blue_lending_appsync ? var.impl_green_lending_appsync ? concat([local.blue_schema], [local.green_schema]) : [local.blue_schema] : var.impl_green_lending_appsync ? [local.green_schema] : []
}

resource "aws_appsync_graphql_api" "CZL74B-ally-lending" {
  for_each = toset(local.schemas_list)
  # count               = var.impl_blue_lending_appsync? 1 : 0
  name                = "CZL74B-ally-lending"
  schema              = "${file(local.schema)}"
  authentication_type = "AWS_LAMBDA"
  # additional_authentication_provider {
  #   authentication_type = "API_KEY"
  # }
  lambda_authorizer_config {
    # authorizer_uri                 = local.czl74b-authForAS_function_arn[var.environment] //Lambda Alias for AppSync Lambda Authorizer is not supported
    authorizer_uri                   = module.czl74b_authforas.arn
    authorizer_result_ttl_in_seconds = 0
  }
  log_config {
    cloudwatch_logs_role_arn = aws_iam_role.CZL74B_role.arn
    field_log_level          = "ERROR"
  }
  tags = merge(
    module.namespace.tags,
    map("deployment_color", each.value=="Blue" ? "blue" : "green")
    )
}

# resource "aws_appsync_api_key" "CZL74B-apigee" {
#   api_id  = aws_appsync_graphql_api.CZL74B-ally-lending.id
#   expires = "2022-05-03T04:00:00Z"
# }

# Token for Apigee
resource "random_password" "header_token" {
  length = 15
  special = false
}
