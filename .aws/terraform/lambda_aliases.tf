resource "aws_lambda_alias" "czl74b-hello_lambda_prod_blue" {
  name             = "czl74b-hello_lambda_prod_blue"
  description      = "Alias to manage prod blue traffic"
  function_name    = module.czl74b-hello_lambda.arn
  function_version = "$LATEST"
  # change to 68

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}
resource "aws_lambda_alias" "czl74b-hello_lambda_prod_green" {
  name             = "czl74b-hello_lambda_prod_green"
  description      = "Alias to manage prod green traffic"
  function_name    = module.czl74b-hello_lambda.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}

# Lending Resolver Lambda Aliases created for dev, qa and prod
resource "aws_lambda_alias" "czl74b-lending_dev_blue" {
  name             = "czl74b-lending_dev_blue"
  description      = "Alias to manage dev blue traffic"
  function_name    = module.czl74b-lending.arn
  function_version = "$LATEST"
  # change to 1

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}
# Lending Resolver Lambda Aliases created for dev, qa and prod
resource "aws_lambda_alias" "czl74b-lending_dev_green" {
  name             = "czl74b-lending_dev_green"
  description      = "Alias to manage dev green traffic"
  function_name    = module.czl74b-lending.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}

resource "aws_lambda_alias" "czl74b-lending_qa_blue" {
  name             = "czl74b-lending_qa_blue"
  description      = "Alias to manage qa blue traffic"
  function_name    = module.czl74b-bal_lambda.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}
resource "aws_lambda_alias" "czl74b-lending_qa_green" {
  name             = "czl74b-lending_qa_green"
  description      = "Alias to manage qa green traffic"
  function_name    = module.czl74b-bal_lambda.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}
resource "aws_lambda_alias" "czl74b-lending_prod_blue" {
  name             = "czl74b-lending_prod_blue"
  description      = "Alias to manage prod blue traffic"
  function_name    = module.czl74b-bal_lambda.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}
resource "aws_lambda_alias" "czl74b-lending_prod_green" {
  name             = "czl74b-lending_prod_green"
  description      = "Alias to manage prod green traffic"
  function_name    = module.czl74b-bal_lambda.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}
# Business Function Lambda Aliases

## Balance Lambda Aliases for each environment
resource "aws_lambda_alias" "czl74b-bal_lambda_dev" {
  name             = "czl74b-bal_lambda_dev"
  description      = "Alias to manage prod traffic"
  function_name    = module.czl74b-bal_lambda.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}
resource "aws_lambda_alias" "czl74b-bal_lambda_qa" {
  name             = "czl74b-bal_lambda_qa"
  description      = "Alias to manage prod traffic"
  function_name    = module.czl74b-bal_lambda.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}
resource "aws_lambda_alias" "czl74b-bal_lambda_prod" {
  name             = "czl74b-bal_lambda_prod"
  description      = "Alias to manage prod traffic"
  function_name    = module.czl74b-bal_lambda.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}

## Payment Info Lambda Aliases for each environment
resource "aws_lambda_alias" "czl74b-pay_lambda_dev_blue" {
  name             = "czl74b-pay_lambda_dev_blue"
  description      = "Alias to manage live blue traffic"
  function_name    = module.czl74b-pay_lambda.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}
resource "aws_lambda_alias" "czl74b-pay_lambda_dev_green" {
  name             = "czl74b-pay_lambda_dev_green"
  description      = "Alias to manage green traffic"
  function_name    = module.czl74b-pay_lambda.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}
resource "aws_lambda_alias" "czl74b-pay_lambda_qa_blue" {
  name             = "czl74b-pay_lambda_qa_blue"
  description      = "Alias to manage live blue traffic"
  function_name    = module.czl74b-pay_lambda.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}
resource "aws_lambda_alias" "czl74b-pay_lambda_qa_green" {
  name             = "czl74b-pay_lambda_qa_green"
  description      = "Alias to manage green traffic"
  function_name    = module.czl74b-pay_lambda.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}
resource "aws_lambda_alias" "czl74b-pay_lambda_prod_blue" {
  name             = "czl74b-pay_lambda_prod_blue"
  description      = "Alias to manage live blue traffic"
  function_name    = module.czl74b-pay_lambda.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}
resource "aws_lambda_alias" "czl74b-pay_lambda_prod_green" {
  name             = "czl74b-pay_lambda_prod_green"
  description      = "Alias to manage green traffic"
  function_name    = module.czl74b-pay_lambda.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}

# SOR Function Lambda Aliases

## Balance Inquiry Lambda Aliases for each environment

resource "aws_lambda_alias" "czl74b-bal-inquiry_dev" {
  name             = "czl74b-bal-inquiry_dev"
  description      = "Alias to manage live traffic for dev environment"
  function_name    = module.czl74b-bal-inquiry.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}

resource "aws_lambda_alias" "czl74b-bal-inquiry_qa" {
  name             = "czl74b-bal-inquiry_qa"
  description      = "Alias to manage live traffic for qa environment"
  function_name    = module.czl74b-bal-inquiry.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}

resource "aws_lambda_alias" "czl74b-bal-inquiry_prod" {
  name             = "czl74b-bal-inquiry_prod"
  description      = "Alias to manage live traffic for prod environment"
  function_name    = module.czl74b-bal-inquiry.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}
## Credit Pay Info Lambda Aliases for each environment

resource "aws_lambda_alias" "czl74b-creditPayInfo_dev" {
  name             = "czl74b-creditPayInfo_dev"
  description      = "Alias to manage live traffic for dev environment"
  function_name    = module.czl74b-creditPayInfo.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}

resource "aws_lambda_alias" "czl74b-creditPayInfo_qa" {
  name             = "czl74b-creditPayInfo_qa"
  description      = "Alias to manage live traffic for qa environment"
  function_name    = module.czl74b-creditPayInfo.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}

resource "aws_lambda_alias" "czl74b-creditPayInfo_prod" {
  name             = "czl74b-creditPayInfo_prod"
  description      = "Alias to manage live traffic for prod"
  function_name    = module.czl74b-creditPayInfo.arn
  function_version = "$LATEST"

  # routing_config {
  #   additional_version_weights = {
  #     "2" = 0.5
  #   }
  # }
}