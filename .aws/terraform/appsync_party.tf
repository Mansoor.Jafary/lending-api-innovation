#######################
# AppSync GraphQL API #
#######################

resource "aws_appsync_graphql_api" "CZL74B-ally-party" {
  name                = "CZL74B-ally-party"
  tags                = module.namespace.tags
  schema              = "${file("./../../src/graphql/Party/schema/schema.graphql")}"
  authentication_type = "API_KEY"
  # Could not find documentation for AWS_LAMBDA Authorization feature.
  # authentication_type  = "AWS_LAMBDA"
}

resource "aws_appsync_api_key" "CZL74B-apigee-party" {
  api_id  = aws_appsync_graphql_api.CZL74B-ally-party.id
  expires = "2022-05-03T04:00:00Z"
}