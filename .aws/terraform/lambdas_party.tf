# #############
# Lambdas     #
# #############

# Resolver Lambdas \/\/\/\/\/\/\/

module "czl74b-party" {
  source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
  namespace   = module.namespace.lower_resource_name
  name        = "czl74b-party"
  environment = var.environment
  code        = "./../../src/resolver/.webpack/party/party"
  # code_requires_zipping = false
  handler       = "main.handler"
  runtime       = "nodejs14.x"
  size          = "s"
  timeout       = 600
  vpc_cidr_code = lookup(var.awsacct_cidr_code, var.environment)
  tags          = module.namespace.tags
  short_tags    = module.namespace.short_tags
  env_vars = {
    SCHEMA_PATH = "/party.graphql",
    ENVIRONMENT = var.environment
  }
  layer_arns = [
    "${aws_lambda_layer_version.czl74b-libs-resolver.arn}",
    "${local.appconfigLambdaExtension}"
  ]
}



