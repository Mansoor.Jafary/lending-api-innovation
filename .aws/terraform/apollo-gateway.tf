

# module "function-get-index" {
#   source                = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v4.2.0"
#   namespace             = module.namespace.lower_short_name
#   tags                  = module.namespace.tags
#   short_tags            = module.namespace.short_tags
#   name                  = "get-index"
#   code                  = "../../.serverless/get-index.zip"
#   lambda_insights       = true
#   code_requires_zipping = false
#   handler               = "/opt/nodejs/node_modules/datadog-lambda-js/handler.handler"
#   runtime               = "nodejs14.x"
#   size                  = "s"
#   timeout               = 10
#   warmer_enabled        = false
#   vpc_cidr_code         = local.account_cidr_code
#   env_vars = {
#     NAMESPACE = module.namespace.lower_short_name
#     ENVIRONMENT = var.environment
#     DD_LAMBDA_HANDLER = "src/generated/lambda/get-index.handler"
#     DD_API_KEY = var.dd_api_key
#     DD_TRACE_ENABLED = true
#     DD_MERGE_XRAY_TRACES = true
#     DD_FLUSH_TO_LOG      = true
#     DD_ENHANCED_METRICS  = true
#   }

#   layer_arns = [
#     "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Extension:9",
#     "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Node14-x:62"
#   ]
# }

# Lending Domain Apollo Gateway Server Lambda
module "gateway-server" {
  source          = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
  namespace       = module.namespace.lower_resource_name
  name            = "gateway-server"
  environment     = var.environment
  code            = "./../../src/apollo-gateway"
  handler         = "main.handler"
  runtime         = "${local.nodejs_compatible_runtimes}"
  size            = "s"
  timeout         = 600
  lambda_insights = true
  vpc_cidr_code   = lookup(var.awsacct_cidr_code, var.environment)
  tags            = module.namespace.tags
  short_tags      = module.namespace.short_tags
  # env_vars = {
  #   # APOLLO Envs for enabling auto sync of schemas with Studio // Need successful testing
  #   # APOLLO_GRAPH_REF = "Lending-API-siyezg@current"
  #   # APOLLO_SCHEMA_REPORTING = true
  #   # APOLLO_KEY = ["service:Lending-API-siyezg:e5wIsGLv1cioXSONTk4_EQ"]
  #   # Datadog
  #   # DD_LAMBDA_HANDLER = "./../../src/apollo-gateway/main.handler"
  #   # DD_API_KEY = var.dd_api_key
  #   # DD_TRACE_ENABLED = true
  #   # DD_MERGE_XRAY_TRACES = true
  #   # DD_FLUSH_TO_LOG      = true
  #   # DD_ENHANCED_METRICS  = true
  # }
  layer_arns = [
    "${aws_lambda_layer_version.czl74b-npm-apollo-libs.arn}",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Extension:9",
    # "arn:aws:lambda:${var.region}:464622532012:layer:Datadog-Node14-x:62"
  ]
}
# Temporary Apollo Server for Demo Schema and Mock Responses
module "apollo-server" {
  source          = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
  namespace       = module.namespace.lower_resource_name
  name            = "apollo-server"
  environment     = var.environment
  code            = "./../../src/apollo-server"
  handler         = "main.handler"
  runtime         = "${local.nodejs_compatible_runtimes}"
  size            = "s"
  timeout         = 600
  lambda_insights = true
  vpc_cidr_code   = lookup(var.awsacct_cidr_code, var.environment)
  tags            = module.namespace.tags
  short_tags      = module.namespace.short_tags
  layer_arns = [
    "${aws_lambda_layer_version.czl74b-npm-apollo-libs.arn}"
  ]
}

# data "template_file" "swagger_file" {
#   template = file("../../swagger.yaml")
#   vars = {
#     credentials = aws_iam_role.api_gateway_role.arn
#     get-index = module.gateway-server.invoke_arn
#   }
# }

# module "czl74b-apollo_gateway" {
#   source                = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-api-gateway.git?ref=v1.9.0"
#   namespace             = module.namespace.lower_short_name
#   name                  = "czl74b-apollo_gateway"
#   tags                  = module.namespace.tags
#   environment           = var.environment
#   cache_cluster_enabled = false
#   api_body              = data.template_file.swagger_file.rendered
# }

resource "aws_apigatewayv2_api" "czl74b-apollo_gateway" {
  name            = "czl74b-apollo_gateway"
  description     = "Apollo API Gateway for Apollo Studio"
  protocol_type   = "HTTP"
  target          = module.gateway-server.arn
  credentials_arn = aws_iam_role.api_gateway_role.arn
}

resource "aws_iam_role" "api_gateway_role" {
  name                 = "${module.namespace.lower_short_name}-apigw-role"
  permissions_boundary = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy/CloudCoreL3Permissions"
  assume_role_policy   = data.aws_iam_policy_document.api_gateway_assume_role_policy.json
  tags                 = module.namespace.tags
}

resource "aws_iam_role_policy" "api_gateway_role_policy" {
  name   = "${module.namespace.lower_short_name}-api_gateway_role_policy"
  role   = aws_iam_role.api_gateway_role.id
  policy = data.aws_iam_policy_document.api_gateway_role_policy.json
}

data "aws_iam_policy_document" "api_gateway_role_policy" {
  version = "2012-10-17"

  statement {
    actions = [
      "lambda:InvokeFunction"
    ]

    effect = "Allow"

    resources = [
      module.gateway-server.arn
    ]
  }
}

# data "aws_iam_policy_document" "api_gateway_policy" {
#   version = "2012-10-17"
#   statement {
#     sid    = "ReadFromApigeeIps"
#     effect = "Allow"
#     principals {
#       type        = "*"
#       identifiers = ["*"]
#     }
#     actions   = ["execute-api:Invoke"]
#     resources = ["execute-api:/*"]
#     condition {
#       test     = "StringLike"
#       variable = "aws:SourceIp"
#       values   = []
#     }
#   }
# }

data "aws_iam_policy_document" "api_gateway_assume_role_policy" {
  version = "2012-10-17"
  statement {
    sid     = "AllowApiGatewayToAssumeRole"
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["apigateway.amazonaws.com"]
    }
  }
}