

# data "aws_api_gateway_resource" "api-gateway" {
#   rest_api_id = module.api-gateway.rest_api.id
#   path   = "/"
# }

# resource "aws_api_gateway_method" "MyDemoMethod" {
#   authorization = aws_api_gateway_authorizer.demo_auth.name
#   http_method   = "GET"
#   resource_id   = data.aws_api_gateway_resource.api-gateway.id
#   rest_api_id   = module.api-gateway.rest_api.id
# }

# resource "aws_api_gateway_method_response" "response_200" {
#   rest_api_id = module.api-gateway.rest_api.id
#   resource_id = data.aws_api_gateway_resource.api-gateway.id
#   http_method = aws_api_gateway_method.MyDemoMethod.http_method
#   status_code = "200"
# }

# resource "aws_api_gateway_integration" "MyDemoIntegration" {
#   rest_api_id             = module.api-gateway.rest_api.id
#   resource_id             = data.aws_api_gateway_resource.api-gateway.id
#   http_method             = aws_api_gateway_method.MyDemoMethod.http_method
#   integration_http_method = "POST"
#   type                    = "AWS"
#   uri                     = module.czl74b-i2c_lambda.invoke_arn
# }

# resource "aws_api_gateway_integration_response" "MyDemoIntegrationResponse" {
#   rest_api_id = module.api-gateway.rest_api.id
#   resource_id = data.aws_api_gateway_resource.api-gateway.id
#   http_method = aws_api_gateway_method.MyDemoMethod.http_method
#   status_code = aws_api_gateway_method_response.response_200.status_code
# }



# Create the Lambda Authorizer for API Gateway
# module "czl74b_authorizer" {
#   source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v5"
#   namespace   = module.namespace.lower_resource_name
#   name        = "czl74b-authorizer"
#   environment = var.environment

#   code = "./../../src/apigateway-authorizer"
#   handler = "main.handler"
#   runtime = "nodejs14.x"
#   size = "s"
#   timeout = 600
#   vpc_cidr_code = lookup(var.awsacct_cidr_code, var.environment)
#   tags          = module.namespace.tags
#   short_tags    = module.namespace.short_tags
# }

# Attach the Lambda Authorizer to the API Gateway
# resource "aws_api_gateway_authorizer" "demo_auth" {
#   name                              = "demo_auth"
#   rest_api_id                       = module.api-gateway.rest_api.id
#   type                              = "REQUEST"
#   authorizer_uri                    = module.czl74b_authorizer.invoke_arn
#   authorizer_credentials            = aws_iam_role.invocation_role.arn
#   authorizer_result_ttl_in_seconds  = 0
# }

resource "aws_iam_role" "invocation_role" {
  name                 = "api_gateway_auth_invocation"
  path                 = "/"
  permissions_boundary = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy/CloudCoreL3Permissions"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# resource "aws_iam_role_policy" "invocation_policy" {
#   name = "default"
#   role = aws_iam_role.invocation_role.id

#   policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Action": "lambda:InvokeFunction",
#       "Effect": "Allow",
#       "Resource": "${module.czl74b_authorizer.arn}"
#     }
#   ]
# }
# EOF
# }

# Permission for API Gateway to access Authorizer Lambda Function directly
# resource "aws_lambda_permission" "czl74b_authorizer" {
#    statement_id  = "AllowAPIGatewayInvoke"
#    action        = "lambda:InvokeFunction"
#    function_name = module.czl74b_authorizer.name
#    principal     = "apigateway.amazonaws.com"

#    # The "/*/*" portion grants access from any method on any resource
#    # within the API Gateway REST API.
#    source_arn = "arn:aws:iam::650487181597:role/api_gateway_auth_invocation"
# }