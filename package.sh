export cwd=$(pwd)
cd ./src/apis/business
npm install
serverless package
mkdir -p .webpack/libs-business/nodejs/node_modules
cp -RL .webpack/dependencies/node_modules/* .webpack/libs-business/nodejs/node_modules
find .webpack/libs-business/nodejs/node_modules -name "aws-sdk" -exec rm -Rf "{}" \;
find .webpack -name "main.js.map" -exec rm -Rf "{}" \;
cd $cwd

cd ./src/apis/sor
npm install
serverless package
mkdir -p .webpack/libs-sor/nodejs/node_modules
cp -RL .webpack/dependencies/node_modules/* .webpack/libs-sor/nodejs/node_modules
find .webpack/libs-sor/nodejs/node_modules -name "aws-sdk" -exec rm -Rf "{}" \;
find .webpack -name "main.js.map" -exec rm -Rf "{}" \;
cd $cwd

cd ./src/apis/utility
npm install
serverless package
mkdir -p .webpack/libs-utility/nodejs/node_modules
cp -RL .webpack/dependencies/node_modules/* .webpack/libs-utility/nodejs/node_modules
find .webpack/libs-utility/nodejs/node_modules -name "aws-sdk" -exec rm -Rf "{}" \;
find .webpack -name "main.js.map" -exec rm -Rf "{}" \;
cd $cwd

cd ./src/appsync-authorizer
npm install
serverless package
mkdir -p .webpack/libs-auth/nodejs/node_modules
cp -RL .webpack/dependencies/node_modules/* .webpack/libs-auth/nodejs/node_modules
find .webpack/libs-auth/nodejs/node_modules -name "aws-sdk" -exec rm -Rf "{}" \;
find .webpack -name "main.js.map" -exec rm -Rf "{}" \;
cd $cwd

cd ./src/resolver
npm install
serverless package
mkdir -p .webpack/libs-resolver/nodejs/node_modules
cp -RL .webpack/dependencies/node_modules/* .webpack/libs-resolver/nodejs/node_modules
find .webpack/libs-resover/nodejs/node_modules -name "aws-sdk" -exec rm -Rf "{}" \;
find .webpack -name "main.js.map" -exec rm -Rf "{}" \;
cd $cwd


cd ./src/npm-apollo-libs
npm install
find .webpack/libs-resover/nodejs/node_modules -name "aws-sdk" -exec rm -Rf "{}" \;
find .webpack -name "main.js.map" -exec rm -Rf "{}" \;
cd $cwd