const readSchemaFromFile = require('../../common-utils/read-schema-from-file/index');

exports.handler = async function (event, context) {
    console.log('Resolver Lambda :: Lending :: Event:', event);
    console.log('Resolver Lambda :: Lending :: Context:', context);
    console.log(`Resolver Lambda :: Lending :: Current Parent ${event.info.parentTypeName} and Field ${event.info.fieldName}`);
    let result = [];
    switch (event.info.parentTypeName) {
        case 'Query':
            console.log(`Resolver Lambda :: Lending :: Entering Parent ${event.info.parentTypeName}`);
            switch (event.info.fieldName) {
                case 'partyByAccountNumber':
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    // todo: implement logic to return Party By AccountNumber
                    result = {};
                    break;
                case '_service':
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    result = { sdl: readSchemaFromFile(process.env.SCHEMA_PATH) };
                    break;
                case '__typename':
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    // Not sufficient : need capability to set extensions ...
                    result = { ftv1: 'test' };
                    break;
                case '_entities':
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    const { representations } = event.arguments;
                    const entities = [];

                    for (const representation of representations) {
                        const filteredProduct = products.find((p) => {
                            for (const key of Object.keys(representation)) {
                                if (typeof representation[key] != 'object' && key != '__typename' && p[key] != representation[key]) {
                                    return false;
                                } else if (typeof representation[key] == 'object') {
                                    for (const subkey of Object.keys(representation[key])) {
                                        if (
                                            typeof representation[key][subkey] != 'object' &&
                                            p[key][subkey] != representation[key][subkey]
                                        ) {
                                            return false;
                                        }
                                    }
                                }
                            }
                            return true;
                        });

                        entities.push({ ...filteredProduct, __typename: 'Party' });
                    }
                    result = entities;
                    break;
            }
            break;
        case 'Customer':
            console.log(`Resolver Lambda :: Lending :: Entering Parent ${event.info.parentTypeName}`);
            switch (event.info.fieldName) {
                case 'accounts':
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    result = [
                        { 
                            __typename: 'LineOfCreditAccount',
                            accountID: 'X001' 
                        },
                        { 
                            __typename: 'LineOfCreditAccount',
                            accountID: 'X002' 
                        }
                    ];
                    break;
            }
            break;
    }
    console.log(`returning ${JSON.stringify(result)}`);
    return result;
};