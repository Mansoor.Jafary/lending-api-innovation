const readSchemaFromFile = require('../../common-utils/read-schema-from-file/index');
const { getFunctionCaller } = require('../../common-utils/api-caller/index');

const { RecordNotFoundError } = require('../../common-utils/lending-errors/index');
const LendingResponse = require('../../common-utils/lending-response/index');

exports.handler = async function (event, context) {

    console.log('Resolver Lambda :: Lending :: Event:', JSON.stringify(event));
    console.log('Resolver Lambda :: Lending :: Context:', JSON.stringify(context));
    console.log(`Resolver Lambda :: Lending :: Current Parent ${event.info.parentTypeName} and Field ${event.info.fieldName}`);
    const { awsRequestId } = context;
    let result = [];
    switch (event.info.parentTypeName) {
        case 'Query': {
            console.log(`Resolver Lambda :: Lending :: Entering Parent ${event.info.parentTypeName}`);
            switch (event.info.fieldName) {
                case '_service': {
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    console.log(process.env.SCHEMA_PATH);
                    result = { sdl: readSchemaFromFile(process.env.SCHEMA_PATH) };
                    console.log(`Resolver Lambda :: ${event.info.parentTypeName} ${event.info.fieldName} Result: ${result}`);
                    break;
                }
                case '__typename': {
                    // Not sufficient : need capability to set extensions ...
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    result = { ftv1: 'test' };
                    console.log(`Resolver Lambda :: ${event.info.parentTypeName} ${event.info.fieldName} Result: ${result}`);
                    break;
                }
                case '_entities': {
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    const { representations } = event.arguments;
                    const entities = [];

                    for (const representation of representations) {
                        console.log(`Resolver Lambda :: Lending :: Representation ${JSON.stringify(representation)}`);
                        const { accountID } = representation;
                        const callFunction = getFunctionCaller(context);
                        const functionURI = JSON.parse(process.env.URIS).readBalancesSnapshot;
                        const payload = { accountID };
                        const lambdaResponse = await callFunction(functionURI, payload);
                        console.log(`Resolver Lambda :: Lending :: Lambda Response ${JSON.stringify(lambdaResponse)}`);

                        if (!(lambdaResponse.error instanceof Error || lambdaResponse.error && lambdaResponse.error.text)) {
                            const filteredProduct = lambdaResponse.data.find((p) => {
                                for (const key of Object.keys(representation)) {
                                    if (typeof representation[key] != 'object' && key != '__typename' && p[key] != representation[key]) {
                                        return false;
                                    } else if (typeof representation[key] == 'object') {
                                        for (const subkey of Object.keys(representation[key])) {
                                            if (
                                                typeof representation[key][subkey] != 'object' &&
                                                p[key][subkey] != representation[key][subkey]
                                            ) {
                                                return false;
                                            }
                                        }
                                    }
                                }
                                return true;
                            });
                            const resp = {
                                financialPosition: {
                                    balancesPosition: {
                                        currentBalance: filteredProduct.currentBalance,
                                        allocatedCreditLimit: filteredProduct.allocatedCreditLimit,
                                        availableCreditLimit: filteredProduct.allocatedCreditLimit
                                    }
                                }
                            }
                            entities.push({ ...resp, __typename: 'LineOfCreditAccount' });
                            result = entities;
                        } else {
                            result = lambdaResponse;
                        }
                    }
                    console.log(`Resolver Lambda :: ${event.info.parentTypeName} ${event.info.fieldName} Result: ${result}`);
                    break;
                }
                case 'accountByAccountID': {
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    context.myData = { foo: 'bar' };
                    event.myData = { foo: 'bar' };
                    const { arguments: { input: { accountID } } } = event;
                    console.log(`Ready to call :: getFunctionCaller :: CONTEXT :: ${JSON.toString(context)}`);
                    const callFunction = getFunctionCaller(context);
                    const functionURI = JSON.parse(process.env.URIS).readBalancesSnapshot;
                    const payload = { accountID };
                    const lambdaResponse = await callFunction(functionURI, payload);
                    console.log(`Response :: getFunctionCaller :: ${JSON.stringify(lambdaResponse)}`);
                    if (!(lambdaResponse.error instanceof Error || lambdaResponse.error && lambdaResponse.error.text)) {
                        const lineOfCreditAccounts = lambdaResponse.data.filter(obj => obj.accountID === accountID).map(obj => ({
                            __typename: 'LineOfCreditAccount',
                            accountID: obj.accountID,
                            financialPosition: {
                                balancesPosition: {
                                    currentBalance: obj.currentBalance,
                                    allocatedCreditLimit: obj.allocatedCreditLimit,
                                    availableCreditLimit: obj.availableCreditLimit
                                }
                            }
                        }));
                        console.log(`Resolver Lambda :: ${event.info.parentTypeName} ${event.info.fieldName} lineOfCreditAccounts: ${JSON.stringify(lineOfCreditAccounts)}`);
                        if (lineOfCreditAccounts.length <= 0) {
                            console.log('No Account Found');
                            result = new LendingResponse(new RecordNotFoundError('There is a problem with your request. Please check your query and try again', 400, awsRequestId), null);
                        } else {
                            console.log('Found an account to return');
                            result = lineOfCreditAccounts[0];
                        }
                        console.log(`Resolver Lambda :: ${event.info.parentTypeName} ${event.info.fieldName} Result: ${result}`);
                    } else {
                        result = lambdaResponse;
                    }
                    break;
                }
                // case 'bgTest': {
                //     result = "Hello World!";
                //     break;
                // }
            }
            break;
        }
        case 'Mutation': {
            console.log(`Resolver Lambda :: Lending :: Entering Parent ${event.info.parentTypeName}`);
            switch (event.info.fieldName) {
                case 'createNewInstallmentLoansUploadURL': {
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    console.log(`Ready to call :: getFunctionCaller :: CONTEXT :: ${JSON.toString(context)}`);
                    const callFunction = getFunctionCaller(context);
                    const functionURI = JSON.parse(process.env.URIS).createNewLoansUploadURL;
                    const lambdaResponse = await callFunction(functionURI, {});
                    console.log(`Response :: getFunctionCaller :: ${JSON.stringify(lambdaResponse)}`);
                    if (!(lambdaResponse.error instanceof Error || lambdaResponse.error && lambdaResponse.error.text)) {
                        result = lambdaResponse.data;
                        console.log(`Resolver Lambda :: ${event.info.parentTypeName} ${event.info.fieldName} Result: ${JSON.stringify(result) }`);                       
                    } else {
                        result = lambdaResponse;
                    }
                    break;
                }
            }
        }
        case 'LineOfCreditAccount': {
            console.log(`Resolver Lambda :: Lending :: Entering Parent ${ event.info.parentTypeName }`);
            switch (event.info.fieldName) {
                case 'financialPosition': {
                    console.log('Resolver Lambda :: financialPosition :: Event:', JSON.stringify(event));
                    console.log('Resolver Lambda :: financialPosition :: Context:', JSON.stringify(context));
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${ event.info.fieldName }`);
                    // todo: implement logic to return Party By AccountNumber
                    result = event.source.financialPosition;
                    console.log(`Resolver Lambda :: ${ event.info.parentTypeName } ${ event.info.fieldName } Result: ${ result }`);
                    break;
                }
            }
            break;
        }
        case 'LineOfCreditAccountFinancialPosition': {
            console.log(`Resolver Lambda :: Lending :: Entering Parent ${ event.info.parentTypeName }`);
            switch (event.info.fieldName) {
                case 'balancesPosition': {
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${ event.info.fieldName }`);
                    result = {
                        data: event.source.balancesPosition
                    };
                    console.log(`Resolver Lambda :: ${ event.info.parentTypeName } ${ event.info.fieldName } Result: ${ result }`);
                    break;
                }
            }
            break;
        }
    }
    console.log(`returning ${ JSON.stringify(result) }`);
    return result;
};