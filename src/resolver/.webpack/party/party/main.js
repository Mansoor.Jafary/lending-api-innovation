/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ([
/* 0 */,
/* 1 */
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

const { readFileSync } = __webpack_require__(2);


const readSchemaFromFile = (_path) => {
    console.log('PATH:',_path);
    console.log('DIRNAME:',__dirname);
    const SCHEMA = readFileSync(`${__dirname}${_path}`).toString('utf-8');
    console.log('SCHEMA:', SCHEMA);
    return SCHEMA;
}
//console.log(readSchemaFromFile('./schema/Party/schema.graphql'));
module.exports = readSchemaFromFile;




/***/ }),
/* 2 */
/***/ ((module) => {

"use strict";
module.exports = require("fs");

/***/ })
/******/ 	]);
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
const readSchemaFromFile = __webpack_require__(1);

exports.handler = async function (event, context) {
    console.log('Resolver Lambda :: Lending :: Event:', event);
    console.log('Resolver Lambda :: Lending :: Context:', context);
    console.log(`Resolver Lambda :: Lending :: Current Parent ${event.info.parentTypeName} and Field ${event.info.fieldName}`);
    let result = [];
    switch (event.info.parentTypeName) {
        case 'Query':
            console.log(`Resolver Lambda :: Lending :: Entering Parent ${event.info.parentTypeName}`);
            switch (event.info.fieldName) {
                case 'partyByAccountNumber':
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    // todo: implement logic to return Party By AccountNumber
                    result = {};
                    break;
                case '_service':
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    result = { sdl: readSchemaFromFile(process.env.SCHEMA_PATH) };
                    break;
                case '__typename':
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    // Not sufficient : need capability to set extensions ...
                    result = { ftv1: 'test' };
                    break;
                case '_entities':
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    const { representations } = event.arguments;
                    const entities = [];

                    for (const representation of representations) {
                        const filteredProduct = products.find((p) => {
                            for (const key of Object.keys(representation)) {
                                if (typeof representation[key] != 'object' && key != '__typename' && p[key] != representation[key]) {
                                    return false;
                                } else if (typeof representation[key] == 'object') {
                                    for (const subkey of Object.keys(representation[key])) {
                                        if (
                                            typeof representation[key][subkey] != 'object' &&
                                            p[key][subkey] != representation[key][subkey]
                                        ) {
                                            return false;
                                        }
                                    }
                                }
                            }
                            return true;
                        });

                        entities.push({ ...filteredProduct, __typename: 'Party' });
                    }
                    result = entities;
                    break;
            }
            break;
        case 'Customer':
            console.log(`Resolver Lambda :: Lending :: Entering Parent ${event.info.parentTypeName}`);
            switch (event.info.fieldName) {
                case 'accounts':
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    result = [
                        { 
                            __typename: 'LineOfCreditAccount',
                            accountID: 'X001' 
                        },
                        { 
                            __typename: 'LineOfCreditAccount',
                            accountID: 'X002' 
                        }
                    ];
                    break;
            }
            break;
    }
    console.log(`returning ${JSON.stringify(result)}`);
    return result;
};
})();

var __webpack_export_target__ = exports;
for(var i in __webpack_exports__) __webpack_export_target__[i] = __webpack_exports__[i];
if(__webpack_exports__.__esModule) Object.defineProperty(__webpack_export_target__, "__esModule", { value: true });
/******/ })()
;
//# sourceMappingURL=main.js.map