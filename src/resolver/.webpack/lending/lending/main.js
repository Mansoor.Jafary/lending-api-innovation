/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ([
/* 0 */,
/* 1 */
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

const { readFileSync } = __webpack_require__(2);


const readSchemaFromFile = (_path) => {
    console.log('PATH:',_path);
    console.log('DIRNAME:',__dirname);
    const SCHEMA = readFileSync(`${__dirname}${_path}`).toString('utf-8');
    console.log('SCHEMA:', SCHEMA);
    return SCHEMA;
}
//console.log(readSchemaFromFile('./schema/Party/schema.graphql'));
module.exports = readSchemaFromFile;




/***/ }),
/* 2 */
/***/ ((module) => {

"use strict";
module.exports = require("fs");

/***/ }),
/* 3 */
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Not supported - Need Lambda Node Runtime v16.x
// const { setTimeout: setTimeoutPromise } = require('timers/promises');
const LendingResponse = __webpack_require__(4);
const { getAppConfigData } = __webpack_require__(5);
const initExecutionTimer = __webpack_require__(9)();

const {
  DomainError,
  SystemError,
  AuthorizationError,
  ForbiddenError,
  RecordNotFoundError,
  ValidationError,
  TimeoutError,
  sanitizeError
} = __webpack_require__(11);


function getFunctionCaller({ awsRequestId }) {
  console.log('test');
  return async function callFunction(functionURI, payload) {
    console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction :: START`);
    const { AWS_REGION, E0 } = process.env;

    const callFunctionTimer = initExecutionTimer('callFunction');
    const timer1 = callFunctionTimer.startExecutionTimer();

    const AWS = __webpack_require__(7); // lazy loading aws-sdk
    if (E0 === 'TRUE') {
      // lazy load proxy-agent only in e0 for sam local testing
      const proxy = __webpack_require__(8);
      AWS.config.update({ httpOptions: { agent: proxy(HTTP_PROXY) } });
    }
    const lambda = new AWS.Lambda({ region: AWS_REGION });
    const appConfigData = await getAppConfigData();

    // const { globalAppConfigs: { functionCallerTimeout } = {} } = appConfigData;

    // not supported until Lambda Runtime Node v16
    // const cancelTimeout = new AbortController();
    // const cancelPromise = new AbortController(); // cannot abort promise

    // Not supported - Need Lambda Node Runtime v16.x
    // const functionCallTimeout = new Promise(async (resolve, reject) => {
    //   try {
    //     await setTimeoutPromise(functionCallerTimeout, undefined, { signal: cancelTimeout.signal });
    //     // cancelPromise.abort(); // cannot abort Promise
    //     return resolve(new LendingResponse(new TimeoutError(undefined, undefined, awsRequestId), null));
    //   } catch (err) {
    //     if (err.name === 'AbortError') {
    //       console.log('This timeout control was aborted');
    //     }
    //   }
    // });

    // Used to clear timeout. This approach only until v16 is not supported
    // let functionTimeoutSignal = null;

    // const functionCallTimeout = new Promise(resolve => {
    //     console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.functionCallTimeout :: START`);

    //     functionTimeoutSignal = setTimeout(() => {
    //       console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.functionCallTimeout :: Timeout Triggered after ${functionCallerTimeout}ms`);
    //       return resolve(new LendingResponse(new TimeoutError(undefined, undefined, awsRequestId), null));
    //     }, functionCallerTimeout) ;
    //   });

    const callFunctionResponse = new Promise(async (resolve, reject) => {
        console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: START`);
        const callFunctionResponseTimer = initExecutionTimer('callFunctionResponse');
        const timer1 = callFunctionResponseTimer.startExecutionTimer()

        // alternate for setTimeoutPromise function until Lambda supports Node Runtime v16
        // console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: AppConfig functionCallerTimeout Value :: ${functionCallerTimeout}`);

        const invokeParams = {};
        invokeParams.FunctionName = functionURI;
        invokeParams.Payload = JSON.stringify(payload);
        console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Invoke Request :: ${JSON.stringify(invokeParams)}`);
        try {
          lambda.invoke(invokeParams, (error, data) => {
            if (error) {
              console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Invoke Error :: ${error}`);
              return resolve(new LendingResponse(new SystemError(undefined, undefined, awsRequestId), null));
            }
            console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Result :: ${JSON.stringify(data)}`);
            const resJson = JSON.parse(data.Payload);

            if (resJson.error instanceof Error || resJson.error && resJson.error.text) {
              console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Final Response with Error :: ${JSON.stringify(resJson)}`); 
              callFunctionResponseTimer.logExecutionDuration(timer1);
              return resolve(new LendingResponse(resJson.error, null));           
            }
            
            console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Lambda Response :: ${JSON.stringify(resJson)}`);
            callFunctionResponseTimer.logExecutionDuration(timer1);
            
            if (resJson.data) {
                return resolve(new LendingResponse(null, resJson.data));
            } else { // todo: this is an anti-pattern. Do we want to keep it?
                return resolve(new LendingResponse(null, resJson));
            }
          });
        } catch (error) { // Invoke response is { Payload: { error: null } }
          console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Unexpected Error :: ${error}`);
          return resolve(new LendingResponse(resJson.error, null));
        }
      });

    return Promise.race([
      callFunctionResponse,
      // functionCallTimeout
    ]).then((result) => {
      if (result.error instanceof Error || result.error && result.error.text) {
        // Sanitize error before return to caller
        result.error = sanitizeError(result.error);
      }
      callFunctionTimer.logExecutionDuration(timer1);
      // clearTimeout(functionTimeoutSignal);
      console.log(`Promise.race :: Returning Result :: ${JSON.stringify(result)}`);
      return result;
    });
  }
}

function getRestAPICaller({ awsRequestId }) {
  console.log('API Caller :: getRestAPICaller START');

  return async function callRestAPI(config) {
    console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI :: START`);
    console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI :: Config: ${config}`);

    const axios = __webpack_require__(12); // lazy loading axios 
    const { AbortController } = __webpack_require__(13); // lazy loading node-abort-controller

    const appConfigData = await getAppConfigData();
    const { globalAppConfigs: { apiCallerTimeout = null } = {} } = appConfigData;

    const cancelRequest = new AbortController();

    // Track start time of the call
    axios.interceptors.request.use(
      function (config) {
        config.proxy = false;
        // set default method as POST
        if (!config.method) {
          config.method = 'POST';
        } 
        console.info('Incoming Timeout:', config.timeout);
        // set a default global configurable timeout value for the call
        if (!config.timeout) {
          config.timeout = apiCallerTimeout;
        }
        config.signal = cancelRequest.signal;
        Object.assign(config.headers, defaultHeaders);
        config.metadata = { startTime: new Date() }
        console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI  :: Interceptor Config Updated ::', ${JSON.stringify(config)}`);
        return config;
      },
      function (error) {
        return Promise.reject(error);
      }
    );

    // Track end time of the call
    axios.interceptors.response.use(
      function (response) {
        response.config.metadata.endTime = new Date();
        response.duration = response.config.metadata.endTime - response.config.metadata.startTime;
        return response;
      },
      function (error) {
        console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI :: axios Interceptor Error :: ${error}`);
        if (!axios.isCancel(error)) {
          error.config.metadata.endTime = new Date();
          error.duration = error.config.metadata.endTime - error.config.metadata.startTime;
        }
        return Promise.reject(error);
      }
    );

    const defaultHeaders = {
      'Content-Type': 'application/json',
    };

    const callAPIResponse = new Promise((resolve) => {
      console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI.callAPIResponse :: START`);
      const callAPIResponseTimer = initExecutionTimer('callFunctionResponse');
      const timer1 = callAPIResponseTimer.startExecutionTimer()

      // implement timeout for axios call
      const apiTimeoutSignal = setTimeout(() => {
        console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI.callAPIResponse :: Timeout Triggered after ${apiCallerTimeout}ms`);
        cancelRequest.abort();
      }, apiCallerTimeout);

      axios(config)
        .then(result => {
          // axios returned a response. clear the timeout timer
          clearTimeout(apiTimeoutSignal);
          console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callAPIResponse :: Success :: ${result}`);
          callAPIResponseTimer.logExecutionDuration(timer1);
          return resolve(new LendingResponse(null, result.data));
        })
        .catch(error => {
          if (axios.isCancel(error)) {
            console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI :: Returning response after API Call Cancelled`);
            return resolve(new LendingResponse(new TimeoutError(undefined, undefined, awsRequestId), null));
          } else {
            // axios call was not cancelled due to timeout. clear the timeout timer
            clearTimeout(apiTimeoutSignal);
            if (error.response) {
              // The request was made and the server responded with a status code that falls out of the range of 2xx
              console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callAPIResponse :: Error Response:: ${error.message}`);
              if (error.response.status === 401) {
                return resolve(new LendingResponse(new AuthorizationError('Unauthorized', 401, awsRequestId), null));
              } else if (error.response.status === 403) {
                return resolve(new LendingResponse(new ForbiddenError('Forbidden', 403, awsRequestId), null));
              } else if (error.response.status >= 400 && e.response.status < 500) {
                return resolve(new LendingResponse(new DomainError('There is a problem with your request. Please check your query and try again', 400, awsRequestId), null));
              }
            } else if (error.request) {
              // The request was made but no response was received
              console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callAPIResponse :: Error No Response from API Call :: ${error.message}`);
              return resolve(new LendingResponse(new SystemError(undefined, undefined, awsRequestId), null));
            } else {
              // Something happened in setting up the request that triggered an Error
              console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callAPIResponse :: Error Occured With Message :: ${error.message}`);
              return resolve(new LendingResponse(new SystemError(undefined, undefined, awsRequestId), null));
            }
            console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callAPIResponse :: Unknown Error :: ${error.message}`);
            return resolve(new LendingResponse(new SystemError(undefined, undefined, awsRequestId), null));
          }
        })
    });

    return callAPIResponse.then((result) => {
      if (result.error instanceof Error) {
        // Sanitize error before return to caller
        result.error = sanitizeError(result.error);
      }
      return result;
    });
  }
}

module.exports = { getRestAPICaller, getFunctionCaller }

/***/ }),
/* 4 */
/***/ ((module) => {

class LendingResponse {
    constructor(error, data) {
        this.error = error;
        this.data = data;
    }
}

module.exports = LendingResponse;

/***/ }),
/* 5 */
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Observations:
// 1. HTTP Call is significantly faster compared to SDK version -- difference in calls was noted to be 2 to 3 digit milliseconds

const getAppConfigData = async () => {
  console.log('getAppConfigData :: AppConfig Response :: START');
  const http = __webpack_require__(6); //lazy load
  const { APPCONFIG_LOCALHOST } = process.env;
  console.log(`getAppConfigData :: AppConfig LOCALHOST :: ${APPCONFIG_LOCALHOST}`);
  
  const res = await new Promise((resolve, reject) => {
    http.get(
      `${APPCONFIG_LOCALHOST}`,
      resolve
    );
  });

  let configData = await new Promise((resolve, reject) => {
    let data = '';
    res.on('data', chunk => data += chunk);
    res.on('error', err => reject(err));
    res.on('end', () => resolve(data));
  });

  const parsedConfigData = JSON.parse(configData);
  console.log("getAppConfigData :: AppConfig Response ::", parsedConfigData);
  return parsedConfigData;
}

// todo: Decommission getAppConfigAlernate - this function is slower than getAppConfigData
const getAppConfigDataAlternate = async () => {
  console.log('getAppConfigDataAlternate :: AppConfig Response :: START');
  const AWS = __webpack_require__(7); //lazy load
  const { E0 } = process.env;
  if (E0 === 'TRUE') {
    // lazy load proxy-agent only in e0 for sam local testing
    const proxy = __webpack_require__(8);
    AWS.config.update({ httpOptions: { agent: proxy(HTTP_PROXY) } });
  }
  const appconfig = new AWS.AppConfig({ apiVersion: '2019-10-09' });
  const appconfigParams = JSON.parse(process.env.APPCONFIG_PARAMS);
  const appConfigResponse = await appconfig.getConfiguration(appconfigParams.appConfig).promise().catch(err => {
    console.log(err);
  });

  const parsedConfigData = JSON.parse(Buffer.from(appConfigResponse.Content, 'base64').toString('ascii'));
  console.log("getAppConfigDataAlternate :: AppConfig Response ::", parsedConfigData);
  return parsedConfigData;
}

module.exports = { getAppConfigData, getAppConfigDataAlternate };

/***/ }),
/* 6 */
/***/ ((module) => {

"use strict";
module.exports = require("http");

/***/ }),
/* 7 */
/***/ ((module) => {

"use strict";
module.exports = require("aws-sdk");

/***/ }),
/* 8 */
/***/ ((module) => {

"use strict";
module.exports = require("proxy-agent");

/***/ }),
/* 9 */
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

const now = __webpack_require__(10);

function initExecutionTimer(moduleName) {
    const lambdaName = moduleName || process.env.AWS_LAMBDA_FUNCTION_NAME;
    return (marker) => {
        let timeStop = 0;
        return {
            startExecutionTimer: () => {
                const startTime = now();
                return startTime;
            },
            logExecutionDuration: (startTimer) => {
                timeStop++;
                const endTime = now();
                const duration = endTime - startTimer;
                console.log(`${lambdaName} :: ${marker} :: Stop ${timeStop} :: Duration ${duration.toFixed(2)}`);
            }
        }
    }
}

module.exports = initExecutionTimer;

/***/ }),
/* 10 */
/***/ ((module) => {

"use strict";
module.exports = require("performance-now");

/***/ }),
/* 11 */
/***/ ((module) => {

// Error codes structure takes from API Styleguide published by the API Champions team here https://confluence.int.ally.com/display/EAP/Error+Handling

class SystemError extends Error {
    constructor(message = 'Service is currently unavailable. Please try again later.', statusCode = 503, correlationId = null) {
        super(message);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'SYSTEM';
        this.statusCode = statusCode;
        this.info = {
            code: 'F-BILD-LAPI-503',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class DomainError extends Error {
    constructor(message = 'Please check your request and try again.', statusCode = 400, correlationId = null) {
        super(message);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-400',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class TimeoutError extends SystemError {
    constructor(message = 'Execution aborted due to timeout. Please try again later.', statusCode = 504, correlationId = null) {
        super(message, statusCode, correlationId);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'SYSTEM';
        this.statusCode = statusCode;
        this.info = {
            code: 'F-BILD-LAPI-504',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class AuthorizationError extends DomainError {
    constructor(message = 'Unauthorized.', statusCode = 401, correlationId = null) {
        super(message, statusCode, correlationId);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-401',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class ForbiddenError extends DomainError {
    constructor(message = 'Forbidden', statusCode = 403, correlationId = null) {
        super(message, statusCode, correlationId);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-403',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class RecordNotFoundError extends DomainError {
    constructor(message = 'Not Record Found', statusCode = 404, correlationId = null) {
        super(message, statusCode, correlationId);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-404',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class ValidationError extends DomainError {
    constructor(property, statusCode, correlationId) {
        super(`Failed to validate ${property}`, statusCode = 422, correlationId = null);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-422',
            correlationId,
            property: this.property
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

const sanitizeError = (error) => {
    
    if (error instanceof DomainError || error.type === 'DOMAIN') {
        console.warn(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: Domain Error :: ${error}. Return error to consumer.`);
        return error;
    }
    console.error(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: System Error :: ${error}. Sanitizing error prior to returning to consumer.`);
    return new SystemError(undefined, undefined, error.info.correlationId);
}

module.exports = {
    DomainError,
    SystemError,
    TimeoutError,
    AuthorizationError,
    ForbiddenError,
    RecordNotFoundError,
    ValidationError,
    sanitizeError
}

/***/ }),
/* 12 */
/***/ ((module) => {

"use strict";
module.exports = require("axios");

/***/ }),
/* 13 */
/***/ ((module) => {

"use strict";
module.exports = require("node-abort-controller");

/***/ })
/******/ 	]);
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
const readSchemaFromFile = __webpack_require__(1);
const { getFunctionCaller } = __webpack_require__(3);

const { RecordNotFoundError } = __webpack_require__(11);
const LendingResponse = __webpack_require__(4);

exports.handler = async function (event, context) {

    console.log('Resolver Lambda :: Lending :: Event:', JSON.stringify(event));
    console.log('Resolver Lambda :: Lending :: Context:', JSON.stringify(context));
    console.log(`Resolver Lambda :: Lending :: Current Parent ${event.info.parentTypeName} and Field ${event.info.fieldName}`);
    const { awsRequestId } = context;
    let result = [];
    switch (event.info.parentTypeName) {
        case 'Query': {
            console.log(`Resolver Lambda :: Lending :: Entering Parent ${event.info.parentTypeName}`);
            switch (event.info.fieldName) {
                case '_service': {
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    console.log(process.env.SCHEMA_PATH);
                    result = { sdl: readSchemaFromFile(process.env.SCHEMA_PATH) };
                    console.log(`Resolver Lambda :: ${event.info.parentTypeName} ${event.info.fieldName} Result: ${result}`);
                    break;
                }
                case '__typename': {
                    // Not sufficient : need capability to set extensions ...
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    result = { ftv1: 'test' };
                    console.log(`Resolver Lambda :: ${event.info.parentTypeName} ${event.info.fieldName} Result: ${result}`);
                    break;
                }
                case '_entities': {
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    const { representations } = event.arguments;
                    const entities = [];

                    for (const representation of representations) {
                        console.log(`Resolver Lambda :: Lending :: Representation ${JSON.stringify(representation)}`);
                        const { accountID } = representation;
                        const callFunction = getFunctionCaller(context);
                        const functionURI = JSON.parse(process.env.URIS).readBalancesSnapshot;
                        const payload = { accountID };
                        const lambdaResponse = await callFunction(functionURI, payload);
                        console.log(`Resolver Lambda :: Lending :: Lambda Response ${JSON.stringify(lambdaResponse)}`);

                        if (!(lambdaResponse.error instanceof Error || lambdaResponse.error && lambdaResponse.error.text)) {
                            const filteredProduct = lambdaResponse.data.find((p) => {
                                for (const key of Object.keys(representation)) {
                                    if (typeof representation[key] != 'object' && key != '__typename' && p[key] != representation[key]) {
                                        return false;
                                    } else if (typeof representation[key] == 'object') {
                                        for (const subkey of Object.keys(representation[key])) {
                                            if (
                                                typeof representation[key][subkey] != 'object' &&
                                                p[key][subkey] != representation[key][subkey]
                                            ) {
                                                return false;
                                            }
                                        }
                                    }
                                }
                                return true;
                            });
                            const resp = {
                                financialPosition: {
                                    balancesPosition: {
                                        currentBalance: filteredProduct.currentBalance,
                                        allocatedCreditLimit: filteredProduct.allocatedCreditLimit,
                                        availableCreditLimit: filteredProduct.allocatedCreditLimit
                                    }
                                }
                            }
                            entities.push({ ...resp, __typename: 'LineOfCreditAccount' });
                            result = entities;
                        } else {
                            result = lambdaResponse;
                        }
                    }
                    console.log(`Resolver Lambda :: ${event.info.parentTypeName} ${event.info.fieldName} Result: ${result}`);
                    break;
                }
                case 'accountByAccountID': {
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    context.myData = { foo: 'bar' };
                    event.myData = { foo: 'bar' };
                    const { arguments: { input: { accountID } } } = event;
                    console.log(`Ready to call :: getFunctionCaller :: CONTEXT :: ${JSON.toString(context)}`);
                    const callFunction = getFunctionCaller(context);
                    const functionURI = JSON.parse(process.env.URIS).readBalancesSnapshot;
                    const payload = { accountID };
                    const lambdaResponse = await callFunction(functionURI, payload);
                    console.log(`Response :: getFunctionCaller :: ${JSON.stringify(lambdaResponse)}`);
                    if (!(lambdaResponse.error instanceof Error || lambdaResponse.error && lambdaResponse.error.text)) {
                        const lineOfCreditAccounts = lambdaResponse.data.filter(obj => obj.accountID === accountID).map(obj => ({
                            __typename: 'LineOfCreditAccount',
                            accountID: obj.accountID,
                            financialPosition: {
                                balancesPosition: {
                                    currentBalance: obj.currentBalance,
                                    allocatedCreditLimit: obj.allocatedCreditLimit,
                                    availableCreditLimit: obj.availableCreditLimit
                                }
                            }
                        }));
                        console.log(`Resolver Lambda :: ${event.info.parentTypeName} ${event.info.fieldName} lineOfCreditAccounts: ${JSON.stringify(lineOfCreditAccounts)}`);
                        if (lineOfCreditAccounts.length <= 0) {
                            console.log('No Account Found');
                            result = new LendingResponse(new RecordNotFoundError('There is a problem with your request. Please check your query and try again', 400, awsRequestId), null);
                        } else {
                            console.log('Found an account to return');
                            result = lineOfCreditAccounts[0];
                        }
                        console.log(`Resolver Lambda :: ${event.info.parentTypeName} ${event.info.fieldName} Result: ${result}`);
                    } else {
                        result = lambdaResponse;
                    }
                    break;
                }
                // case 'bgTest': {
                //     result = "Hello World!";
                //     break;
                // }
            }
            break;
        }
        case 'Mutation': {
            console.log(`Resolver Lambda :: Lending :: Entering Parent ${event.info.parentTypeName}`);
            switch (event.info.fieldName) {
                case 'createNewInstallmentLoansUploadURL': {
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${event.info.fieldName}`);
                    console.log(`Ready to call :: getFunctionCaller :: CONTEXT :: ${JSON.toString(context)}`);
                    const callFunction = getFunctionCaller(context);
                    const functionURI = JSON.parse(process.env.URIS).createNewLoansUploadURL;
                    const lambdaResponse = await callFunction(functionURI, {});
                    console.log(`Response :: getFunctionCaller :: ${JSON.stringify(lambdaResponse)}`);
                    if (!(lambdaResponse.error instanceof Error || lambdaResponse.error && lambdaResponse.error.text)) {
                        result = lambdaResponse.data;
                        console.log(`Resolver Lambda :: ${event.info.parentTypeName} ${event.info.fieldName} Result: ${JSON.stringify(result) }`);                       
                    } else {
                        result = lambdaResponse;
                    }
                    break;
                }
            }
        }
        case 'LineOfCreditAccount': {
            console.log(`Resolver Lambda :: Lending :: Entering Parent ${ event.info.parentTypeName }`);
            switch (event.info.fieldName) {
                case 'financialPosition': {
                    console.log('Resolver Lambda :: financialPosition :: Event:', JSON.stringify(event));
                    console.log('Resolver Lambda :: financialPosition :: Context:', JSON.stringify(context));
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${ event.info.fieldName }`);
                    // todo: implement logic to return Party By AccountNumber
                    result = event.source.financialPosition;
                    console.log(`Resolver Lambda :: ${ event.info.parentTypeName } ${ event.info.fieldName } Result: ${ result }`);
                    break;
                }
            }
            break;
        }
        case 'LineOfCreditAccountFinancialPosition': {
            console.log(`Resolver Lambda :: Lending :: Entering Parent ${ event.info.parentTypeName }`);
            switch (event.info.fieldName) {
                case 'balancesPosition': {
                    console.log(`Resolver Lambda :: Lending :: Entering Field ${ event.info.fieldName }`);
                    result = {
                        data: event.source.balancesPosition
                    };
                    console.log(`Resolver Lambda :: ${ event.info.parentTypeName } ${ event.info.fieldName } Result: ${ result }`);
                    break;
                }
            }
            break;
        }
    }
    console.log(`returning ${ JSON.stringify(result) }`);
    return result;
};
})();

var __webpack_export_target__ = exports;
for(var i in __webpack_exports__) __webpack_export_target__[i] = __webpack_exports__[i];
if(__webpack_exports__.__esModule) Object.defineProperty(__webpack_export_target__, "__esModule", { value: true });
/******/ })()
;
//# sourceMappingURL=main.js.map