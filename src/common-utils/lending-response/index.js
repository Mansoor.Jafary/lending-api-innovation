class LendingResponse {
    constructor(error, data) {
        this.error = error;
        this.data = data;
    }
}

module.exports = LendingResponse;