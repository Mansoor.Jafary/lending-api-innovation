// Not supported - Need Lambda Node Runtime v16.x
// const { setTimeout: setTimeoutPromise } = require('timers/promises');
const LendingResponse = require('../lending-response/index');
const { getAppConfigData } = require('../common-initializers/index');
const initExecutionTimer = require('../execution-timer/index')();

const {
  DomainError,
  SystemError,
  AuthorizationError,
  ForbiddenError,
  RecordNotFoundError,
  ValidationError,
  TimeoutError,
  sanitizeError
} = require('../lending-errors/index');


function getFunctionCaller({ awsRequestId }) {
  console.log('test');
  return async function callFunction(functionURI, payload) {
    console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction :: START`);
    const { AWS_REGION, E0 } = process.env;

    const callFunctionTimer = initExecutionTimer('callFunction');
    const timer1 = callFunctionTimer.startExecutionTimer();

    const AWS = require('aws-sdk'); // lazy loading aws-sdk
    if (E0 === 'TRUE') {
      // lazy load proxy-agent only in e0 for sam local testing
      const proxy = require('proxy-agent');
      AWS.config.update({ httpOptions: { agent: proxy(HTTP_PROXY) } });
    }
    const lambda = new AWS.Lambda({ region: AWS_REGION });
    const appConfigData = await getAppConfigData();

    // const { globalAppConfigs: { functionCallerTimeout } = {} } = appConfigData;

    // not supported until Lambda Runtime Node v16
    // const cancelTimeout = new AbortController();
    // const cancelPromise = new AbortController(); // cannot abort promise

    // Not supported - Need Lambda Node Runtime v16.x
    // const functionCallTimeout = new Promise(async (resolve, reject) => {
    //   try {
    //     await setTimeoutPromise(functionCallerTimeout, undefined, { signal: cancelTimeout.signal });
    //     // cancelPromise.abort(); // cannot abort Promise
    //     return resolve(new LendingResponse(new TimeoutError(undefined, undefined, awsRequestId), null));
    //   } catch (err) {
    //     if (err.name === 'AbortError') {
    //       console.log('This timeout control was aborted');
    //     }
    //   }
    // });

    // Used to clear timeout. This approach only until v16 is not supported
    // let functionTimeoutSignal = null;

    // const functionCallTimeout = new Promise(resolve => {
    //     console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.functionCallTimeout :: START`);

    //     functionTimeoutSignal = setTimeout(() => {
    //       console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.functionCallTimeout :: Timeout Triggered after ${functionCallerTimeout}ms`);
    //       return resolve(new LendingResponse(new TimeoutError(undefined, undefined, awsRequestId), null));
    //     }, functionCallerTimeout) ;
    //   });

    const callFunctionResponse = new Promise(async (resolve, reject) => {
        console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: START`);
        const callFunctionResponseTimer = initExecutionTimer('callFunctionResponse');
        const timer1 = callFunctionResponseTimer.startExecutionTimer()

        // alternate for setTimeoutPromise function until Lambda supports Node Runtime v16
        // console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: AppConfig functionCallerTimeout Value :: ${functionCallerTimeout}`);

        const invokeParams = {};
        invokeParams.FunctionName = functionURI;
        invokeParams.Payload = JSON.stringify(payload);
        console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Invoke Request :: ${JSON.stringify(invokeParams)}`);
        try {
          lambda.invoke(invokeParams, (error, data) => {
            if (error) {
              console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Invoke Error :: ${error}`);
              return resolve(new LendingResponse(new SystemError(undefined, undefined, awsRequestId), null));
            }
            console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Result :: ${JSON.stringify(data)}`);
            const resJson = JSON.parse(data.Payload);

            if (resJson.error instanceof Error || resJson.error && resJson.error.text) {
              console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Final Response with Error :: ${JSON.stringify(resJson)}`); 
              callFunctionResponseTimer.logExecutionDuration(timer1);
              return resolve(new LendingResponse(resJson.error, null));           
            }
            
            console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Lambda Response :: ${JSON.stringify(resJson)}`);
            callFunctionResponseTimer.logExecutionDuration(timer1);
            
            if (resJson.data) {
                return resolve(new LendingResponse(null, resJson.data));
            } else { // todo: this is an anti-pattern. Do we want to keep it?
                return resolve(new LendingResponse(null, resJson));
            }
          });
        } catch (error) { // Invoke response is { Payload: { error: null } }
          console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Unexpected Error :: ${error}`);
          return resolve(new LendingResponse(resJson.error, null));
        }
      });

    return Promise.race([
      callFunctionResponse,
      // functionCallTimeout
    ]).then((result) => {
      if (result.error instanceof Error || result.error && result.error.text) {
        // Sanitize error before return to caller
        result.error = sanitizeError(result.error);
      }
      callFunctionTimer.logExecutionDuration(timer1);
      // clearTimeout(functionTimeoutSignal);
      console.log(`Promise.race :: Returning Result :: ${JSON.stringify(result)}`);
      return result;
    });
  }
}

function getRestAPICaller({ awsRequestId }) {
  console.log('API Caller :: getRestAPICaller START');

  return async function callRestAPI(config) {
    console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI :: START`);
    console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI :: Config: ${config}`);

    const axios = require('axios'); // lazy loading axios 
    const { AbortController } = require('node-abort-controller'); // lazy loading node-abort-controller

    const appConfigData = await getAppConfigData();
    const { globalAppConfigs: { apiCallerTimeout = null } = {} } = appConfigData;

    const cancelRequest = new AbortController();

    // Track start time of the call
    axios.interceptors.request.use(
      function (config) {
        config.proxy = false;
        // set default method as POST
        if (!config.method) {
          config.method = 'POST';
        } 
        console.info('Incoming Timeout:', config.timeout);
        // set a default global configurable timeout value for the call
        if (!config.timeout) {
          config.timeout = apiCallerTimeout;
        }
        config.signal = cancelRequest.signal;
        Object.assign(config.headers, defaultHeaders);
        config.metadata = { startTime: new Date() }
        console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI  :: Interceptor Config Updated ::', ${JSON.stringify(config)}`);
        return config;
      },
      function (error) {
        return Promise.reject(error);
      }
    );

    // Track end time of the call
    axios.interceptors.response.use(
      function (response) {
        response.config.metadata.endTime = new Date();
        response.duration = response.config.metadata.endTime - response.config.metadata.startTime;
        return response;
      },
      function (error) {
        console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI :: axios Interceptor Error :: ${error}`);
        if (!axios.isCancel(error)) {
          error.config.metadata.endTime = new Date();
          error.duration = error.config.metadata.endTime - error.config.metadata.startTime;
        }
        return Promise.reject(error);
      }
    );

    const defaultHeaders = {
      'Content-Type': 'application/json',
    };

    const callAPIResponse = new Promise((resolve) => {
      console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI.callAPIResponse :: START`);
      const callAPIResponseTimer = initExecutionTimer('callFunctionResponse');
      const timer1 = callAPIResponseTimer.startExecutionTimer()

      // implement timeout for axios call
      const apiTimeoutSignal = setTimeout(() => {
        console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI.callAPIResponse :: Timeout Triggered after ${apiCallerTimeout}ms`);
        cancelRequest.abort();
      }, apiCallerTimeout);

      axios(config)
        .then(result => {
          // axios returned a response. clear the timeout timer
          clearTimeout(apiTimeoutSignal);
          console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callAPIResponse :: Success :: ${result}`);
          callAPIResponseTimer.logExecutionDuration(timer1);
          return resolve(new LendingResponse(null, result.data));
        })
        .catch(error => {
          if (axios.isCancel(error)) {
            console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI :: Returning response after API Call Cancelled`);
            return resolve(new LendingResponse(new TimeoutError(undefined, undefined, awsRequestId), null));
          } else {
            // axios call was not cancelled due to timeout. clear the timeout timer
            clearTimeout(apiTimeoutSignal);
            if (error.response) {
              // The request was made and the server responded with a status code that falls out of the range of 2xx
              console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callAPIResponse :: Error Response:: ${error.message}`);
              if (error.response.status === 401) {
                return resolve(new LendingResponse(new AuthorizationError('Unauthorized', 401, awsRequestId), null));
              } else if (error.response.status === 403) {
                return resolve(new LendingResponse(new ForbiddenError('Forbidden', 403, awsRequestId), null));
              } else if (error.response.status >= 400 && e.response.status < 500) {
                return resolve(new LendingResponse(new DomainError('There is a problem with your request. Please check your query and try again', 400, awsRequestId), null));
              }
            } else if (error.request) {
              // The request was made but no response was received
              console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callAPIResponse :: Error No Response from API Call :: ${error.message}`);
              return resolve(new LendingResponse(new SystemError(undefined, undefined, awsRequestId), null));
            } else {
              // Something happened in setting up the request that triggered an Error
              console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callAPIResponse :: Error Occured With Message :: ${error.message}`);
              return resolve(new LendingResponse(new SystemError(undefined, undefined, awsRequestId), null));
            }
            console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callAPIResponse :: Unknown Error :: ${error.message}`);
            return resolve(new LendingResponse(new SystemError(undefined, undefined, awsRequestId), null));
          }
        })
    });

    return callAPIResponse.then((result) => {
      if (result.error instanceof Error) {
        // Sanitize error before return to caller
        result.error = sanitizeError(result.error);
      }
      return result;
    });
  }
}

module.exports = { getRestAPICaller, getFunctionCaller }