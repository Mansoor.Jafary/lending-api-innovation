const now = require('performance-now');

function initExecutionTimer(moduleName) {
    const lambdaName = moduleName || process.env.AWS_LAMBDA_FUNCTION_NAME;
    return (marker) => {
        let timeStop = 0;
        return {
            startExecutionTimer: () => {
                const startTime = now();
                return startTime;
            },
            logExecutionDuration: (startTimer) => {
                timeStop++;
                const endTime = now();
                const duration = endTime - startTimer;
                console.log(`${lambdaName} :: ${marker} :: Stop ${timeStop} :: Duration ${duration.toFixed(2)}`);
            }
        }
    }
}

module.exports = initExecutionTimer;