const AWS = require('aws-sdk');

exports.getSecrets = function (event, context) {
    const { AWS_REGION, SECRET_NAME, HTTP_PROXY, E0 } = process.env;
    console.log(`getSecrets :: Region: ${AWS_REGION} Secret Name ${SECRET_NAME}`);
    if(E0 === 'TRUE'){
        // lazy load proxy-agent only in e0 for sam local testing
        const proxy = require('proxy-agent');
        AWS.config.update({ httpOptions: { agent: proxy(HTTP_PROXY) }});
    }

    const client = new AWS.SecretsManager({ AWS_REGION });

    return new Promise((resolve, reject) => {
        client.getSecretValue({ SecretId: SECRET_NAME }, function (err, data) {
            if (err) {
                console.log(`getSecrets :: getSecretValue Error: ${err}`);
                reject(Error(err));
            }
            let secrets = {};
            // Decrypts secret using the associated KMS CMK.
            // Depending on whether the secret is a string or binary, one of these fields will be populated.
            console.log(`getSecrets :: getSecretValue :: ${JSON.stringify(data)}`);
            if ('SecretString' in data) {
                secrets = data.SecretString;
            } else {
                // if secret is binary
                let buff = new Buffer(data.SecretBinary, 'base64');
                secrets = buff.toString('ascii');
            }
            resolve(JSON.parse(secrets));
        });
    });
};