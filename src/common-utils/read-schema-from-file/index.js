const { readFileSync } = require('fs');


const readSchemaFromFile = (_path) => {
    console.log('PATH:',_path);
    console.log('DIRNAME:',__dirname);
    const SCHEMA = readFileSync(`${__dirname}${_path}`).toString('utf-8');
    console.log('SCHEMA:', SCHEMA);
    return SCHEMA;
}
//console.log(readSchemaFromFile('./schema/Party/schema.graphql'));
module.exports = readSchemaFromFile;


