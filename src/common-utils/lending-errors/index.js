// Error codes structure takes from API Styleguide published by the API Champions team here https://confluence.int.ally.com/display/EAP/Error+Handling

class SystemError extends Error {
    constructor(message = 'Service is currently unavailable. Please try again later.', statusCode = 503, correlationId = null) {
        super(message);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'SYSTEM';
        this.statusCode = statusCode;
        this.info = {
            code: 'F-BILD-LAPI-503',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class DomainError extends Error {
    constructor(message = 'Please check your request and try again.', statusCode = 400, correlationId = null) {
        super(message);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-400',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class TimeoutError extends SystemError {
    constructor(message = 'Execution aborted due to timeout. Please try again later.', statusCode = 504, correlationId = null) {
        super(message, statusCode, correlationId);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'SYSTEM';
        this.statusCode = statusCode;
        this.info = {
            code: 'F-BILD-LAPI-504',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class AuthorizationError extends DomainError {
    constructor(message = 'Unauthorized.', statusCode = 401, correlationId = null) {
        super(message, statusCode, correlationId);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-401',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class ForbiddenError extends DomainError {
    constructor(message = 'Forbidden', statusCode = 403, correlationId = null) {
        super(message, statusCode, correlationId);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-403',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class RecordNotFoundError extends DomainError {
    constructor(message = 'Not Record Found', statusCode = 404, correlationId = null) {
        super(message, statusCode, correlationId);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-404',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class ValidationError extends DomainError {
    constructor(property, statusCode, correlationId) {
        super(`Failed to validate ${property}`, statusCode = 422, correlationId = null);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-422',
            correlationId,
            property: this.property
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

const sanitizeError = (error) => {
    
    if (error instanceof DomainError || error.type === 'DOMAIN') {
        console.warn(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: Domain Error :: ${error}. Return error to consumer.`);
        return error;
    }
    console.error(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: System Error :: ${error}. Sanitizing error prior to returning to consumer.`);
    return new SystemError(undefined, undefined, error.info.correlationId);
}

module.exports = {
    DomainError,
    SystemError,
    TimeoutError,
    AuthorizationError,
    ForbiddenError,
    RecordNotFoundError,
    ValidationError,
    sanitizeError
}