
// This alternate implementation worked with the following appconfig AppConfig        # APPCONFIG_PARAMS =  jsonencode({
// "appConfig": {
// "Application": "${aws_appconfig_application.czl74b-lambda_config.name}",
// "Environment": "${aws_appconfig_application.czl74b-lambda_config.name}",
// "Configuration": "${aws_appconfig_configuration_profile.czl74b-lambda_config.configuration_profile_id}",
// "ClientId": "lapi"
// }
// }),

const AWS = require('aws-sdk');

const getAppConfigData = async () => {   
    const appconfig = new AWS.AppConfig({ apiVersion: '2019-10-09' });
    const appconfigParams = JSON.parse(process.env.APPCONFIG_PARAMS);
    const appConfigResponse = await appconfig.getConfiguration(appconfigParams.appConfig).promise().catch(err => {
        console.log(err);
    });

    const appConfigResponseJSON = JSON.parse(Buffer.from(appConfigResponse.Content, 'base64').toString('ascii'));
    console.log("readAccountSnapshot :: AppConfig Response ::", appConfigResponseJSON);
    return appConfigResponseJSON;
}

module.exports = { getAppConfigData };