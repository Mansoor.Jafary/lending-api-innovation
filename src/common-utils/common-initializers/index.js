// Observations:
// 1. HTTP Call is significantly faster compared to SDK version -- difference in calls was noted to be 2 to 3 digit milliseconds

const getAppConfigData = async () => {
  console.log('getAppConfigData :: AppConfig Response :: START');
  const http = require('http'); //lazy load
  const { APPCONFIG_LOCALHOST } = process.env;
  console.log(`getAppConfigData :: AppConfig LOCALHOST :: ${APPCONFIG_LOCALHOST}`);
  
  const res = await new Promise((resolve, reject) => {
    http.get(
      `${APPCONFIG_LOCALHOST}`,
      resolve
    );
  });

  let configData = await new Promise((resolve, reject) => {
    let data = '';
    res.on('data', chunk => data += chunk);
    res.on('error', err => reject(err));
    res.on('end', () => resolve(data));
  });

  const parsedConfigData = JSON.parse(configData);
  console.log("getAppConfigData :: AppConfig Response ::", parsedConfigData);
  return parsedConfigData;
}

// todo: Decommission getAppConfigAlernate - this function is slower than getAppConfigData
const getAppConfigDataAlternate = async () => {
  console.log('getAppConfigDataAlternate :: AppConfig Response :: START');
  const AWS = require('aws-sdk'); //lazy load
  const { E0 } = process.env;
  if (E0 === 'TRUE') {
    // lazy load proxy-agent only in e0 for sam local testing
    const proxy = require('proxy-agent');
    AWS.config.update({ httpOptions: { agent: proxy(HTTP_PROXY) } });
  }
  const appconfig = new AWS.AppConfig({ apiVersion: '2019-10-09' });
  const appconfigParams = JSON.parse(process.env.APPCONFIG_PARAMS);
  const appConfigResponse = await appconfig.getConfiguration(appconfigParams.appConfig).promise().catch(err => {
    console.log(err);
  });

  const parsedConfigData = JSON.parse(Buffer.from(appConfigResponse.Content, 'base64').toString('ascii'));
  console.log("getAppConfigDataAlternate :: AppConfig Response ::", parsedConfigData);
  return parsedConfigData;
}

module.exports = { getAppConfigData, getAppConfigDataAlternate };