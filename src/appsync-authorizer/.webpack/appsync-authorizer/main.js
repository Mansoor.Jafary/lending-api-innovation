/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ([
/* 0 */,
/* 1 */
/***/ ((module) => {

"use strict";
module.exports = require("graphql");

/***/ })
/******/ 	]);
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
const { parse } = __webpack_require__(1);

// Function authorizes GraphQL Query fields based on scopes in authorizationToken
function getScopesValidator(apiId) {
  return function validateScopes(tokenScopes, queriedFields) {
    deniedFields = [];
    for ( let i = 0; i < queriedFields.length; i++ ) {
      if (!tokenScopes.includes(queriedFields[i])) {
        deniedFields.push(`arn:aws:appsync:us-east-1:650487181597:apis/${apiId}/types/Query/fields/${queriedFields[i]}`)
      }
    }
    return deniedFields;
  }
}

// Function traverses AST to returns the fields in the GraphQL Query request
function getFields(selectionSet) {
  // TODO: handle all field nodes in other fragments
  return selectionSet.selections.map(
    (selection) =>
      // TODO: handle fragments
      selection.name.value
  );
}
exports.handler =  async function(event, context) {
  console.log("EVENT:", JSON.stringify(event, null, 2));
  // console.log("CONTEXT:", JSON.stringify(context, null, 2));


    const { requestContext: { apiId, queryString }, authorizationToken } = event;
    const APIGEE_KEY = process.env.APIGEE_KEY;

    // TODO: Remove and replace use of testAuthorizationToken with authorizationToken received in event
    // TOTO: generate apigee_key and get dynamically
    const testAuthorizationToken = JSON.stringify({
      apigee_key: process.env.APIGEE_KEY,
      scopes: 'mutation:_service:_entities:hello:fineGrainAuthFail:accountByAccountID:createNewInstallmentLoansUploadURL:bgTest'
    });
    // console.log("authorizationToken:", authorizationToken);
    // console.log("queryString:", queryString);

    const {apigee_key, scopes} = JSON.parse(testAuthorizationToken);
    const tokenScopes = scopes.split(':');
    const queryObj = parse(queryString);
    console.log('Query Object:', queryObj);
    const queriedFields = getFields(queryObj.definitions[0].selectionSet);
    console.log('Queried Fields:', queriedFields);
    console.log('Token Scopes:', tokenScopes);

    if(apigee_key !== APIGEE_KEY) {
      console.log("APIGEE_KEY: ", APIGEE_KEY);
      console.log("apigee_key: ", apigee_key);
      console.log("APIGEE_KEY Mismatch - Authorization Failed!");
      return {  
        isAuthorized: false,
      };
    }

    const validateScopes = getScopesValidator(apiId);
    const deniedFields = validateScopes(tokenScopes, queriedFields);
    console.log('Denied Fields:', deniedFields);
    const permissions = {  
      isAuthorized: false,
      deniedFields: [
        ...deniedFields
      ]
    };

    if (queriedFields.length > deniedFields.length) {
      permissions.isAuthorized = true;
    }

    console.log(permissions);
    return permissions;
}
  
})();

var __webpack_export_target__ = exports;
for(var i in __webpack_exports__) __webpack_export_target__[i] = __webpack_exports__[i];
if(__webpack_exports__.__esModule) Object.defineProperty(__webpack_export_target__, "__esModule", { value: true });
/******/ })()
;
//# sourceMappingURL=main.js.map