const { parse } = require('graphql');

// Function authorizes GraphQL Query fields based on scopes in authorizationToken
function getScopesValidator(apiId) {
  return function validateScopes(tokenScopes, queriedFields) {
    deniedFields = [];
    for ( let i = 0; i < queriedFields.length; i++ ) {
      if (!tokenScopes.includes(queriedFields[i])) {
        deniedFields.push(`arn:aws:appsync:us-east-1:650487181597:apis/${apiId}/types/Query/fields/${queriedFields[i]}`)
      }
    }
    return deniedFields;
  }
}

// Function traverses AST to returns the fields in the GraphQL Query request
function getFields(selectionSet) {
  // TODO: handle all field nodes in other fragments
  return selectionSet.selections.map(
    (selection) =>
      // TODO: handle fragments
      selection.name.value
  );
}
exports.handler =  async function(event, context) {
  console.log("EVENT:", JSON.stringify(event, null, 2));
  // console.log("CONTEXT:", JSON.stringify(context, null, 2));


    const { requestContext: { apiId, queryString }, authorizationToken } = event;
    const APIGEE_KEY = process.env.APIGEE_KEY;

    // TODO: Remove and replace use of testAuthorizationToken with authorizationToken received in event
    // TOTO: generate apigee_key and get dynamically
    const testAuthorizationToken = JSON.stringify({
      apigee_key: process.env.APIGEE_KEY,
      scopes: 'mutation:_service:_entities:hello:fineGrainAuthFail:accountByAccountID:createNewInstallmentLoansUploadURL:bgTest'
    });
    // console.log("authorizationToken:", authorizationToken);
    // console.log("queryString:", queryString);

    const {apigee_key, scopes} = JSON.parse(testAuthorizationToken);
    const tokenScopes = scopes.split(':');
    const queryObj = parse(queryString);
    console.log('Query Object:', queryObj);
    const queriedFields = getFields(queryObj.definitions[0].selectionSet);
    console.log('Queried Fields:', queriedFields);
    console.log('Token Scopes:', tokenScopes);

    if(apigee_key !== APIGEE_KEY) {
      console.log("APIGEE_KEY: ", APIGEE_KEY);
      console.log("apigee_key: ", apigee_key);
      console.log("APIGEE_KEY Mismatch - Authorization Failed!");
      return {  
        isAuthorized: false,
      };
    }

    const validateScopes = getScopesValidator(apiId);
    const deniedFields = validateScopes(tokenScopes, queriedFields);
    console.log('Denied Fields:', deniedFields);
    const permissions = {  
      isAuthorized: false,
      deniedFields: [
        ...deniedFields
      ]
    };

    if (queriedFields.length > deniedFields.length) {
      permissions.isAuthorized = true;
    }

    console.log(permissions);
    return permissions;
}
  