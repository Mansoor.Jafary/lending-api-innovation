/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ([
/* 0 */,
/* 1 */
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Not supported - Need Lambda Node Runtime v16.x
// const { setTimeout: setTimeoutPromise } = require('timers/promises');
const LendingResponse = __webpack_require__(2);
const { getAppConfigData } = __webpack_require__(3);
const initExecutionTimer = __webpack_require__(7)();

const {
  DomainError,
  SystemError,
  AuthorizationError,
  ForbiddenError,
  RecordNotFoundError,
  ValidationError,
  TimeoutError,
  sanitizeError
} = __webpack_require__(9);


function getFunctionCaller({ awsRequestId }) {
  console.log('test');
  return async function callFunction(functionURI, payload) {
    console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction :: START`);
    const { AWS_REGION, E0 } = process.env;

    const callFunctionTimer = initExecutionTimer('callFunction');
    const timer1 = callFunctionTimer.startExecutionTimer();

    const AWS = __webpack_require__(5); // lazy loading aws-sdk
    if (E0 === 'TRUE') {
      // lazy load proxy-agent only in e0 for sam local testing
      const proxy = __webpack_require__(6);
      AWS.config.update({ httpOptions: { agent: proxy(HTTP_PROXY) } });
    }
    const lambda = new AWS.Lambda({ region: AWS_REGION });
    const appConfigData = await getAppConfigData();

    // const { globalAppConfigs: { functionCallerTimeout } = {} } = appConfigData;

    // not supported until Lambda Runtime Node v16
    // const cancelTimeout = new AbortController();
    // const cancelPromise = new AbortController(); // cannot abort promise

    // Not supported - Need Lambda Node Runtime v16.x
    // const functionCallTimeout = new Promise(async (resolve, reject) => {
    //   try {
    //     await setTimeoutPromise(functionCallerTimeout, undefined, { signal: cancelTimeout.signal });
    //     // cancelPromise.abort(); // cannot abort Promise
    //     return resolve(new LendingResponse(new TimeoutError(undefined, undefined, awsRequestId), null));
    //   } catch (err) {
    //     if (err.name === 'AbortError') {
    //       console.log('This timeout control was aborted');
    //     }
    //   }
    // });

    // Used to clear timeout. This approach only until v16 is not supported
    // let functionTimeoutSignal = null;

    // const functionCallTimeout = new Promise(resolve => {
    //     console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.functionCallTimeout :: START`);

    //     functionTimeoutSignal = setTimeout(() => {
    //       console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.functionCallTimeout :: Timeout Triggered after ${functionCallerTimeout}ms`);
    //       return resolve(new LendingResponse(new TimeoutError(undefined, undefined, awsRequestId), null));
    //     }, functionCallerTimeout) ;
    //   });

    const callFunctionResponse = new Promise(async (resolve, reject) => {
        console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: START`);
        const callFunctionResponseTimer = initExecutionTimer('callFunctionResponse');
        const timer1 = callFunctionResponseTimer.startExecutionTimer()

        // alternate for setTimeoutPromise function until Lambda supports Node Runtime v16
        // console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: AppConfig functionCallerTimeout Value :: ${functionCallerTimeout}`);

        const invokeParams = {};
        invokeParams.FunctionName = functionURI;
        invokeParams.Payload = JSON.stringify(payload);
        console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Invoke Request :: ${JSON.stringify(invokeParams)}`);
        try {
          lambda.invoke(invokeParams, (error, data) => {
            if (error) {
              console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Invoke Error :: ${error}`);
              return resolve(new LendingResponse(new SystemError(undefined, undefined, awsRequestId), null));
            }
            console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Result :: ${JSON.stringify(data)}`);
            const resJson = JSON.parse(data.Payload);

            if (resJson.error instanceof Error || resJson.error && resJson.error.text) {
              console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Final Response with Error :: ${JSON.stringify(resJson)}`); 
              callFunctionResponseTimer.logExecutionDuration(timer1);
              return resolve(new LendingResponse(resJson.error, null));           
            }
            
            console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Lambda Response :: ${JSON.stringify(resJson)}`);
            callFunctionResponseTimer.logExecutionDuration(timer1);
            
            if (resJson.data) {
                return resolve(new LendingResponse(null, resJson.data));
            } else { // todo: this is an anti-pattern. Do we want to keep it?
                return resolve(new LendingResponse(null, resJson));
            }
          });
        } catch (error) { // Invoke response is { Payload: { error: null } }
          console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getFunctionCaller.callFunction.callFunctionResponse :: Unexpected Error :: ${error}`);
          return resolve(new LendingResponse(resJson.error, null));
        }
      });

    return Promise.race([
      callFunctionResponse,
      // functionCallTimeout
    ]).then((result) => {
      if (result.error instanceof Error || result.error && result.error.text) {
        // Sanitize error before return to caller
        result.error = sanitizeError(result.error);
      }
      callFunctionTimer.logExecutionDuration(timer1);
      // clearTimeout(functionTimeoutSignal);
      console.log(`Promise.race :: Returning Result :: ${JSON.stringify(result)}`);
      return result;
    });
  }
}

function getRestAPICaller({ awsRequestId }) {
  console.log('API Caller :: getRestAPICaller START');

  return async function callRestAPI(config) {
    console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI :: START`);
    console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI :: Config: ${config}`);

    const axios = __webpack_require__(10); // lazy loading axios 
    const { AbortController } = __webpack_require__(11); // lazy loading node-abort-controller

    const appConfigData = await getAppConfigData();
    const { globalAppConfigs: { apiCallerTimeout = null } = {} } = appConfigData;

    const cancelRequest = new AbortController();

    // Track start time of the call
    axios.interceptors.request.use(
      function (config) {
        config.proxy = false;
        // set default method as POST
        if (!config.method) {
          config.method = 'POST';
        } 
        console.info('Incoming Timeout:', config.timeout);
        // set a default global configurable timeout value for the call
        if (!config.timeout) {
          config.timeout = apiCallerTimeout;
        }
        config.signal = cancelRequest.signal;
        Object.assign(config.headers, defaultHeaders);
        config.metadata = { startTime: new Date() }
        console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI  :: Interceptor Config Updated ::', ${JSON.stringify(config)}`);
        return config;
      },
      function (error) {
        return Promise.reject(error);
      }
    );

    // Track end time of the call
    axios.interceptors.response.use(
      function (response) {
        response.config.metadata.endTime = new Date();
        response.duration = response.config.metadata.endTime - response.config.metadata.startTime;
        return response;
      },
      function (error) {
        console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI :: axios Interceptor Error :: ${error}`);
        if (!axios.isCancel(error)) {
          error.config.metadata.endTime = new Date();
          error.duration = error.config.metadata.endTime - error.config.metadata.startTime;
        }
        return Promise.reject(error);
      }
    );

    const defaultHeaders = {
      'Content-Type': 'application/json',
    };

    const callAPIResponse = new Promise((resolve) => {
      console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI.callAPIResponse :: START`);
      const callAPIResponseTimer = initExecutionTimer('callFunctionResponse');
      const timer1 = callAPIResponseTimer.startExecutionTimer()

      // implement timeout for axios call
      const apiTimeoutSignal = setTimeout(() => {
        console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI.callAPIResponse :: Timeout Triggered after ${apiCallerTimeout}ms`);
        cancelRequest.abort();
      }, apiCallerTimeout);

      axios(config)
        .then(result => {
          // axios returned a response. clear the timeout timer
          clearTimeout(apiTimeoutSignal);
          console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callAPIResponse :: Success :: ${result}`);
          callAPIResponseTimer.logExecutionDuration(timer1);
          return resolve(new LendingResponse(null, result.data));
        })
        .catch(error => {
          if (axios.isCancel(error)) {
            console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callRestAPI :: Returning response after API Call Cancelled`);
            return resolve(new LendingResponse(new TimeoutError(undefined, undefined, awsRequestId), null));
          } else {
            // axios call was not cancelled due to timeout. clear the timeout timer
            clearTimeout(apiTimeoutSignal);
            if (error.response) {
              // The request was made and the server responded with a status code that falls out of the range of 2xx
              console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callAPIResponse :: Error Response:: ${error.message}`);
              if (error.response.status === 401) {
                return resolve(new LendingResponse(new AuthorizationError('Unauthorized', 401, awsRequestId), null));
              } else if (error.response.status === 403) {
                return resolve(new LendingResponse(new ForbiddenError('Forbidden', 403, awsRequestId), null));
              } else if (error.response.status >= 400 && e.response.status < 500) {
                return resolve(new LendingResponse(new DomainError('There is a problem with your request. Please check your query and try again', 400, awsRequestId), null));
              }
            } else if (error.request) {
              // The request was made but no response was received
              console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callAPIResponse :: Error No Response from API Call :: ${error.message}`);
              return resolve(new LendingResponse(new SystemError(undefined, undefined, awsRequestId), null));
            } else {
              // Something happened in setting up the request that triggered an Error
              console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callAPIResponse :: Error Occured With Message :: ${error.message}`);
              return resolve(new LendingResponse(new SystemError(undefined, undefined, awsRequestId), null));
            }
            console.log(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: getRestAPICaller.callAPIResponse :: Unknown Error :: ${error.message}`);
            return resolve(new LendingResponse(new SystemError(undefined, undefined, awsRequestId), null));
          }
        })
    });

    return callAPIResponse.then((result) => {
      if (result.error instanceof Error) {
        // Sanitize error before return to caller
        result.error = sanitizeError(result.error);
      }
      return result;
    });
  }
}

module.exports = { getRestAPICaller, getFunctionCaller }

/***/ }),
/* 2 */
/***/ ((module) => {

class LendingResponse {
    constructor(error, data) {
        this.error = error;
        this.data = data;
    }
}

module.exports = LendingResponse;

/***/ }),
/* 3 */
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Observations:
// 1. HTTP Call is significantly faster compared to SDK version -- difference in calls was noted to be 2 to 3 digit milliseconds

const getAppConfigData = async () => {
  console.log('getAppConfigData :: AppConfig Response :: START');
  const http = __webpack_require__(4); //lazy load
  const { APPCONFIG_LOCALHOST } = process.env;
  console.log(`getAppConfigData :: AppConfig LOCALHOST :: ${APPCONFIG_LOCALHOST}`);
  
  const res = await new Promise((resolve, reject) => {
    http.get(
      `${APPCONFIG_LOCALHOST}`,
      resolve
    );
  });

  let configData = await new Promise((resolve, reject) => {
    let data = '';
    res.on('data', chunk => data += chunk);
    res.on('error', err => reject(err));
    res.on('end', () => resolve(data));
  });

  const parsedConfigData = JSON.parse(configData);
  console.log("getAppConfigData :: AppConfig Response ::", parsedConfigData);
  return parsedConfigData;
}

// todo: Decommission getAppConfigAlernate - this function is slower than getAppConfigData
const getAppConfigDataAlternate = async () => {
  console.log('getAppConfigDataAlternate :: AppConfig Response :: START');
  const AWS = __webpack_require__(5); //lazy load
  const { E0 } = process.env;
  if (E0 === 'TRUE') {
    // lazy load proxy-agent only in e0 for sam local testing
    const proxy = __webpack_require__(6);
    AWS.config.update({ httpOptions: { agent: proxy(HTTP_PROXY) } });
  }
  const appconfig = new AWS.AppConfig({ apiVersion: '2019-10-09' });
  const appconfigParams = JSON.parse(process.env.APPCONFIG_PARAMS);
  const appConfigResponse = await appconfig.getConfiguration(appconfigParams.appConfig).promise().catch(err => {
    console.log(err);
  });

  const parsedConfigData = JSON.parse(Buffer.from(appConfigResponse.Content, 'base64').toString('ascii'));
  console.log("getAppConfigDataAlternate :: AppConfig Response ::", parsedConfigData);
  return parsedConfigData;
}

module.exports = { getAppConfigData, getAppConfigDataAlternate };

/***/ }),
/* 4 */
/***/ ((module) => {

"use strict";
module.exports = require("http");

/***/ }),
/* 5 */
/***/ ((module) => {

"use strict";
module.exports = require("aws-sdk");

/***/ }),
/* 6 */
/***/ ((module) => {

"use strict";
module.exports = require("proxy-agent");

/***/ }),
/* 7 */
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

const now = __webpack_require__(8);

function initExecutionTimer(moduleName) {
    const lambdaName = moduleName || process.env.AWS_LAMBDA_FUNCTION_NAME;
    return (marker) => {
        let timeStop = 0;
        return {
            startExecutionTimer: () => {
                const startTime = now();
                return startTime;
            },
            logExecutionDuration: (startTimer) => {
                timeStop++;
                const endTime = now();
                const duration = endTime - startTimer;
                console.log(`${lambdaName} :: ${marker} :: Stop ${timeStop} :: Duration ${duration.toFixed(2)}`);
            }
        }
    }
}

module.exports = initExecutionTimer;

/***/ }),
/* 8 */
/***/ ((module) => {

"use strict";
module.exports = require("performance-now");

/***/ }),
/* 9 */
/***/ ((module) => {

// Error codes structure takes from API Styleguide published by the API Champions team here https://confluence.int.ally.com/display/EAP/Error+Handling

class SystemError extends Error {
    constructor(message = 'Service is currently unavailable. Please try again later.', statusCode = 503, correlationId = null) {
        super(message);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'SYSTEM';
        this.statusCode = statusCode;
        this.info = {
            code: 'F-BILD-LAPI-503',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class DomainError extends Error {
    constructor(message = 'Please check your request and try again.', statusCode = 400, correlationId = null) {
        super(message);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-400',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class TimeoutError extends SystemError {
    constructor(message = 'Execution aborted due to timeout. Please try again later.', statusCode = 504, correlationId = null) {
        super(message, statusCode, correlationId);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'SYSTEM';
        this.statusCode = statusCode;
        this.info = {
            code: 'F-BILD-LAPI-504',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class AuthorizationError extends DomainError {
    constructor(message = 'Unauthorized.', statusCode = 401, correlationId = null) {
        super(message, statusCode, correlationId);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-401',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class ForbiddenError extends DomainError {
    constructor(message = 'Forbidden', statusCode = 403, correlationId = null) {
        super(message, statusCode, correlationId);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-403',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class RecordNotFoundError extends DomainError {
    constructor(message = 'Not Record Found', statusCode = 404, correlationId = null) {
        super(message, statusCode, correlationId);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-404',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class ValidationError extends DomainError {
    constructor(property, statusCode, correlationId) {
        super(`Failed to validate ${property}`, statusCode = 422, correlationId = null);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-422',
            correlationId,
            property: this.property
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

const sanitizeError = (error) => {
    
    if (error instanceof DomainError || error.type === 'DOMAIN') {
        console.warn(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: Domain Error :: ${error}. Return error to consumer.`);
        return error;
    }
    console.error(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: System Error :: ${error}. Sanitizing error prior to returning to consumer.`);
    return new SystemError(undefined, undefined, error.info.correlationId);
}

module.exports = {
    DomainError,
    SystemError,
    TimeoutError,
    AuthorizationError,
    ForbiddenError,
    RecordNotFoundError,
    ValidationError,
    sanitizeError
}

/***/ }),
/* 10 */
/***/ ((module) => {

"use strict";
module.exports = require("axios");

/***/ }),
/* 11 */
/***/ ((module) => {

"use strict";
module.exports = require("node-abort-controller");

/***/ }),
/* 12 */
/***/ ((module) => {

// const _ = require('lodash');

const responseBuilder = (response) => {
    console.log("readBalancesSnapshot :: responseBuilder ::", response);
    // const readBalancesPositionResponse = response
    // {
    //     currentBalance: _.get(response, 'currentBalance', null),
    //     allocatedCreditLimit: _.get(response, 'allocatedCreditLimit', null),
    //     availableCreditLimit: _.get(response, 'availableCreditLimit', null)
    // }
    // console.log("readBalancesSnapshot :: Created Response ::", readBalancesPositionResponse);
    // return readBalancesPositionResponse;

    const readBalancesPositionResponse = response;
    return readBalancesPositionResponse;
}

module.exports = responseBuilder;

/***/ }),
/* 13 */
/***/ ((module) => {

const requestBuilder = (
    { accountID }
) => {
    console.log("readAccountSnapshot :: requestBuilder Proxy Account Number ::", accountID);
    const payload = {
        accountID
    };
    return payload;
};

module.exports = requestBuilder;

/***/ })
/******/ 	]);
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
const { getFunctionCaller }  = __webpack_require__(1);
const LendingResponse = __webpack_require__(2);
const { sanitizeError } = __webpack_require__(9);

const responseBuilder = __webpack_require__(12);
const requestBuilder = __webpack_require__(13);

exports.handler = async function(event, context) {
    const startTime = new Date();
    console.log('readBalancesSnapshot :: Event ::', event);

    return new Promise(async (resolve, reject) => {
        const callFunction = getFunctionCaller(context);
        const functionURI = JSON.parse(process.env.URIS).balanceInquiry;
        const payload = requestBuilder(event);
        const endTimeRequestBuilder = new Date();
        const requestBuilderDuration = endTimeRequestBuilder - startTime;
        console.log('readBalancesSnapshot :: RequestBuilder Duration ::', requestBuilderDuration);
        console.log('readBalancesSnapshot :: RequestBuilder ::', payload);

        callFunction (functionURI, payload)
        .then(result => {
            if(!(result.error instanceof Error)) {
                const endTimeLambdaInvoke = new Date();
                const lambdaInvokeDuration = endTimeLambdaInvoke - startTime;
                console.log('readBalancesSnapshot :: Lambda Invoke Duration ::', lambdaInvokeDuration);
                console.log("readBalancesSnapshot :: SOR Lambda Response ::", result.data);
                const transformed = responseBuilder(result.data);
                const endTimeResponseTransform = new Date();
                const responseTransformDuration = endTimeResponseTransform - startTime;
                console.log('readBalancesSnapshot :: SOR Response Transform Duration ::', responseTransformDuration);
                console.log('readBalancesSnapshot :: Transformed Response ::', transformed);
                return resolve(new LendingResponse(null, transformed));
            }
            console.log('readBalancesSnapshot :: Final Response with Error ::', result);
            return resolve(new LendingResponse(sanitizeError(result.error), null));
        })
        .catch(e => {
            console.log('readBalancesSnapshot :: Make API Call Error ::', e);
            return resolve(new LendingResponse(sanitizeError(e), null));
        });
    });
};
})();

var __webpack_export_target__ = exports;
for(var i in __webpack_exports__) __webpack_export_target__[i] = __webpack_exports__[i];
if(__webpack_exports__.__esModule) Object.defineProperty(__webpack_export_target__, "__esModule", { value: true });
/******/ })()
;
//# sourceMappingURL=main.js.map