const LendingResponse = require('../../../../src/common-utils/nodejs/node_modules/lending-response/index');
const { DomainError } = require('../../../../src/common-utils/nodejs/node_modules/lending-errors/index');

const app = require('./main');

const OLD_ENV = process.env;
const URIS = JSON.stringify({
    balanceInquiry: 'foo'
});

beforeAll(() => {
    jest.resetModules()
    process.env = {
        ...OLD_ENV,
        URIS
    };
});

afterAll(() => {
  process.env = OLD_ENV;
});


describe('Lambda Function', function () {
    it('Returns a Promise', async () => {
        const event = {};
        const context = {};
        return expect(app.handler(event, context)).toBeInstanceOf(Promise);
    });

    it('Returned Promise resolves to a LendingResponse', () => {
        const event = {
            proxyAccountNumber: 'X001'
        };
        const context = {};
        const result = app.handler(event, context);
        return expect(result).resolves.toBeInstanceOf(LendingResponse);
    });
});

describe('Invoke Lambda', function () {
    it('Resolves success response when invoke lambda is successful with no error', () => {
        const event = {
            proxyAccountNumber: 'X001'
        };
        const context = {};

        const expectedResponse = new LendingResponse(null, { foo: 'bar' });
        const result = app.handler(event, context);
        return expect(result).resolves.toStrictEqual(expectedResponse);
    });

    // tried didnt work https://stackoverflow.com/questions/52551035/tobeinstanceofnumber-does-not-work-in-jest
    it('Resolves success error response when invoke lambda is successful with error', async () => {
        const event = {
            proxyAccountNumber: 'X999'
        };
        const context = {};
        
        const result = await app.handler(event, context);
        expect(result.data).toEqual(null);
        expect(result.error).toBeInstanceOf(DomainError);
    });

    it('Handles rejected promise when invokeinvoke lambda error', async () => {
        const event = {
            proxyAccountNumber: 'X999'
        };
        const context = {};
        const result = app.handler(event, context);
        return expect(result).rejects;

    });

});
