const { getFunctionCaller }  = require('../../../common-utils/api-caller/index');
const LendingResponse = require('../../../common-utils/lending-response/index');
const { sanitizeError } = require('../../../common-utils/lending-errors/index');

const responseBuilder = require('./builder/responseBuilder');
const requestBuilder = require('./builder/balanceInquiryRequestBuilder');

exports.handler = async function(event, context) {
    const startTime = new Date();
    console.log('readBalancesSnapshot :: Event ::', event);

    return new Promise(async (resolve, reject) => {
        const callFunction = getFunctionCaller(context);
        const functionURI = JSON.parse(process.env.URIS).balanceInquiry;
        const payload = requestBuilder(event);
        const endTimeRequestBuilder = new Date();
        const requestBuilderDuration = endTimeRequestBuilder - startTime;
        console.log('readBalancesSnapshot :: RequestBuilder Duration ::', requestBuilderDuration);
        console.log('readBalancesSnapshot :: RequestBuilder ::', payload);

        callFunction (functionURI, payload)
        .then(result => {
            if(!(result.error instanceof Error)) {
                const endTimeLambdaInvoke = new Date();
                const lambdaInvokeDuration = endTimeLambdaInvoke - startTime;
                console.log('readBalancesSnapshot :: Lambda Invoke Duration ::', lambdaInvokeDuration);
                console.log("readBalancesSnapshot :: SOR Lambda Response ::", result.data);
                const transformed = responseBuilder(result.data);
                const endTimeResponseTransform = new Date();
                const responseTransformDuration = endTimeResponseTransform - startTime;
                console.log('readBalancesSnapshot :: SOR Response Transform Duration ::', responseTransformDuration);
                console.log('readBalancesSnapshot :: Transformed Response ::', transformed);
                return resolve(new LendingResponse(null, transformed));
            }
            console.log('readBalancesSnapshot :: Final Response with Error ::', result);
            return resolve(new LendingResponse(sanitizeError(result.error), null));
        })
        .catch(e => {
            console.log('readBalancesSnapshot :: Make API Call Error ::', e);
            return resolve(new LendingResponse(sanitizeError(e), null));
        });
    });
};