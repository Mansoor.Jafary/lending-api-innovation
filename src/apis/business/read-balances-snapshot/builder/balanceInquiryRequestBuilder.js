const requestBuilder = (
    { accountID }
) => {
    console.log("readAccountSnapshot :: requestBuilder Proxy Account Number ::", accountID);
    const payload = {
        accountID
    };
    return payload;
};

module.exports = requestBuilder;