const AWS = require('aws-sdk');
const lambda = new AWS.Lambda({ region: 'us-east-1'});

const responseBuilder = require('./builder/responseBuilder');
const requestBuilder = require('./builder/balanceInquiryRequestBuilder');

exports.handler =  async function(event, context, callback) {
    const params = requestBuilder(event);
    const invokeRes = await lambda.invoke(params).promise().catch( err => 
    {
        console.log(err);
        callback(err);
    });

    console.log("readBalancesSnapshot :: balanceInquiry Result ::", invokeRes);
    const balancesInquiryRespponse = JSON.parse(invokeRes.Payload);
    const balancesPosition = responseBuilder(balancesInquiryRespponse);
    console.log("readBalancesSnapshot :: balanceInquiry Result Transformed ::", balancesPosition);

    callback(null, balancesPosition);
};