const requestBuilder = async (
    { accountID }
) => {
    console.log("readPaymentSnapshot :: requestBuilder Proxy Account Number ::", accountID);
    const payload = {
        accountID
    };
    return payload;
};

module.exports = requestBuilder;