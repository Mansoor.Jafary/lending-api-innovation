/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ([
/* 0 */,
/* 1 */
/***/ ((module) => {

"use strict";
module.exports = require("openpgp");

/***/ }),
/* 2 */
/***/ ((module) => {

"use strict";
module.exports = require("aws-sdk");

/***/ }),
/* 3 */
/***/ ((module) => {

class LendingResponse {
    constructor(error, data) {
        this.error = error;
        this.data = data;
    }
}

module.exports = LendingResponse;

/***/ }),
/* 4 */
/***/ ((module) => {

// Error codes structure takes from API Styleguide published by the API Champions team here https://confluence.int.ally.com/display/EAP/Error+Handling

class SystemError extends Error {
    constructor(message = 'Service is currently unavailable. Please try again later.', statusCode = 503, correlationId = null) {
        super(message);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'SYSTEM';
        this.statusCode = statusCode;
        this.info = {
            code: 'F-BILD-LAPI-503',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class DomainError extends Error {
    constructor(message = 'Please check your request and try again.', statusCode = 400, correlationId = null) {
        super(message);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-400',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class TimeoutError extends SystemError {
    constructor(message = 'Execution aborted due to timeout. Please try again later.', statusCode = 504, correlationId = null) {
        super(message, statusCode, correlationId);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'SYSTEM';
        this.statusCode = statusCode;
        this.info = {
            code: 'F-BILD-LAPI-504',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class AuthorizationError extends DomainError {
    constructor(message = 'Unauthorized.', statusCode = 401, correlationId = null) {
        super(message, statusCode, correlationId);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-401',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class ForbiddenError extends DomainError {
    constructor(message = 'Forbidden', statusCode = 403, correlationId = null) {
        super(message, statusCode, correlationId);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-403',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class RecordNotFoundError extends DomainError {
    constructor(message = 'Not Record Found', statusCode = 404, correlationId = null) {
        super(message, statusCode, correlationId);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-404',
            correlationId
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

class ValidationError extends DomainError {
    constructor(property, statusCode, correlationId) {
        super(`Failed to validate ${property}`, statusCode = 422, correlationId = null);
        this.message = message;
        this.text = message;
        this.name = this.constructor.name;
        this.type = 'DOMAIN';
        this.statusCode = statusCode;
        this.info = {
            code: 'W-BILD-LAPI-422',
            correlationId,
            property: this.property
        };

        Error.captureStackTrace(this, this.constructor);
    }
}

const sanitizeError = (error) => {
    
    if (error instanceof DomainError || error.type === 'DOMAIN') {
        console.warn(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: Domain Error :: ${error}. Return error to consumer.`);
        return error;
    }
    console.error(`${process.env.AWS_LAMBDA_FUNCTION_NAME} :: System Error :: ${error}. Sanitizing error prior to returning to consumer.`);
    return new SystemError(undefined, undefined, error.info.correlationId);
}

module.exports = {
    DomainError,
    SystemError,
    TimeoutError,
    AuthorizationError,
    ForbiddenError,
    RecordNotFoundError,
    ValidationError,
    sanitizeError
}

/***/ })
/******/ 	]);
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
const openpgp = __webpack_require__(1);
const { S3, SSM } = __webpack_require__(2);
const LendingResponse = __webpack_require__(3);
const { sanitizeError } = __webpack_require__(4);

const s3 = new S3({ signatureVersion: 'v4' });
const ssm = new SSM();

const generateId = (correlationId, clientId) => `${clientId}/${correlationId}`;

const buildPublicKeyPath = (id) => `${id}/public.key`;

const generatePassword = () => Math.random().toString(36).substr(2);

const generatePgpKeys = (password) => openpgp.generateKey({
  type: 'rsa',
  rsaBits: 2048,
  userIDs: [{ name: 'Secure File Upload' }],
  passphrase: password,
  format: 'armored'
});

const generateSignedUrlForGet = (bucket, id, expires) => s3.getSignedUrlPromise('getObject', {
  Bucket: bucket,
  Key: id,
  Expires: expires  // number of seconds before the URL expires
});

const generateSignedUrlForPut = (bucket, id, expires) => s3.getSignedUrlPromise('putObject', {
  Bucket: bucket,
  Key: id,
  Expires: expires, // number of seconds before the URL expires
  ServerSideEncryption: 'aws:kms'
});

const putPublicKeyInS3 = async (bucket, key, keyPromise) => { 
  const { publicKey } = await keyPromise;
  return s3.putObject({
    Bucket: bucket,
    Key: key,
    Body: publicKey,
    ServerSideEncryption: 'aws:kms'
  }).promise();
};


const putSecretsInParameterStore = async (parameter_store, id, keysPromise, password, expires) => {
  const { privateKey } = await keysPromise;
  let expirationTime = new Date();
  expirationTime.setSeconds(expirationTime.getSeconds() + expires)
  return ssm.putParameter({
    Description: 'Generated by the upload-requests function.',
    Type: 'SecureString',
    Name: `/${parameter_store}/${id}`,
    Value: JSON.stringify({
      privateKey,
      password
    }),
    Tier: 'Advanced',
    Policies: JSON.stringify([
      {
        Type: 'Expiration',
        Version: '1.0',
        Attributes: {
          Timestamp: expirationTime.toISOString()
        }
      }
    ]),
    Tags: [
      {
        Key: 'ApplicationId',
        Value: parameter_store
      }
    ]
  }).promise();
};
exports.handler = async function (event, context) {
  return new Promise(async (resolve) => {

    const {
      headers: { clientId = '123456' } = {}
    } = event;

    const { awsRequestId: correlationId } = context;
    const {
      url_expires = 3600,
      expires = +url_expires,
      new_loans_bucket,
      parameter_store
    } = process.env;
    const password = generatePassword();
    const id = generateId(correlationId, clientId);
    const publicKeyPath = buildPublicKeyPath(id);
    const uploadUrlPromise = generateSignedUrlForPut(new_loans_bucket, `${id}/${correlationId}.gpg`, expires);
    const keysPromise = generatePgpKeys(password);
    const keyUrlPromise = putPublicKeyInS3(new_loans_bucket, publicKeyPath, keysPromise).then(() => {
      return generateSignedUrlForGet(new_loans_bucket, publicKeyPath, expires);
    });
    const putSecretInStorePromise = putSecretsInParameterStore(parameter_store, id, keysPromise, password, expires);

    return Promise.all([uploadUrlPromise, keyUrlPromise, putSecretInStorePromise])
      .then(values => {
        const result = {
          uploadUrl: values[0],
          keyUrl: values[1]
        };
        return resolve(new LendingResponse(null, result));
      })
      .catch(e => {
        return resolve(new LendingResponse(sanitizeError(e), null));
      });
  });
}
})();

var __webpack_export_target__ = exports;
for(var i in __webpack_exports__) __webpack_export_target__[i] = __webpack_exports__[i];
if(__webpack_exports__.__esModule) Object.defineProperty(__webpack_export_target__, "__esModule", { value: true });
/******/ })()
;
//# sourceMappingURL=main.js.map