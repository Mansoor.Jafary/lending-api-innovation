// const { getSecrets } = require('secrets-manager');

const requestBuilder = async ({ accountNumber }) => {
    // const secrets = await getSecrets();
    // const {userId, password} = secrets;
    // const requestBody = '{"query": "query { hello }"}';

    // const requestBody = { query: `query testQuery { hello }` };
    const requestBody = {query: `query { productAccountByAccountNumber(input: {accountNumber: \"9333012227000183\"}) { ... on LineOfCreditAccount { financialPosition { balancesPosition { metrics } } } } }`};
    const config = {};
    config.url = 'https://uol3xwbl5zgytg4h5hsuphwhx4.appsync-api.us-east-1.amazonaws.com/graphql';
    config.data = requestBody;
    config.headers = { 'x-api-key': 'da2-to7bjyxrgnd7nmjpazx7njo74e' };
    console.log('readBalanceInquiry :: Request Builder Config ::', config);
    return config;
};

module.exports = requestBuilder;