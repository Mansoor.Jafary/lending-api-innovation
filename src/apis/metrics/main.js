const requestBuilder = require('./builder/requestBuilder');
// const responseBuilder = require('./builder/responseBuilder');
const { makeAPICall }  = require('api-caller');

exports.handler =  async function(event, context) {
    const startTime = new Date();
    console.log('Metrics :: Event ::', event);
    const config = await requestBuilder(event);
    const endTimeRequestBuilder = new Date();
    const requestBuilderDuration = endTimeRequestBuilder - startTime;
    console.log('Metrics :: RequestBuilder Duration ::', requestBuilderDuration);
    console.log('Metrics :: Request Config ::', config);
    return new Promise((resolve, reject) => {
        makeAPICall(config, context)
        .then((result => {
            if(!result.error) {
                const endTimeMakeAPICall = new Date();
                const endTimeMakeAPICallDuration = endTimeMakeAPICall - startTime;
                console.log('Metrics :: Make API Call Duration ::', endTimeMakeAPICallDuration);
                console.log("Metrics :: Make API Call Result ::", result.data);
                // const transformed = responseBuilder(result.data.balanceInquiryAdvanceResponse);
                // const endTimeTransformResponse = new Date();
                // const transformResponseDuration = endTimeTransformResponse - startTime;
                // console.log('Metrics :: Response Transformed Duration ::', transformResponseDuration);
                // result.data = transformed;
                // console.log('Metrics :: Final Response with Data ::', result);
                resolve(result);                
            }
            console.log('Metrics :: Final Response with Error ::', result);
            resolve(result);
        }))
        .catch(e => {
            console.log('Metrics :: Make API Call Error ::', e);
            reject(e);
        });
    });
};