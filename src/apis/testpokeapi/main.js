const https = require('https');

exports.handler =  async function(event, context) {
  console.log("EVENT: \n" + JSON.stringify(event, null, 2));
  
  let dataString = '';
  
  const response = await new Promise((resolve, reject) => {
  
    const req = https.get("https://pokeapi.co/api/v2/pokemon/ditto", function(res) {
      
      res.on('data', chunk => {
        dataString += chunk;
      });
      
      res.on('end', () => {
        resolve({
          statusCode: 200,
          body: JSON.stringify(JSON.parse(dataString), null, 4)
        });
      });
    });
    
    req.on('error', (e) => {
      console.log(e);
      reject({
        statusCode: 500,
        body: 'Something went wrong!'
      });
    });

  });
  
  console.log('Result:', response);
  return response;
}
