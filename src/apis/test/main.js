const axios = require('axios');
exports.handler = async function (event, context) {
  console.log("EVENT: \n" + JSON.stringify(event, null, 2));

  const requestBody = {
    balanceInquiryAdvance: {
      acquirer: {
        id: 'AllyBank',
        userId: 'AllyBankTest',
        password: 'TempPass?123'
      },
      card: {
        number: '9333012227000183'
      }
    }
  };


  const config = {};
  config.url = JSON.parse(process.env.URIS).i2C_TEST_API;
  config.data = requestBody;
  // config.timeout = 10000;
  config.proxy = false;
  config.method = 'POST';
  config.headers = {'Content-Type': 'application/json'};


  return new Promise((resolve, reject) => {
    try {
      axios(config)
        .then(result => {
          console.log("Make API Call :: Success :: ", result);
          resolve(result.data);
        });
    } catch (e) {
      console.log("Make API Call :: Error Attempting API Call :: ", e);
      reject(e);
    }
  });
}