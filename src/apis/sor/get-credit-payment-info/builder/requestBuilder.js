const requestBuilder = async (
        { accountID },
        { userId, password }
    ) => {
    const requestBody = {
        getCreditPaymentInfo: {
            acquirer: {
                id: "AllyBank",
                userId: userId,
                password: password
            },
            card: {
                number: accountID
            }
        }
    };
    
    const getCreditPaymentInfoRequest = {};
    getCreditPaymentInfoRequest.url = JSON.parse(process.env.URIS).getCreditPaymentInfoI2CURL;
    getCreditPaymentInfoRequest.data = requestBody;
    console.log('getCreditPaymentInfo :: Request Builder Options ::', getCreditPaymentInfoRequest);
    return getCreditPaymentInfoRequest;
};

module.exports = requestBuilder;