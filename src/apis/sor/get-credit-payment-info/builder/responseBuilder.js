const _ = require('lodash');

function handleZeroBalance(amount) {
    if(amount !== 0) {
        return `${amount}`
    }
    return '0.00';
}

// const responseBuilder = (response) => {
//     const readPaymentsPositionResponse = 
//     {
//         nextPaymentDate: _.get(response, 'dueDate', null),
//         nextPaymentDue: {
//             amount: handleZeroBalance(_.get(response, 'minDueAmount', null)),
//             currency: null
//         },
//         lastPaymentReceivedDate: _.get(response, 'lastPaymentReceiveDate', null),
//         lastPaymentReceived: {
//             amount: handleZeroBalance(_.get(response, 'lastPaymentReceivedAmount', null)),
//             currency: null
//         }
//     }
//     return readPaymentsPositionResponse;
// }

const responseBuilder = (response) => {
    const readPaymentsPositionResponse = 
    {
        nextPaymentDate: '2021-11-15',
        nextPaymentDue: {
            amount: '102.00',
            currency: 'USD'
        },
        lastPaymentReceivedDate: '2021-10-15',
        lastPaymentReceived: {
            amount: '514.00',
            currency: 'USD'
        }
    }
    return readPaymentsPositionResponse;
}

module.exports = responseBuilder;