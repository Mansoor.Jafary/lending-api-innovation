const readPaymentsPositionMockedResponse = {
    data: {
        nextPaymentDate: '2021-11-15',
        nextPaymentDue: {
            amount: '102.00',
            currency: 'USD'
        },
        lastPaymentReceivedDate: '2021-10-15',
        lastPaymentReceived: {
            amount: '514.00',
            currency: 'USD'
        }
    }
}

module.exports = readPaymentsPositionMockedResponse;