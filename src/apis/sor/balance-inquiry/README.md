# How to Test Locally  
1. Ensure you have the following pre-requisites  
    1. SAM-CLI:  
        - Recommend to install this globally. See install Instructions here -> https://aws.amazon.com/serverless/sam/  
    2. Docker Desktop  
        - Tip: Ensure proxy is setup in Preferences -> Resources -> Proxies in Docker Desktop application when behind corporate proxy  
    3. Ensure local mocks are running  
        - Running `npm start` from the root folder initializes dependent mock
    4. Ensure following files are created in function's root folder  
        - template.yaml // this is the sam test configuration  
        - event.json // file containing mock event to be passed to the lambda function  
        - env.json // file containing environment variables for sam-cli local setup  

2. Invoke Lambda Function Locally using SAM-CLI  
    1. (Optional) Run sam build
        `sam build -t ./sam-local/template.yaml`
    2. Invoke the lambda function using sam invoke  
    `sam local invoke -e ./sam-local/event.json`
    OR (if you skipped sam build)
    `sam local invoke -t ./sam-local/template.yaml -e ./sam-local/event.json`

