const { getRestAPICaller }  = require('../../../common-utils/api-caller/index');
const { getSecrets } = require('../../../common-utils/secrets-manager/index');
const { getAppConfigData } = require('../../../common-utils/common-initializers/index');

const LendingResponse = require('../../../common-utils/lending-response/index');
const { sanitizeError } = require('../../../common-utils/lending-errors/index');

const requestBuilder = require('./builder/balanceInquiryAdvanceRequestBuilder');
const responseBuilder = require('./builder/responseBuilder');
const mockedResponse = require('./_mocks/index');

exports.handler =  async function(event, context) {
    const startTime = new Date();
    console.log('readBalanceInquiry :: Event ::', event);
    
    const appConfigData = await getAppConfigData();
    console.log('readBalanceInquiry :: AppConfig Data ::', appConfigData);

    const endTimeAppCongigResponse = new Date();
    const appConfigResponseDuration = endTimeAppCongigResponse - startTime;
    console.log('readBalanceInquiry :: AppConfig Init Duration ::', appConfigResponseDuration);
    
    // todo: remove from here and as a nested function in api caller call
    const { czl74bBalInquiry: { mockResponse }} = appConfigData;
    console.log('readBalanceInquiry :: AppConfig mockResponse ::', mockResponse);
    if (mockResponse === 'TRUE'){
        console.log('readBalanceInquiry :: Entering AppConfig mockResponse TRUE');
        return new Promise ((resolve, reject) => {
            return resolve(mockedResponse);
        });
    }

    const secrets  = await getSecrets();
    console.debug('readBalancesSnapshot :: secrets ::', secrets);
    const endTimeSecretsResponse = new Date();
    const secretsResponseDuration = endTimeSecretsResponse - startTime;
    console.log('readBalancesSnapshot :: Secrets Duration ::', secretsResponseDuration);

    const request = await requestBuilder(event, secrets);
    const endTimeRequestBuilder = new Date();
    const requestBuilderDuration = endTimeRequestBuilder - startTime;
    console.log('readBalanceInquiry :: RequestBuilder Duration ::', requestBuilderDuration);
    console.log('readBalanceInquiry :: Request Options ::', request);
    return new Promise((resolve, reject) => {
        const callRestAPI = getRestAPICaller(context);
        callRestAPI(request)
        .then(result => {
            if(!(result.error instanceof Error)) {
                const endTimeMakeAPICall = new Date();
                const endTimeMakeAPICallDuration = endTimeMakeAPICall - startTime;
                console.log('readBalanceInquiry :: Make API Call Duration ::', endTimeMakeAPICallDuration);
                console.log("readBalanceInquiry :: Balance Inquiry Call ::", result.data);
                const transformed = responseBuilder(result.data.balanceInquiryAdvanceResponse, event);
                const endTimeTransformResponse = new Date();
                const transformResponseDuration = endTimeTransformResponse - startTime;
                console.log('readBalanceInquiry :: Response Transformed Duration ::', transformResponseDuration);
                console.log('readBalanceInquiry :: Transformed Response ::', transformed);
                return resolve(new LendingResponse(null, transformed));
            }
            console.log('readBalanceInquiry :: Final Response with Error ::', result);
            return resolve(new LendingResponse(sanitizeError(result.error), null));
        })
        .catch(e => {
            console.log('readBalanceInquiry :: Make API Call Error ::', e);
            return resolve(new LendingResponse(sanitizeError(e), null));
        });
    });
};