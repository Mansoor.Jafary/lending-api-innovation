// const balanceInquiryAdvanceMockResponse = {
//     data: {
//         currentBalance: {
//             amount: '99.99',
//             currency: 'USD'
//         },
//         allocatedCreditLimit: {
//             amount: '10000.00',
//             currency: 'USD'
//         },
//         availableCreditLimit: {
//             amount: '5002.00',
//             currency: 'USD'
//         }
//     }
// }

const balanceInquiryAdvanceMockResponse = {
    data: [
        {
            accountID: 'X001',
            currentBalance: {
                amount: '199.99',
                currency: 'USD'
            },
            allocatedCreditLimit: {
                amount: '10000.00',
                currency: 'USD'
            },
            availableCreditLimit: {
                amount: '5002.00',
                currency: 'USD'
            }
        },
        {
            accountID: 'X002',
            currentBalance: {
                amount: '299.99',
                currency: 'USD'
            },
            allocatedCreditLimit: {
                amount: '10000.00',
                currency: 'USD'
            },
            availableCreditLimit: {
                amount: '5002.00',
                currency: 'USD'
            }
        },
        {
            accountID: 'X003',
            currentBalance: {
                amount: '399.99',
                currency: 'USD'
            },
            allocatedCreditLimit: {
                amount: '10000.00',
                currency: 'USD'
            },
            availableCreditLimit: {
                amount: '5002.00',
                currency: 'USD'
            }
        },
    ]
};

module.exports = balanceInquiryAdvanceMockResponse;