const _ = require('lodash');

function handleZeroBalance(amount) {
    if(amount !== 0) {
        return `${amount}`
    }
    return '0.00';
}
const responseBuilder = (response) => {
    const balanceInquiryAdvanceResponse = 
    {
        currentBalance: {
            amount: handleZeroBalance(_.get(response, 'balance', null)),
            currency: 'USD'
        },
        allocatedCreditLimit: {
            amount: '10000.00',
            currency: 'USD'
        },
        availableCreditLimit: {
            amount: '5002.00',
            currency: 'USD'
        },
    }
    return balanceInquiryAdvanceResponse
}

module.exports = responseBuilder;