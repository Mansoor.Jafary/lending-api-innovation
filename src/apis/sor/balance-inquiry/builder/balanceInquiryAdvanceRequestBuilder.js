const requestBuilder = async ({ accountID }, 
    { userId, password }) => {
    
    const requestBody = {
        balanceInquiryAdvance: {
            acquirer: {
                id: "AllyBank",
                userId: userId,
                password: password
            },
            card: {
                number: accountID
            }
        }
    };

    
    const balanceInquiryAdvanceRequest = {};
    // const urlString = process.env.URIS;
    // console.log("Printing Debug:",urlString);
    // console.log("Printin Debut2:", urlString.balanceInquiryAdvanceI2CURL);
    balanceInquiryAdvanceRequest.url = JSON.parse(process.env.URIS).balanceInquiryAdvanceI2CURL;
    balanceInquiryAdvanceRequest.data = requestBody;
    // balanceInquiryAdvanceRequest.timeout = 2000;
    console.log('readBalanceInquiry :: Request Builder Options ::', balanceInquiryAdvanceRequest);
    return balanceInquiryAdvanceRequest;
};

module.exports = requestBuilder;