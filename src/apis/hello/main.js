const AWS = require('aws-sdk');
// const { getSecrets } = require('secrets-manager');
// const { getAppConfigData, getAppConfigDataAlternate } = require('common-initializers');
// const http = require('http');
var ssmClient = new AWS.SSM();

// let i = 1;

exports.handler =  async function(event, context) {
  // console.log("EVENT: \n" + JSON.stringify(event, null, 2));
  // Parameter Store Invocation example
  const { ENVIRONMENT } = process.env
  const paramStorePathConfig =  {
    Path: `/${ENVIRONMENT}/105082/${ENVIRONMENT}/lapi/mocks/`,
    Recursive: true,
    WithDecryption: false
   };
   const params = await ssmClient.getParametersByPath(paramStorePathConfig, (err, data) => {
     if(err) {
       console.log("Error while accessing Parameter Store")
     }
   }).promise();
   // const param_vals = await servmgr.getParameter({ Name: '/default/105082/default/lapi/mocks/api-gateway-host', WithDecryption: true }).promise();
   console.log(params.Parameters);
   const [ mockApiGatewayHost ] = params.Parameters.filter(param => param.Name === `${paramStorePathConfig.Path}api-gateway-host`);
   if (! mockApiGatewayHost.Value) console.log('Error fetching params')
   console.log("LAPI Mocks API Gateway Host Is:", mockApiGatewayHost.Value);
   // i = i + 1;
   // console.log('Value of i: ', i);

  
  // const appConfigDataStartTime = new Date();
  // const appConfigData = await getAppConfigData();
  // console.log('appConfigData:',appConfigData);
  // const appConfigDataEndTime = new Date();
  // const durationAppConfigData = appConfigDataEndTime - appConfigDataStartTime;
  // console.log('durationAppConfigData:', durationAppConfigData);


  // const getAppConfigDataAlternateStartTime = new Date();
  // const appConfigDataAlternate = await getAppConfigDataAlternate();
  // console.log('getAppConfigDataAlternate:',appConfigDataAlternate);
  // const getAppConfigDataAlternateEndTime = new Date();
  // const durationAppConfigDataAlternate = getAppConfigDataAlternateEndTime - getAppConfigDataAlternateStartTime;
  // console.log('durationAppConfigDataAlternate:', durationAppConfigDataAlternate);


  // const {AWS_APPCONFIG_EXTENSION_HTTP_PORT, APPLICATION_NAME, ENVIRONMENT_NAME, CONFIGURATION_NAME } = process.env;
  
  // const res = await new Promise((resolve, reject) => {
  //   http.get(
  //     `http://localhost:${AWS_APPCONFIG_EXTENSION_HTTP_PORT}/applications/${APPLICATION_NAME}/environments/${ENVIRONMENT_NAME}/configurations/${CONFIGURATION_NAME}`, 
  //     resolve
  //   );
  // });

  // let configData = await new Promise((resolve, reject) => {
  //   let data = '';
  //   res.on('data', chunk => data += chunk);
  //   res.on('error', err => reject(err));
  //   res.on('end', () => resolve(data));
  // });

  // const parsedConfigData = JSON.parse(configData);


  // console.log(parsedConfigData);

  // const SECRET_NAME = process.env.SECRET_NAME;
  // console.log (SECRET_NAME);
  // const secrets = await getSecrets();
  // console.log (secrets);
  // if(secrets.userId){
  //   console.log(secrets.userId);
  // }

  // if(secrets.password){
  //   console.log(secrets.password);
  // }
  

  return "Hello from Lambda Version 2!";
}
