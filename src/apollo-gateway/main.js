"use strict";

process.env['APOLLO_GRAPH_REF'] = 'Lending-API-siyezg@current';
process.env['APOLLO_SCHEMA_REPORTING'] = true;
process.env['APOLLO_KEY'] = ["service:Lending-API-siyezg:e5wIsGLv1cioXSONTk4_EQ"];


// const { ApolloServer } = require('apollo-server');
// const { APIGatewayProxyHandlerV2 } = require('aws-lambda');
const { ApolloServer } = require('apollo-server-lambda');
const { ApolloGateway } = require('@apollo/gateway');
const { readFileSync } = require('fs');
const supergraphSdl = readFileSync('./supergraph.graphql').toString();

const port = 4000;

const gateway = new ApolloGateway({
  supergraphSdl
});
const server = new ApolloServer({
  gateway,
  subscriptions: false
});

// server.listen({ port }).then(({ url }) => {
//     console.log(`Gateway Server ready at ${ url }`)
// });

exports.handler = server.createHandler();

// export const handler: APIGatewayProxyHandlerV2 = async (event, context) => {
//     const result = await wrapper(event, context)
//     return result
//   }

// const wrapper = server.createHandler();

// exports.handler = () => APIGatewayProxyHandlerV2 = {
//   // return wrapper(event, context);
//   return server.createHandler();
// });