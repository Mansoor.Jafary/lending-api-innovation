union _Entity = LineOfCreditAccount

input _Any {
    allyCustomerID: ID!
    accountID: ID!
}

type _Service {
  sdl: String
}

"Read operations supported by the GraphQL API"
type Query @extends {
    _service: _Service!
    _entities(representations: [_Any!]!): [_Entity]!
    "Access the product account using its account identifier"
    accountByAccountID(input: AccountByAccountIDInput!): ProductAccount
    "Temporary field for testing only"
    hello: String
    fineGrainAuthFail: String
    helloPipeline: String
}

type SecureFileUploadURL {
    "Use this URL to send a PUT request for uploading file. URL expires after 15 minutes"
    uploadUrl: String
    "Use this URL to download the public key that should be used to encrypt the file before it is uploaded"
    keyUrl: String
}

type Mutation {
    "Creates a URL and Key required for securely uploading a new Installment Loans file to LAPI"
    createNewInstallmentLoansUploadURL: SecureFileUploadURL
    createPayeeAccount(input: CreatePayeeAccountInput!): CreatePayeeAccountResponse
    deletePayeeAccount(input: DeletePayeeAccountInput!): DeletePayeeAccountResponse
    createPayment(input: CreatePaymentInput!): CreatePaymentResponse
    createBillingDateChangeRequest(input: CreateBillingDateChangeRequestInput!): CreateBillingDateChangeRequestResponse
    updatePaperlessSettings(input: UpdatePaperlessSettingsInput!): UpdatePaperlessSettingsResponse
    createCase(input: CreateCaseInput!): CreateCaseResponse
    createCreditLineIncreaseRequest(input: CreateCreditLineIncreaseRequestInput!): CreateCreditLineIncreaseRequestResponse 
}

"An account created for the Product"
interface ProductAccount @key(fields: "accountID") {
    "The unique identifier that serves as the proxy to locate the account"
    accountID: ID!

    "The date when the account got instantiated"
    openDate: String

    "The product associated with the Product Account"
    product: Product
    
    "One of the enumeration of roles that is associated to a Product Account"
    role: RoleTypeEnum
}

"Input arguments for identifying the product account"
input AccountByAccountIDInput {
    "The unique identifier that serves as the proxy to locate the account"
    accountID: ID!
}

"An product offering of Ally"
type Product {
    "Defines the type of the Product"
    type: ProductTypeEnum

    "Identifies the class of the Product"
    class: ProductClassEnum
    
    "Name of the product as advertized to the customer"
    name: String
}

enum ProductClassEnum {
    LOAN
    CARD
}

enum RoleTypeEnum {
    ACCOUNT_HOLDER
}

"Enumeration of Ally product offerings"
enum ProductTypeEnum {
    LINE_OF_CREDIT
    INSTALLMENT_LOAN
}

"A Line of Credit account"
type LineOfCreditAccount implements ProductAccount @key(fields: "accountID") {
    "The unique identifier that serves as the proxy to locate the account" 
    accountID: ID!

    "The date when the account got instantiated"
    openDate: String

    "The product associated with the Product Account"
    product: Product

    role: RoleTypeEnum

    "The financial state of a Product Account for a given Billing Period"
    financialPosition: LineOfCreditAccountFinancialPosition
    payees: [PayeeAccount]
    billingPeriods(input: BillingPeriodsInput): [BillingPeriod]
    cases: [Case]
}

"The overall financial position of the product account"
type LineOfCreditAccountFinancialPosition {
    "Balance Summaries for the Line of Credit Account"
    balancesPosition: BalancesPosition

    "Payment Summaries for the Line of Credit Account"
    paymentsPosition: RecentPaymentsAndDues
}

"Balance Summaries for the Line of Credit Account"
type BalancesPosition {
    "The portion of the line of that is unpaid, not including deferred balance, accrued interest, fees or other charges"
    currentBalance: Money

    "The total credit limit of the account including amounts used"
    allocatedCreditLimit: Money

    "The portion of the Line of credit account that is unused and available"
    availableCreditLimit: Money

    "Scanner for Metrics"
    metrics: String
}

"Payments Summaries for the Line of Credit Account"
type RecentPaymentsAndDues {
    "Date by which a payment must be made to avoid additional charges"
    nextPaymentDate: String

    "Amount in which a payment must be made to keep account in good standing"
    nextPaymentDue: Money

    "Date when most recent payment was received"
    lastPaymentReceivedDate: String

    "Money recent payment received"
    lastPaymentReceived: Money
}

"A monetary instrument of value acceptable to settle a transaction"
type Money {
    "The value of a monetary instrument"
    amount: String
    
    "The currency associated to the value of a monetary instrument"
    currency: String
}

type PayeeAccount { foo: String }

input BillingPeriodsInput { foo: String }

type BillingPeriod { 
    from: String
    to: String
    transactions(input: TransactionsInput): [Transaction]
    payments(input: PaymentsInput): [Payment]
}
input TransactionsInput { foo: String }
type Transaction { foo: String }
type Case {
    type: CaseTypeEnum
}
enum CaseTypeEnum {
    DISPUTE
    FRAUD
}
input PaymentsInput { foo: String }
type Payment { source: PaymentSource }
type PaymentSource { foo: String }

input CreatePayeeAccountInput { foo: ID! }
type CreatePayeeAccountResponse { foo: String }
input DeletePayeeAccountInput { foo: ID! }
type DeletePayeeAccountResponse { foo: String }
input CreatePaymentInput { foo: String }
type CreatePaymentResponse { foo: String }
input CreateBillingDateChangeRequestInput { foo: String }
type CreateBillingDateChangeRequestResponse { foo: String }
input UpdatePaperlessSettingsInput { foo: String }
type UpdatePaperlessSettingsResponse { foo: String }
input CreateCaseInput { foo: String }
type CreateCaseResponse { foo: String }
input CreateCreditLineIncreaseRequestInput { foo: String }
type CreateCreditLineIncreaseRequestResponse { foo: String }