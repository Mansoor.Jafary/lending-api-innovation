// const { authPolicyTemplate, authPolicyVars, createAuthPolicy } = require('./authPolicyBuilder');
// const { createAuthPolicy } = require('./authPolicyBuilder');

exports.handler =  async function(event, context) {
    console.log("EVENT: \n" + JSON.stringify(event, null, 2))
    console.log("CONTEXT: \n" + JSON.stringify(context, null, 2))

    const { multiValueHeaders: { Authorization: [ authorizationToken ] } } = event;
    
    // 1. Todo: Read JWT Token from event header
    // 2. Validate JWT Token Identity - performed by CIAM?
    // 3. Decrypt Payload to read claims
    // 4. Set Auth Policy at runtime allowing the access using claims in the JWT Token // these claims need be set.

    // authPolicyVars.arn = 'arn:aws:execute-api:us-east-1:650487181597:uuq9s1kuec/*/*' // this needs to be passed dynamically, todo: how to read and save module arn to pass here
    // authPolicyVars.action = 'execute-api:Invoke';
    // if(authorizationToken === '1234'){
    //   authPolicyVars.decision = 'Allow';      
    // } else {
    //   authPolicyVars.decision = 'Deny';
    // }
    

    let effect = 'Deny';
    
    if(authorizationToken === '1234') {
      effect = 'Allow';      
    }

    const policyDoc = {
      "principalId": "abc123",
      "policyDocument": {
        "Version": "2012-10-17",
        "Statement": [
          {
            "Effect": effect,
            "Action": [
              "execute-api:Invoke"
            ],
            "Resource": [
              "arn:aws:execute-api:us-east-1:650487181597:mjujiyilnj/*/*"
            ]
          }
        ]
      }
    };
    return policyDoc;
}