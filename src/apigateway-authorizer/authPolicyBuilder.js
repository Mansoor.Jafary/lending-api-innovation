// const authPolicyTemplate = "{ \"Version\": \"2012-10-17\", \"Statement\": [{\"Effect\": \"${this.effect}\",\"Action\": [\"${this.action}\"],\"Resource\": [\"${this.arn}\"]}]}";

// const authPolicyVars = {
//     effect: "DENY",
//     arn: '',
//     action: ''
// }

const authPolicyTemplate = {
	"principalId": "abc123",
	"policyDocument": {
        Version: "2012-10-17",
        Statement:[]
    }
};


const createAuthPolicy = function(resource, action, effect){
    // return new Function("return `"+authPolicyTemplate +"`;").call(authPolicyVars);

    const actionVal = [];
    actionVal.push(action);
    const resourceVal = [];
    resourceVal.push(resource);
    const statement = [];
    statement.push({
        Effect: effect,
        Action: actionVal,
        Resource: resourceVal
    })

    authPolicyTemplate.policyDocument.Statement = statement;
    
    return authPolicyTemplate;
}

// module.exports = { authPolicyTemplate, authPolicyVars, createAuthPolicy };
module.exports = { createAuthPolicy };