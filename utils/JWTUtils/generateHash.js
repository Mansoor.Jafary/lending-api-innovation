const hash = require('object-hash');
const readline = require("readline");

// const query = "{\n  productAccountByAccountNumber(input: {accountNumber: \"9333012227000183\"}) {\n    ... on LineOfCreditAccount {\n      openDate\n      financialPosition {\n        balancesPosition {\n          currentBalance {\n            amount\n            currency\n          }\n        }\n        paymentsPosition {\n          lastPaymentReceived {\n            amount\n            currency\n          }\n          nextPaymentDue {\n            amount\n            currency\n          }\n          lastPaymentReceivedDate\n          nextPaymentDate\n        }\n      }\n    }\n  }\n}\n";

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question("Enter String: ", function(query) {
    const hashed = hash(query);
    console.log('Hash Value:', hashed);
    rl.close();
});

rl.on("close", function() {
    process.exit(0);
});


