
# Description

Proof of concept for Lending API Platform which uses the AWS AppSync GraphQL API with Terraform.

# Architecture

Apigee -> GraphQL (AppSync) --> Business Function (Lambda) --> SOR Function (Lambda) --> i2c (SOR)  
Includes: Secrets Manager for i2c API secrets  
Bonus: AWS API Gateway with paths to AppSync - This pattern will not be used however demonstrated  

# Terraform-Implementation

API Gateway and Lambda modules have been instantiated using the starter project.  
AppSync has been instantiated and configured from available Terraform public libraries.  

# Token Generation
A custom graphql token can be generated using utils/JWTUtils/generateJWTToken.js  
`node generateJWTToken.js`

# Query Whitelisting
Hashed values of whitelisted queries can be added to src/appsync-authorizer/queries.js with scopes.  
Generate a hash of a query using /Users/czl74b/dev-js/lending-api-innovation/utils/JWTUtils/generateHash.js    
Here is an example of how to generate a query.  
```
USACCMNBSTEMD6R:JWTUtils czl74b$ node generateHash.js
Enter String: {\n  productAccountByAccountNumber(input: {accountNumber: \"9333012227000183\"}) {\n    ... on LineOfCreditAccount {\n      openDate\n      financialPosition {\n        balancesPosition {\n          currentBalance {\n            amount\n            currency\n          }\n        }\n        paymentsPosition {\n          lastPaymentReceived {\n            amount\n            currency\n          }\n          nextPaymentDue {\n            amount\n            currency\n          }\n          lastPaymentReceivedDate\n          nextPaymentDate\n        }\n      }\n    }\n  }\n}\n
Hash Value: 1060fea9fa033a02b3384c6e8e9141aacc9f7999
```

# How to create NodeJS deployment packages for Common Node Modules
From the root folder of your function create a zip file. Required structure in the zip file is as follows:  
/nodejs/node_modules/<module-name>/<module-files>  
```
zip -r api-caller_v1.1.1.zip ./
```

Note: This is used for creating lambda layers. This is not needed when using lambda modules.

## What's included

`Jenkinsfile/.gitlab-ci.yml` - A minimal pipeline configuration to deploy to your accounts using the standard PCE Terraform pipeline

`.gitignore` - The Ally default .gitignore with additions for Terraform file types

`backend.tf` - The backend configuration. This tells Terraform where to find and store your projects state. The state is used by Terraform to map real world resources to your configuration and keep track of metadata. State is shared per project and stored in S3.

`providers.tf` - Configures your providers. In this case, sets the AWS region

`versions.tf ` - The versions of terraform and providers required to use this project

`variables.tf` - Variables are declared here. You can set their required types, default values, and descriptions. The default required variables are included.

`terraform.tfvars` - Variable values are defined here. Any variables from `variables.tf` can be set or overriden here

`main.tf ` - The primary file for defining terraform resources. This is where you will include the data and resources to create infrastructure. As your project grows you may reorganize into multiple `.tf` files or modules (folders)

## How to use

Ensure your local terraform environment is set up ([See instructions here](https://confluence.int.ally.com/display/SE/4%29+Developer+-+Onboarding)).

Add your infrustructure resources to `main.tf` then from within the `.aws/terraform` folder in a terminal run `terraform init` to download project dependencies (Make sure your terminal is logged in to AWS). Run `terraform fmt` to fix indentation and `terraform validate` to validate. Commit and push to Bitbucket/Gitlab then run a build of your branch to deploy to AWS.

Ally Terraform Modules: [https://bitbucket.int.ally.com/projects/TF](https://bitbucket.int.ally.com/projects/TF)
The Ally Terraform modules project contains modules for common AWS services and patterns such as an S3 bucket or Lambda function. You should always use modules as a starting point instead of building all the resources yourself. Modules provide baseline configurations and comply with IPRM requirements. If no module is available for a service you want to use, consult the cloud team as the service may not yet be approved for production usage.

AWS Provider Documentation: [https://registry.terraform.io/providers/hashicorp/aws](https://registry.terraform.io/providers/hashicorp/aws)
Ensure you are looking at documentation for the version of the AWS provider you're using. (Specified in the `versions.tf` file)
README
