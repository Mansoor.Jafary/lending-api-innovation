const aws = require('aws-sdk');
const path = require('path');
const fs = require('fs');
const proxy = require('proxy-agent');
const jsonata = require('jsonata');

const terraformOutputPath = path.resolve(__dirname, '../../.aws/terraform/terraform-output.json');
const lambdaVersionsLimitFeatureBranch = process.env.LAMBDA_VERSIONS_LIMIT_FEATURE_BRANCH || 2;
const lambdaVersionsLimitDefault = process.env.LAMBDA_VERSIONS_LIMIT || 10;

const {
    APPLICATION_ID,
    AWS_DEFAULT_PROFILE,
    AWS_REGION,
    BRANCH,
    INNOVATION_ACCOUNT_NUMBER,
    IS_PROD,
    NON_PROD_ACCOUNT_NUMBER,
    PROD_ACCOUNT_NUMBER,
    SHORT_BRANCH_NAME,
    HTTP_PROXY,
    IS_OFFLINE,
    DARK_ALIAS_NAME = 'dark',
    PROD_ALIAS_NAME = 'prod'
} = process.env;
const exit = process.exit;

// Not needed for Gitlab Pipeline, as it uses env variables for auth
// // AWS credentials
// const httpOptions = HTTP_PROXY ? { agent: proxy(HTTP_PROXY) } : {};
// const credentials = new aws.SharedIniFileCredentials({ profile: AWS_DEFAULT_PROFILE || 'default' });
// const awsParams =
//     IS_OFFLINE === 'true'
//         ? { region: AWS_REGION, credentials, httpOptions }
//         : { region: AWS_REGION };

// aws.config.update(awsParams);

/**
 * Return data from Terraform state file located in S3.
 * @param {String} env environment of current work
 * @returns Object
 */
const getDataFromTFState = async (env) => {
    const s3 = new aws.S3();
    const workspace = getWorkspace(env);
    const accountNumber = getAWSAccountNumber();
    const bucket = `ally-${AWS_REGION}-${accountNumber}-tf-states`;
    const key = `env:/${workspace}/${APPLICATION_ID}-journey/terraform.tfstate`;
    const params = { Bucket: bucket, Key: key };
    let fileObject;

    console.log('Loading data from TFSTATE...');
    console.log(`bucket: ${bucket}`);
    console.log(`key: ${key}`);

    try {
        await new Promise((resolve, reject) =>
            s3.getObject(params, (err, result) => (err ? reject(err) : resolve(result))),
        )
            .then((result) => {
                console.log(' Successfully read from S3 ');
                fileObject = result;
            })
            .catch((error) => {
                console.log(' Failed to read from S3 ', JSON.stringify(error));
                exit(10);
            });

        return JSON.parse(fileObject.Body.toString('utf-8'));
    } catch (error) {
        console.log('Error in reading state file', error);
        exit(10);
    }
};

/**
 * Return AWS Account number
 * @returns String
 */
const getAWSAccountNumber = () => {
    const findInBranch = (item) => {
        if (BRANCH.indexOf(item) !== -1) {
            return true;
        }
        return false;
    };

    if (IS_PROD === 'false' && (BRANCH === 'develop' || ['feature/', 'PR-'].some(findInBranch))) {
        return INNOVATION_ACCOUNT_NUMBER;
    } else if (
        IS_PROD === 'false' &&
        (BRANCH === 'master' || BRANCH === 'main' || ['release/', 'hotfix/', 'support/'].some(findInBranch))
    ) {
        return NON_PROD_ACCOUNT_NUMBER;
    } else if (IS_PROD === 'true' && (BRANCH === 'master' || BRANCH === 'main')) {
        return PROD_ACCOUNT_NUMBER;
    }
    return '';
};

/**
 * Returns the Terraform workspace name derived from environment.
 * @param {String} env environment name from terraform-output.json
 * @returns String
 */
const getWorkspace = (env) => {
    return ['develop', 'master', 'main'].indexOf(BRANCH) !== -1 ? env : `${env}-${SHORT_BRANCH_NAME}`;
};

/**
 * Returns parsed file
 * @param {String} filepath the absolute path of a file to process
 * @returns Promise<object>
 */
const processFile = async (filepath) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filepath, { encoding: 'utf-8' }, (error, data) => {
            if (error) {
                return reject({
                    file: filepath,
                    error: { message: 'An error occurred parsing JSON' },
                });
            }
            const jsonData = JSON.parse(data);
            return resolve(jsonData);
        });
    });
};

/**
 * Return an array of AWS parameters to update trigger event.
 * @param {Array<string>} queueNames SQS queues to configure
 * @param {Object} tfData Terraform state object
 * @param {bool} isTriggerEnabled flag to enable trigger
 * @returns Array
 */
const getParamsForEventTrigger = async (queueNames, tfData, isTriggerEnabled) => {
    try {
        return jsonata('$.resources[type="aws_lambda_event_source_mapping"].instances.attributes')
            .evaluate(tfData)
            .filter((item) => {
                if (typeof item.event_source_arn !== 'undefined') {
                    const itemSqs =
                        item.event_source_arn.indexOf(':') !== -1
                            ? item.event_source_arn.split(':').pop()
                            : null;
                    if (itemSqs && queueNames.indexOf(itemSqs) !== -1) {
                        return item;
                    }
                }
            })
            .map((item) => {
                return {
                    UUID: item.uuid,
                    FunctionName: item.function_arn,
                    Enabled: isTriggerEnabled,
                };
            });
    } catch (error) {
        console.log(
            `An error occurred retrieving params from Terraform state file. ${JSON.stringify(
                error,
            )}`,
        );
        exit(0);
    }
};

/**
 * Update event trigger in AWS
 * @param {Array<Object>} params objects with UUID, FunctionName and Enabled
 * @param {Function} errorFn error function to apply depending on pausing or resuming
 */
const updateEventTrigger = async (params, errorFn) => {
    const lambda = new aws.Lambda({ region: AWS_REGION });
    await new Promise((resolve, reject) =>
        lambda.updateEventSourceMapping(params, (err, result) =>
            err ? reject(err) : resolve(result),
        ),
    )
        .then((result) => {
            console.log(' Successful. Response: ', JSON.stringify(result));
        })
        .catch((error) => {
            errorFn(error);
        });
};

/**
 *
 * @returns {Promise<void>}
 */
const updateFunctionAliases = async (tfData) => {
    try {
        for (let item of tfData.resources) {
            if (item.type === 'aws_lambda_alias') {
                const functionName = item?.instances[0]?.attributes?.function_name;
                const isDarkAlias = item?.instances[0]?.attributes?.name === DARK_ALIAS_NAME;

                if (functionName && isDarkAlias) {
                    console.log("Start: Update function alias for %s", functionName);

                    const aliasVersions = await getProdDarkAliasVersions(functionName).catch(error => {throw error});

                    if (!aliasVersions.dark || !aliasVersions.prod) continue;

                    if (aliasVersions.dark > aliasVersions.prod) {
                        const updateAliasVersionResponse = await updateFunctionAliasVersion(functionName, PROD_ALIAS_NAME, aliasVersions.dark).catch(error => { throw error });
                        console.log("Updated Function: %s, 'prod' alias version to: %s, Response: %o ", functionName, updateAliasVersionResponse.FunctionVersion, updateAliasVersionResponse);
                    }
                }
            }
        }
    } catch (error) {
        console.log('Error in UpdateFunctionAliases', JSON.stringify(error));
    }
}

/**
 *
 * @param lambdaFunctionName
 * @returns {Promise<{}>}
 */
const getProdDarkAliasVersions = async (lambdaFunctionName) => {
    let promises = [];

    const lambda = new aws.Lambda({
        // credentials: credentials,
        region: AWS_REGION
    });

    for (let i = 0; i < 2; i++) {
        const aliasName = i === 0 ? DARK_ALIAS_NAME : PROD_ALIAS_NAME;
        promises.push( lambda.getAlias({
            FunctionName: lambdaFunctionName,
            Name: aliasName
        }).promise());
    }

    return (await Promise.all(
        promises.map(promise => {
            return promise
                .then(response => {
                    console.log(" Get Function Version Response: ", JSON.stringify(response));
                    return {[`${response.Name}`]: response.FunctionVersion}
                })
                .catch(error => {
                    console.log(" FAILED to get version for function: %s. ERROR Message: %s", lambdaFunctionName, error);
                    process.exit(10);
                });
        }),
    )).reduce(function (acc, cVal) {
        return Object.assign(acc, cVal);
    }, {}) || {};
};

/**
 *
 * @param lambdaFunctionName
 * @param aliasName
 * @param aliasVersion
 * @returns {Promise<{}>}
 */
const updateFunctionAliasVersion = async (lambdaFunctionName, aliasName, aliasVersion) => {
    let lambdaVersionResponse = {};
    var lambda = new aws.Lambda({
        // credentials: credentials,
        region: AWS_REGION
    })

    const params = {
        FunctionName: lambdaFunctionName,
        FunctionVersion: aliasVersion,
        Name: aliasName
    };

    await new Promise((resolve, reject) =>
        lambda.updateAlias(params, (err, result) =>
            err ? reject(err) : resolve(result)
        )
    )
        .then((result) => {
            console.log(" Update Function Alias Version Response: ", JSON.stringify(result));
            lambdaVersionResponse = result;
        })
        .catch(error => {
            console.log(" FAILED to update version for function: %s, alias: %s, version: %s. ERROR Message: %s", lambdaFunctionName, aliasName, aliasVersion, error);
            process.exit(10);
        });

    return lambdaVersionResponse;
};

/**
 *
 * @returns {Promise<void>}
 */
const cleanupLambdaVersions = async (tfStateFile) => {
    try {
        for (let item of tfStateFile.resources) {
            if (item.type === 'aws_lambda_function') {
                const functionName = item?.instances[0]?.attributes?.function_name;
                await deleteLambdaVersion(functionName);
            }
        }
    } catch (error) {
        console.log(" FAILED to delete lambda versions. ERROR Message: %s", JSON.stringify(error));
    }
}

/**
 *
 * @param lambdaFunctionName
 * @returns {Promise<void>}
 */
const deleteLambdaVersion = async (lambdaFunctionName) => {
    try {
        let listLambdaVersionsResponse = {};
        const lambda = new aws.Lambda({
            region: AWS_REGION
        });
        const lambdaVersionsLimit = BRANCH.startsWith('feature/') ? lambdaVersionsLimitFeatureBranch : lambdaVersionsLimitDefault;

        await new Promise((resolve, reject) =>
            lambda.listVersionsByFunction({
                    FunctionName: lambdaFunctionName,
                }, (err, result) =>
                    err ? reject(err) : resolve(result)
            )
        )
            .then((result) => {
                listLambdaVersionsResponse = result;
            })
            .catch(error => {
                console.log(" FAILED to get the list of versions for function: %s. ERROR Message: %s", lambdaFunctionName, error);
                throw error;
            });

        const functionVersions = listLambdaVersionsResponse.Versions.filter(element => element.Version !== '$LATEST').sort((a, b) => b.Version - a.Version);

        if (functionVersions.length > lambdaVersionsLimit) {
            for (let i = lambdaVersionsLimit; i < functionVersions.length; i++) {
                console.log(" Deleting version %s for function:", functionVersions[i].Version, functionVersions[i].FunctionName);
                await new Promise((resolve, reject) =>
                    lambda.deleteFunction({
                            FunctionName: functionVersions[i].FunctionName,
                            Qualifier: functionVersions[i].Version
                        }, (err, result) =>
                            err ? reject(err) : resolve(result)
                    )
                )
                    .catch(error => {
                        console.log(" FAILED to delete version for function: %s. ERROR Message: %s", lambdaFunctionName, JSON.stringify(error));
                        throw error;
                    });
            }
        }
    } catch (error) {
        console.log(" FAILED to delete lambda version. ERROR Message: %s", JSON.stringify(error));
    }
};

/**
 * Pause processing of SQS queues
 * @param {Array<String>} queueNames SQS queue names to configure
 * @param {Object} tfData JSON object of the Terraform state file
 */
const pauseProcessing = async (queueNames, tfData) => {
    const params = await getParamsForEventTrigger(queueNames, tfData, false);

    for (const param of params) {
        console.log(`Update event trigger, params: ${JSON.stringify(param)}`);
        await updateEventTrigger(param, (error) => {
            console.log(`FAILED. ERROR Message: ${JSON.stringify(error)}`);
            if (
                error.statusCode === 404 ||
                (error.statusCode === 400 && error.message === 'Function does not exist')
            ) {
                exit(0);
            }
            exit(10);
        });
    }
};

/**
 * Resume processing of SQS queues
 * @param {Array<String>} queueNames SQS queue names to configure
 * @param {Object} tfData JSON object of the Terraform state file
 */
const resumeProcessing = async (queueNames, tfData) => {
    const params = await getParamsForEventTrigger(queueNames, tfData, true);

    for (const param of params) {
        console.log(`Update event trigger, params: ${JSON.stringify(param)}`);
        await updateEventTrigger(param, (error) => {
            console.log(`FAILED. ERROR Message: ${JSON.stringify(error)}`);
            if (error.statusCode === 404) {
                exit(0);
            }
            exit(10);
        });
    }
    await updateFunctionAliases(tfData);
    await cleanupLambdaVersions(tfData);
};

/**
 * Object that lists out the functions to call by CLI
 */
const runCommandFunctions = {
    PAUSE_PROCESSING: (queueNames, tfData) => pauseProcessing(queueNames, tfData),
    RESUME_PROCESSING: (queueNames, tfData) => resumeProcessing(queueNames.reverse(), tfData),
};

/**
 * Function that is called at runtime
 */
const main = async () => {
    const commandIndex = process.argv.indexOf('--command');
    const commandName = process.argv[commandIndex + 1];
    const {env, resourcePrefix} = await processFile(terraformOutputPath)
        .then((jsonData) => {
            return {
                env: jsonData.environment.value || null,
                resourcePrefix: jsonData.resource_prefix.value,
            };
        })
        .catch((error) => {
            console.log(
                `An error occurred processing file: ${terraformOutputPath} error: ${error}`,
            );
            exit(0);
        });
    const queueNames = [
        `${resourcePrefix}-nrt-journey-alert`,
        `${resourcePrefix}-standard-journey-alert`,
    ];

    if (!env) {
        console.log("It's Brand New Environment so Skipping the stage ");
        exit(0);
    }

    if (commandIndex === -1 || !commandName) {
        // Invalid argument error code
        console.log(`Usage: node ${__filename} --command <PAUSE_PROCESSING | RESUME_PROCESSING>`);
        exit(9);
    }

    try {
        const tfData = await getDataFromTFState(env);
        runCommandFunctions[commandName](queueNames, tfData);
    } catch (error) {
        console.log(
            `Error calling ${commandName} queues: ${queueNames.join(', ')}. Response: ${error}`,
        );
        exit(10);
    }
};

main().then();
