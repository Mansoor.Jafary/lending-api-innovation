openapi: '3.0.0'
info:
  title: "${api_title}"
  version: "${api_version}"
  contact:
    email: agileteam-x43@ally.com
  x-internal-documentation: |

  description: >

    ## Introduction

    The Lending GraphQL API powers Digital Servicing Capabilities for Lending Product Customers online. 

    This specification describes the GraphQL API interface in the context of REST without documenting 
    the possible queries and mutation as these are available via the GraphQL Schema.

    ## Environments

    - Production Environment: https://live.api.ally.com/lending/graphql 

    - QA Staging Environment: https://qa.api.ally.com/lending/graphql

    - Sandbox Environment: https://dev.api.ally.com/lending/graphql    

    
    The environments you will have access to here as a developer are Sandbox, QA and Production. In Sandbox and QA, you will have access to features not yet released into Production.

    ## Who should use this API

    Use this API for secured access to Card and Loan Product Accounts in the Lending Line of Business.

    ## Getting started

    Before using this API for the first time:

    - Read this guide

    - Discuss the terms of service with the Lending API team

    - Obtain authorization and access credentials. If you need assistance or would like to leave feedback, please contact the
    [API developers](lapi-operations@ally.com) or reach out to the Ally BILD 
    developer support team.

    ## Overview

    Review the [GraphQL API Schema](https://www.google.com) to familiarize yourself with the Queries and Mutations supported by this API.
    You can review our [architecture and design](https://confluence.int.ally.com/display/~CZL74B/Reference+Architecture) within confluence. 

    ### Consuming a Lending API with OAuth2 Access Credentials

    To begin testing, you will need to generate an OAuth2 Access token to access our API. Refer to the [getting started section of the dev portal](https://developer.ally.com/get-started#how-to-use), this [guide for consuming a Lending API](https://confluence.int.ally.com/display/ACT/Guide+for+Consuming+a+CCN+API) and [this documentation](https://confluence.int.ally.com/display/EAP/How-to%3A+Generate+an+OAuth2+Access+Token) for a more thorough OAuth2 walkthough.

    ### Calling the API

    At its basic, the GraphQL API accepts a POST operation along with the query and mutation in the request body and returns a JSON response.

    ### HTTP Status Codes

    Following is a list of HTTP Status Codes supported by the GraphQL API

    | Code          | Type                            | Description |
    | ------------- | ------------------------------- | -----------:|
    | 200           | OK                              | Success |
    | 400           | Bad Request                     | The request was malformed |
    | 401           | Unauthorized                    | client is not authenticated with the server |
    | 403           | Forbidden                       | The client is authenticated with the server, but not authorized to perform the requested operation on the requested resource |
    | 405           | Method Not Allowed              | The HTTP method used is not allowed on the requested URL |
    | 409           | Conflict                        | There was a conflict when performing the operation, for example, the request attempted to update a resource that had already changed |
    | 429           | Too Many Requests               | Too many requests hit the API too quickly resulting in this rate limit error. We recommend an exponential backoff of your requests. |
    | 500           | Internal Server Error           | An error on the server occurred and was not handled |
    | 501           | Not Implemented                 | The HTTP method is not currently implemented for the requested resource |
    | 503           | Service Unavailable             | The server or one of its dependencies (such as a database) is unable to respond due to overload, outages, etc. |

    
    ### GraphQL API Errors
    
    Aside from when there is an HTTP Protocol error, the Lending GraphQL API will always respond with a 200 OK as long as it operational i.e. it is able to execute a Query or Mutation. 
    Whenever the Lending GraphQL API encounters errors while processing a GraphQL operation, its response to the client includes an errors array that contains each error that occurred. 
    Each error in the array has an provides additional useful information, including an error code.

    Here is an example error response  


    ``` { example of an error response here } ```

servers:
- url: "https://{env}.api.ally.com/{basePath}/{resource}"
  variables:
    env:
      default: dev #development
      enum:
        - qa # QA nonprod environment connected to sandbox and qa2
        - psp # Production support
        - live # Production
    basePath:
      default: "lending" #default basepath for the dev, qa, psp and production environments
    resource:
      default: graphql
      description: This is the default resource for the Lending GraphQL API

tags:
  - name: lending
    description: The Lending API domain

x-amazon-apigateway-gateway-responses:
  BAD_REQUEST_BODY:
    statusCode: '400'
    responseTemplates:
      application/json: '{"message": $context.error.messageString }'
  UNAUTHORIZED:
    statusCode: '401'
    responseParameters:
        gatewayresponse.header.WWW-Authenticate: "'Apikey'"
    responseTemplates:
      application/json: '{"message": $context.error.messageString }'
  ACCESS_DENIED:
    statusCode: '403'
    responseParameters:
    responseTemplates:
      application/json: '{"message": $context.error.messageString }'       
  QUOTA_EXCEEDED:
    statusCode: '429'
    responseTemplates:
      application/json: '{"message": $context.error.messageString}' 
  AUTHORIZER_FAILURE:
    statusCode: '500'
    responseTemplates:
      application/json: '{"message": $context.error.messageString}'

paths:
  /graphql:
    post:
      tags:
        - lending
      summary: Sends queries and mutations to Lending API GraphQL Server
      description: Sends queries and mutations to Lending API GraphQL Server
      operationId: lendingGraphQL
      security:
        - ApigeeApiKey: []
      parameters:
        - name: Ally-API-Dryrun
          in: header
          schema:
            type: boolean
          required: false
          description: Enables Dryruns
          $ref: '#/components/parameters/DeliveryPriority'
      requestBody:
        description: >-
            Query or Mutation object to be processed by the GraphQL Server
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                query:
                  type: "string"
      responses:
        '200':
          description: Successful Operation
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    type: object
        '400':
          description: Improperly formatted request e.g. a Syntax Error
        '401':
          $ref: '#/components/responses/UnauthorizedError'
        '429':
          description: Rate limit exceeded
        '500':
          description: Server error
      x-amazon-apigateway-integration:
        credentials: '${credentials}'
        uri: 'arn:aws:apigateway:${region}:sqs:path/{queueName}'
        responses:
          2\d{2}:
            statusCode: "202"
            responseTemplates:
              application/json: "{\"message\":\"Journey Triggers Accepted\", \"correlationId\" : \"$context.requestId\"}"
          4\d{2}:
            statusCode: "400"
            responseTemplates:
              application/json: "{\"message\":\"Bad Request\", \"correlationId\" : \"$context.requestId\"}"
          5\d{2}:
            statusCode: "500"
            responseTemplates:
              application/json: "{\"message\":\"Internal Server Error\", \"correlationId\" : \"$context.requestId\",\"integration-status\":\"$context.integration.status\",\"integration-requestId\":\"$context.integration.requestId\",\"integration-integrationStatus\":\"$context.integration.integrationStatus\",\"integration-error\":\"$context.integration.error\"}"
        requestParameters:
          integration.request.header.Content-Type: "'application/x-www-form-urlencoded'"
        requestTemplates:
          application/json: "##  See http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-mapping-template-reference.html\n\
            ##  This template will pass through all parameters including path, querystring,\
              \ header, stage variables, and context through to the integration endpoint\
              \ via the body/payload\n\
              #set($allParams = $input.params())\n\
              #if($allParams.header.get('delivery-priority') == '')\n\
              #set($deliveryPriorityParam = 'standard')\n\
              #else\n#set($deliveryPriorityParam = $allParams.header.get('delivery-priority').toLowerCase())\n\
              #end\n\
              #set($isDryRun = $allParams.header.get('Ally-API-Dryrun') && $allParams.header.get('Ally-API-Dryrun') == true)\n\
              #if($deliveryPriorityParam == 'standard' && $isDryRun)\n\
              #set($context.requestOverride.path.queueName='${standard_journey_alert_dryrun_queue_name}')\n\
              #elseif($deliveryPriorityParam == 'standard' && !$isDryRun)\n\
              #set($context.requestOverride.path.queueName='${standard_journey_alert_queue_name}')\n\
              #elseif($deliveryPriorityParam == 'nrt' && !$isDryRun)\n\
              #set($context.requestOverride.path.queueName='${nrt_journey_alert_queue_name}')\n\
              #elseif($deliveryPriorityParam == 'nrt' && $isDryRun)\n\
              #set($context.requestOverride.path.queueName='${nrt_journey_alert_dryrun_queue_name}')\n\
              #else\n#set($context.requestOverride.path.queueName='${standard_journey_alert_queue_name}')\n\
              #end\n\
              Action=SendMessage&MessageBody=\n{\n\"body\"\
              \ : $util.urlEncode($input.json('$')),\n\"correlationId\" : \"$context.requestId\",\n\"deliveryPriority\" : \"$deliveryPriorityParam\",\n\"params\" : {\n#foreach($type in $allParams.keySet())\n\
              \    #set($params = $allParams.get($type))\n\"$type\" : {\n    #foreach($paramName\
              \ in $params.keySet())\n    \"$paramName\" : \"$util.escapeJavaScript($params.get($paramName))\"\
              \n        #if($foreach.hasNext),#end\n    #end\n}\n    #if($foreach.hasNext),#end\n\
              #end\n},\n\"stage-variables\" : {\n#foreach($key in $stageVariables.keySet())\n\
              \"$key\" : \"$util.escapeJavaScript($stageVariables.get($key))\"\n   \
              \ #if($foreach.hasNext),#end\n#end\n},\n\"context\" : {\n    \"account-id\"\
              \ : \"$context.identity.accountId\",\n    \"api-id\" : \"$context.apiId\"\
              ,\n    \"api-key\" : \"$context.identity.apiKey\",\n    \"authorizer-principal-id\"\
              \ : \"$context.authorizer.principalId\",\n    \"caller\" : \"$context.identity.caller\"\
              ,\n    \"cognito-authentication-provider\" : \"$context.identity.cognitoAuthenticationProvider\"\
              ,\n    \"cognito-authentication-type\" : \"$context.identity.cognitoAuthenticationType\"\
              ,\n    \"cognito-identity-id\" : \"$context.identity.cognitoIdentityId\"\
              ,\n    \"cognito-identity-pool-id\" : \"$context.identity.cognitoIdentityPoolId\"\
              ,\n    \"http-method\" : \"$context.httpMethod\",\n    \"stage\" : \"\
              $context.stage\",\n    \"source-ip\" : \"$context.identity.sourceIp\"\
              ,\n    \"user\" : \"$context.identity.user\",\n    \"user-agent\" : \"\
              $context.identity.userAgent\",\n    \"user-arn\" : \"$context.identity.userArn\"\
              ,\n    \"request-id\" : \"$context.requestId\",\n    \"resource-id\" :\
              \ \"$context.resourceId\",\n    \"resource-path\" : \"$context.resourcePath\"\
              \n    }\n}\n"
        passthroughBehavior: "never"
        httpMethod: "POST"
        type: "aws"  

components:
  securitySchemes:
    ApigeeApiKey:
      type: apiKey
      name: x-api-key
      in: header
  responses:
    UnauthorizedError:
      description: There is an issue with the API credentials you have provided
      headers:
        WWW-Authenticate:
          schema:
            type: string
  parameters:
    DeliveryPriority:  
      in: header
      name: delivery-priority
      description: Delivery Priorty value to decide the message processing priority
      required: false
      schema:
        type: string
        enum:
          - NRT     

x-amazon-apigateway-request-validators:
  validateBodyAndParameters:
    validateRequestBody: true
    validateRequestParameters: true
x-amazon-apigateway-request-validator: validateBodyAndParameters
x-amazon-apigateway-api-key-source: "HEADER"