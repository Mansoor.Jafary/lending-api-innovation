export cwd=$(pwd)
cd ./src/common-utils/api-caller
rm -Rf node_modules
npm install
cd $cwd

cd ./src/common-utils/common-initializers
rm -Rf node_modules
npm install
cd $cwd

cd ./src/common-utils/execution-timer
rm -Rf node_modules
npm install
cd $cwd

cd ./src/common-utils/read-schema-from-file
rm -Rf node_modules
npm install
cd $cwd

cd ./src/common-utils/secrets-manager
rm -Rf node_modules
npm install
cd $cwd