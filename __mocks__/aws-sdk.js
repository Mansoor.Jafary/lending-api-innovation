const mockedResponses = {
    X001: {
        StatusCode: 200,
        ExecutedVersion: '$LATEST',
        Payload: '{"error":null,"data":{"currentBalance":{"amount":"4998","currency":"USD"},"allocatedCreditLimit":{"amount":"10000.00","currency":"USD"},"availableCreditLimit":{"amount":"5002.00","currency":"USD"}}}'
    },
    X002: {
        StatusCode: 200,
        ExecutedVersion: '$LATEST',
        Payload: '{"error":{"message":"Service Unavailable","type":"SYSTEM","info":{"code":503,"requestId":"5c5f3bc8-6440-49cd-8bfb-3df272d8bc91"}},"data":null}'
    }
}

const invokeFn = jest.fn().mockImplementation((params, callback) => {
    const paramsJSON = JSON.parse(params.Payload);
    const accountID = mockedResponses[paramsJSON.accountID];
    if (accountID) {
        const awsSdkPromiseResponse = jest.fn().mockReturnValue(accountID);
        return callback(null, awsSdkPromiseResponse());
    }
    return callback(new Error('Lambda Invoke Error'), null);
});

class Lambda {
    invoke = invokeFn;
}

const AWS = {
    Lambda
}

module.exports = AWS;

