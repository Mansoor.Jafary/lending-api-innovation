const { DomainError } = require('../src/common-utils/lending-errors/index');

const mockedResponses = {
    X001: {
        error: null,
        data: {
            foo: 'bar'
        }
    },
    X999: {
        error: new DomainError(),
        data: null
    }
}

const getFunctionCaller = jest.fn(() => {
    return callFunction = jest.fn((uri, payload) => {
        const { accountID } = payload;
        const mock = mockedResponses[accountID];
        if (mock) return Promise.resolve(mockedResponses[accountID]);
        return Promise.resolve({
            error: new DomainError(),
            data: null
        });
    });
});

module.exports = { getFunctionCaller };


