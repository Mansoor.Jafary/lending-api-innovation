const getAppConfigData = jest.fn(() => {
    return { 
        czl74bBalLambda: { 
            czl74bBalInquiryURI: 'foo' 
        }
    };
});

module.exports = { getAppConfigData };