// Grab side awareness value set in sf-requests-pre-flow, which is executed for all northbound traffic via pre-flow-hook at ENV scope. Value is set out-of-band via: bitbucket.int.ally.com/projects/TRAN/repos/otms-script/browse
var lapiBGFlag = context.getVariable("flow.kvm.lapi-bg-flag");
var targetURL = context.getVariable("flow.kvm.lapi-blue-target");
var lapiGreenTarget = context.getVariable("flow.kvm.lapi-green-target");

var targetHost;

if (lapiBGFlag === false) {
    targetURL = lapiGreenTarget;
}

// If the incoming request has any query params we need to add them to the targetURL
var queryParams = context.getVariable('message.querystring');
if (queryParams) {
    targetURL = targetURL + '?' + queryParams;
}

context.setVariable("target.url", targetURL);

context.setVariable("lapiBGFlag", lapiBGFlag);
print('lapiBGFlag value: ' + lapiBGFlag);